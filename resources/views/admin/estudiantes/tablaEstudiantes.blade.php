<div class="" ng-controller="formularioEstudiante">
  <!-- contenedor -->
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Datos de estudiantes participantes <small>Resumen de información</small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-sort" aria-hidden="true"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li>
                  <a ng-click="myOrderBy('nombre')">Nombre</a>
                </li>
                <li>
                  <a ng-click="myOrderBy('carnet')">Carné</a>
                </li>
                <li>
                  <a ng-click="myOrderBy('cedula')">Cédula</a>
                </li>
                <li>
                  <a ng-click="myOrderBy('fkCarrera')">Carrera</a>
                </li>
                <li>
                  <a ng-click="myOrderBy('cuentaBancaria')">Cuenta bancaria</a>
                </li>
                <li>
                  <a ng-click="myOrderBy('fkBanco')">Banco</a>
                </li>
                <li>
                  <a ng-click="myOrderBy('nivel')">Nivel</a>
                </li>
                <li>
                  <a ng-click="myOrderBy('promedio')">Promedio</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="dashboard-widget-content">
            <div class="col-xs-12">

              <div class="table-responsive center-block" id="infoEstudiantes">
                <div class="col-lg-6">
                  <div class="input-group">
                    <input ng-model="filtroEstudiantes" type="text" class="form-control" placeholder="Buscar...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" disabled style="cursor: default" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </span>
                  </div><!-- /input-group -->
                </div><!-- /.col-lg-6 -->
                <br>
                <br>
                <br>
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th id="thNom">Nombre</th>
                      <th>Carnet</th>
                      <th>Cédula</th>
                      <th>Carrera</th>
                      <th>Copia de cédula</th>
                      <th>Curriculum</th>
                      <th>Exp. Académico</th>
                      <th>Informe</th>
                      <th>Cuenta Bancaria</th>
                      <th>Banco</th>
                      <th>Nivel</th>
                      <th>Promedio</th>
                      <th class="text-center">Actividad</th>
                      <th id="thPro"></th>
                    </tr>
                  </thead>
                  <tbody>

                    <tr pagination-id="estudiantesPagination" dir-paginate="estudiante in estudiantes | orderBy : orderEstudiante | filter : filtroEstudiantes | itemsPerPage: estudiantesXPagina">
                      <td>@{{estudiante.nombre}} @{{estudiante.apellidos}}</td>
                      <td>@{{estudiante.carnet}}</td>
                      <td>@{{estudiante.cedula}}</td>
                      <td ng-repeat="carrera in carreras" ng-if="carrera.id == estudiante.fkCarrera">
                        @{{carrera.nombre}}
                      </td>
                      <td><a href="getDocumentos/cedulas@{{estudiante.URLDocumentoCedula}}" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a></td>
                      <td><a href="getDocumentos/cartas@{{estudiante.URLDocumentosPersonales}}" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a></td>
                      <td><a href="getDocumentos/expedientesAcademicos@{{estudiante.URLExpedienteAcademico}}" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a></td>
                      <td><a href="getDocumentos/informesMatricula@{{estudiante.URLInformeMatricula}}"  target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a></td>
                      <td>@{{estudiante.cuentaBancaria}}</td>
                      <td ng-repeat="banco in bancos" ng-if="banco.id == estudiante.fkBanco">
                        @{{banco.nombre}}
                      </td>
                      <td>@{{ estudiante.nivel }}</td>
                      <td>@{{ estudiante.promedio }}</td>
                      <td class="text-center">
                        <a href="" data-toggle="modal" data-target="#modalDetalle" ng-click="cargarDetalleModal(estudiante.nombre+' '+estudiante.apellidos, 'Actividades:', estudiante.id)"><i class="fa fa-eye" aria-hidden="true"></i></a>
                      </td>
                      <td>
                        <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalEstudianteModificar" ng-click="editarEstudiante( estudiante.id, estudiante.carnet, estudiante.cedula, estudiante.nombre, estudiante.apellidos, estudiante.promedio, estudiante.nivel)"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                        @if(auth()->user()->user_type == 1)
                        <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalEliminar" ng-click="eliminarEstudiante(estudiante.id, estudiante.nombre, estudiante.apellidos)"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        @endif
                      </td>

                    </tr>
                  </tbody>
                </table>
                <center><dir-pagination-controls pagination-id="estudiantesPagination"></dir-pagination-controls></center>
              </div>

            </div>
            <!-- <div id="world-map-gdp" class="col-md-8 col-sm-12 col-xs-12" style="height:230px;"></div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Fin de contenedor -->

  @include('admin.estudiantes.modalEstudiantes')
</div>
