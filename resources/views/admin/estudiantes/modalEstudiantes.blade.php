<!-- Modal modificar estudiante -->
<div id="modalEstudianteModificar" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modificar estudiante</h4>
      </div>
      <form action="@{{'estudiantes/' + idEstudiante }}" class="form-horizontal" method="POST" enctype="multipart/form-data" autocomplete="off" name="formModificarEstudiante">
        <div class="modal-body">
        	<!-- Token para formularios de Laravel -->
        	{{ csrf_field() }}
        	<!--  -->
          <!-- Método de envío -->
          <input type="hidden" name="_method" value="PUT">
          <!--  -->
          <div class="form-group form-group-sm @if($errors->has('nombre')) has-error @endif ">
                <label for="nombre" class="control-label col-sm-3">Nombre:</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="nombre" placeholder="Nombre" ng-model="nombreEstudiante" readonly required>
                    @if($errors->has('nombre'))
                        <span class="help-block">{{ $errors->first('nombre') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group form-group-sm @if($errors->has('apellidos')) has-error @endif">
                <label for="apellidos" class="control-label col-sm-3">Apellidos:</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="apellidos" placeholder="Apellidos" ng-model="apellidosEstudiante" readonly required>
                    @if($errors->has('apellidos'))
                        <span class="help-block">{{ $errors->first('apellidos') }}</span>
                    @endif
                </div>
            </div>
          <div class="form-group form-group-sm">
            <label for="carnet" class="control-label col-sm-3">Carné:</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="carnet" placeholder="Carné" ng-model="carnetEstudiante" required @if(auth()->user()->user_type != 1) readonly @endif>
              <span style="color:red" ng-show="formModificarEstudiante.carnet.$dirty && formModificarEstudiante.carnet.$invalid">
              <span class="error" ng-show="formModificarEstudiante.carnet.$error.required">El carné del estudiante es requerido</span>
              </span>
            </div>
          </div>
          <div class="form-group form-group-sm">
            <label for="cedula" class="control-label col-sm-3">Cédula:</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="cedula" placeholder="Cédula" ng-model="cedulaEstudiante" required @if(auth()->user()->user_type != 1) readonly @endif>
              <span style="color:red" ng-show="formModificarEstudiante.cedula.$dirty && formModificarEstudiante.cedula.$invalid">
              <span class="error" ng-show="formModificarEstudiante.cedula.$error.required">La cédula del estudiante es requerida</span>
              </span>
            </div>
          </div>
            <div class="form-group form-group-sm @if($errors->has('promedio')) has-error @endif">
                <label for="promedio" class="control-label col-sm-3">Promedio:</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="promedio" placeholder="Promedio" ng-model="promedioEstudiante" @if(auth()->user()->user_type == 1 || auth()->user()->user_type == 3) @else readonly @endif>
                    <span style="color:red" ng-show="formModificarEstudiante.promedio.$dirty && formModificarEstudiante.promedio.$invalid">
                    <span class="error" ng-show="formModificarEstudiante.promedio.$error.required">El promedio del estudiante es requerido</span>
                    </span>
                    @if($errors->has('promedio'))
                        <span class="help-block">{{ $errors->first('promedio') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group form-group-sm @if($errors->has('nivel')) has-error @endif">
                <label for="nivel" class="control-label col-sm-3">nivel:</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="nivel" placeholder="Nivel" ng-model="nivelEstudiante" @if(auth()->user()->user_type == 1 || auth()->user()->user_type == 2) @else readonly @endif>
                    <span style="color:red" ng-show="formModificarEstudiante.nivel.$dirty && formModificarEstudiante.nivel.$invalid">
                    <span class="error" ng-show="formModificarEstudiante.nivel.$error.required">El nivel del estudiante es requerido</span>
                    </span>
                    @if($errors->has('nivel'))
                        <span class="help-block">{{ $errors->first('nivel') }}</span>
                    @endif
                </div>
            </div>
        <div class="pull-right">
          <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-warning btn-sm" ng-disabled="formModificarEstudiante.$invalid" ng-class="{disabled: formModificarEstudiante.$invalid}">Modificar estudiante</button>
          <br/><br/>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
