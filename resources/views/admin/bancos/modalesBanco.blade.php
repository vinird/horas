<!-- MODAL AGREGAR BANCO -->
<div class="modal fade" id="modalAgregarBanco" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Agregar nuevo banco</h4>
            </div>
            <div class="modal-body">
                <form action="{{ route('bancos.store') }}" class="form-horizontal" method="POST" autocomplete="off" id="formularioBanco" name="formAgregarBanco">
                    <!-- token para formularios de Laravel -->
                {{ csrf_field() }}
                <!--  -->
                    <div class="form-group form-group-sm @if($errors->has('nombre')) has-error @endif">
                        <label for="nombre" class="control-label col-sm-3">Nombre:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nombre" placeholder="Nombre del Banco" ng-model="agregarNombreBanco" required>
                            <span style="color:red" ng-show="formAgregarBanco.nombre.$dirty && formAgregarBanco.nombre.$invalid">
                                <span class="error" ng-show="formAgregarBanco.nombre.$error.required">El nombre del banco es requerido</span>
                            </span>
                            @if($errors->has('nombre'))
                                <span class="help-block">{{ $errors->first('nombre') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success btn-sm" id="boton_enviar" ng-disabled="formAgregarBanco.$invalid" ng-class="{disabled: formAgregarBanco.$invalid}">Agregar banco</button>
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<!-- MODAL MODIFICAR BANCO -->
<div class="modal fade" id="modalModificarBanco" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modificar banco</h4>
            </div>
            <div class="modal-body">
                <form action="@{{ 'bancos/' + modificarBancoID }}" class="form-horizontal" method="POST" autocomplete="off" id="formularioBanco" name="formModificarBanco">
                    <!-- token para formularios de Laravel -->
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PUT">
                <!--  -->
                    <div class="form-group form-group-sm @if($errors->has('nombre')) has-error @endif">
                        <label for="nombre" class="control-label col-sm-3">Nombre:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control validate" name="nombre" placeholder="Nombre del Banco" ng-model="modificarNombreBanco" required>
                            <span style="color:red" ng-show="formModificarBanco.nombre.$dirty && formModificarBanco.nombre.$invalid">
                                <span class="error" ng-show="formModificarBanco.nombre.$error.required">El nombre del banco es requerido</span>
                            </span>
                            @if($errors->has('nombre'))
                                <span class="help-block">{{ $errors->first('nombre') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-warning btn-sm" id="boton_enviar" ng-disabled="formModificarBanco.$invalid" ng-class="{disabled: formModificarBanco.$invalid}">Modificar banco</button>
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
