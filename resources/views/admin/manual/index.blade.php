<!DOCTYPE html>
<html>
<head>
 <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}" />
 <link rel="stylesheet" href="{{ asset('css/manual/styles.css') }}">
 <link rel="stylesheet" href="{{ asset('css/include/materialize.min.css') }}">

</head>

<body>
  <header>
    <div id = "fixed" class="navbar-fixed">
      <nav class = "fonNav">
        <div class="nav-wrapper">
          <a href="#" class="brand-logo"><img src="img/firma-ucr.png"></a>
        </div>
      </nav>
    </div>
  </header>
  <!-- Navbar -->
  <div id="contenedor">
    <div class="row ">
      <a href="{{ asset('manual/estudiantes') }}"><div class="col s12 m4 l8 hvr-grow-shadow" id="box"><h3 class="center-align">Estudiantes <br>
      <i class="fa fa-user-circle" aria-hidden="true" style="margin-top: 20px;"></i></h3></div></a>
      <a href="{{ asset('manual/excel') }}"><div class="center-align col s12 m4 l2 hvr-grow-shadow" id="box02"><h5> Subir <br> promedios y niveles
      </h5><h3><i class="fa fa-cloud-upload" aria-hidden="true"></i></h3></div></a>
      <a href="{{ asset('manual/bancos') }}"><div class="center-align col s12 m4 l2 hvr-grow-shadow" id="box06"><h3>Bancos<br>
      <i class="fa fa-money" aria-hidden="true" style="margin-top: 20px;"></i></h3></div></a>
      <a href="{{ asset('manual/actividades') }}"><div class="center-align col s12 m4 l4 hvr-grow-shadow" id="box03"><h4><i class="fa fa-suitcase" aria-hidden="true"></i>
      &nbsp;Actividades</h4></div></a>
      <a href="{{ asset('manual/fecha') }}"><div class="center-align col s12 m4 l4 hvr-grow-shadow" id="box07"><h4><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;Fecha de concurso
      </h4></div></a>
      <a href="{{ asset('manual/usuarios') }}"><div class="center-align col s12 m4 l1 hvr-grow-shadow" id="box04"><h5>Usuarios <br>
      <i class="fa fa-user-o" aria-hidden="true" style="margin-top: 10px;"></i></h5></div></a>
      <a href="{{ asset('manual/carreras') }}"><div class="center-align col s12 m4 l3 hvr-grow-shadow" id="box05"><h3><i class="fa fa-graduation-cap" aria-hidden="true"></i>
&nbsp;Carreras</h3></div></a>
    </div>
  </div>

  <script type="text/javascript" src="{{ asset('js/include/jquery.min.js') }}"></script>
  <script src="{{ asset('js/include/materialize.min.js') }}"></script>
</body>
</html>
