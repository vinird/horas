@extends('admin.manual.secciones.sub.app')
@section('title', 'Carreras')

@section('content')

<div class="container">
  <h2 class="center-align">Carreras</h2>
  <hr>
  <br>
  <br>
  <br>
  <br>
  <div class="row">

    <div class="col s12 m12 l4 parrafo animated fadeInLeft">
      <h2><i class="fa fa-cloud-upload fa-fw"></i>&nbsp;Paso 1</h2>
      <hr>
      <p>Para agregar carreras debe ir a la sección de carreras en la parte izquierda de la pantalla y seleccionarlo.
      </p>
    </div>
    <div class="col m12 l1 "></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image animated fadeInRight" src="{{ asset('img/manualImages/carreras01.png') }}" alt="">
  </div>

  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo">
      <h2><i class="fa fa-cloud-upload fa-fw"></i>&nbsp;Paso 2</h2>
      <hr>
      <p>Debe ingresar el nombre de la carrera con su respectiva sigla y selecciona la opción de "Agregar carrera".<br>
      </p>
    </div>

    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image" src="{{ asset('img/manualImages/carreras02.png') }}" alt="">
  </div>

  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo">
      <h2><i class="fa fa-check-square fa-fw"></i>&nbsp;Paso 3</h2>
      <hr>
      <p>Luego le aparecerá un mensaje de confirmación donde le informa que se agregó la carrera exitósamente.
      </p>
    </div>
    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image" src="{{ asset('img/manualImages/carreras03.png') }}" alt="">
  </div>



  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo">
      <h2><i class="fa fa-search fa-fw"></i>&nbsp;Paso 4</h2>
      <hr>
      <p>Puede verificar que se agregó exitosamente yendo a la sección de carreras y buscando la carrera que recién agregó.
      </p>
    </div>
    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image" src="{{ asset('img/manualImages/carreras04.png') }}" alt="">
  </div>

  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo">
      <h2><i class="fa fa-pencil-square-o"></i>&nbsp;Paso 5</h2>
      <hr>
      <p>Ahí mismo puede modificarlas o eliminarlas si así lo desea. <br>
        <b>Nota:</b> Recuerde que para eliminar una carrera esta no puede estar vinculada a ningún alumno en el sistema, primero debe desvincularlo y luego la puede eliminar.
      </p>
    </div>
    <div class="col m12 l1"></div>
    <div class=''>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image" src="{{ asset('img/manualImages/carreras05.png') }}" alt="">
    </div>
  </div>
    <div class='row'></div>

  <br><br>
  <hr>
  <br><br>
  <a class="waves-effect waves-light btn-large valign-wrapper" href="{{ url('manual') }}" style="
  width: 100%; margin-bottom:25px;"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;<b>Regresar al Manual</b></a>
</div>

@endsection
@section('scripts')
<script>
  $(document).ready(function(){
    $(".button-collapse").sideNav();
    $('.materialboxed').materialbox();
    var pantalla = $(window).width();
    if(pantalla <= 100){
      $("#fixed").removeClass("navbar-fixed");
    }
  });
</script>
@endsection
