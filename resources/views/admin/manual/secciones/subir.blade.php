@extends('admin.manual.secciones.sub.app')
@section('title', 'Excel')

@section('content')

<div class="container">
  <h2 class="center-align">Subir promedios y niveles</h2>
  <hr>
  <br>
  <br>
  <br>
  <br>
  <div class="row">

    <div class="col s12 m12 l4 parrafo animated fadeInLeft">
      <h2><i class="fa fa-cloud-upload"></i>&nbsp;Paso 1</h2>
      <hr>
      <p>Debe ir a la sección de "Cargas de hoja de calculo" como puede ver en la imagen a la derecha y escoger subir promedios ó subir niveles.<br>
      </p>
    </div>
    <div class="col m12 l1 "></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image animated fadeInRight" src="{{ asset('img/manualImages/Subir0.png') }}" alt="">
  </div>

  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo">
      <h2><i class="fa fa-cloud-upload"></i>&nbsp;Paso 2</h2>
      <hr>
      <p>Luego le aparecerá una ventana con la opción de subir el archivo.<br>
      </p>
    </div>

    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image" src="{{ asset('img/manualImages/Subir1.png') }}" alt="">
  </div>

  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo">
      <h2><i class="fa fa-file-excel-o"></i>&nbsp;Paso 3</h2>
      <hr>
      <p>En este punto debe elegir el archivo donde estén los promedios o los niveles de los estudiantes.<br>
        <b>Nota:</b> Solo se pueden subir archivos con formato .xml, .xlsx o .csv.<br>
      </p>
    </div>
    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image" src="{{ asset('img/manualImages/Subir2.png') }}" alt="">
  </div>



  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo">
      <h2><i class="fa fa-file-excel-o"></i>&nbsp;Paso 4</h2>
      <hr>
      <p>A continuación se prepara el archivo para ser cargado a la página y debe proceder a seleccionar el botón "Agregar".<br>
      </p>
    </div>
    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image" src="{{ asset('img/manualImages/Subir3.png') }}" alt="">
  </div>

  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo">
      <h2><i class="fa fa-thumbs-o-up"></i>&nbsp;Paso 5</h2>
      <hr>
      <p>Cuando el mensaje "Niveles/ Promedios cargados exitosamente" ya los archivos con los niveles o los promedios habrán sido cargados.
      </p>
    </div>
    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image" src="{{ asset('img/manualImages/Subir4.png') }}" alt="">
  </div>
    <div class='row'></div>

  <br><br>
  <hr>
  <br><br>
  <a class="waves-effect waves-light btn-large valign-wrapper" href="{{ url('manual') }}" style="
  width: 100%; margin-bottom:25px;"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;<b>Regresar al Manual</b></a>
</div>

@endsection
@section('scripts')
<script>
  $(document).ready(function(){
    $(".button-collapse").sideNav();
    $('.materialboxed').materialbox();
    var pantalla = $(window).width();
    if(pantalla <= 992){
      $("#fixed").removeClass("navbar-fixed");
    }
  });
</script>
@endsection
