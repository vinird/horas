<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<link rel="stylesheet" href="{{ asset('css/manual/styles.css') }}">
	<link rel="stylesheet" href="{{ asset('css/include/materialize.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}" />
</head>
<body>
	<header>
		<div id = "fixed" class="navbar-fixed">
			<nav class = "fonNav">
				<div class="nav-wrapper">
					<a href="{{ url('home') }}" class="brand-logo"><img src="../img/firma-ucr.png"></a>
					<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
					<ul class="right hide-on-med-and-down">
						<li><a href="{{ url('home') }}"><i class="fa fa-home fa-fw" aria-hidden="true"></i>Home</a></li>
						<li><a href="{{ asset('manual/estudiantes') }}">Estudiantes</a></li>
						<li><a href="{{ asset('manual/actividades') }}">Actividades</a></li>
						<li><a href="{{ asset('manual/bancos') }}">Bancos</a></li>
						<li><a href="{{ asset('manual/excel') }}">Subir promedios</a></li>
						<li><a href="{{ asset('manual/carreras') }}">Carreras</a></li>
						<li><a href="{{ asset('manual/usuarios') }}">Usuarios</a></li>
						<li><a href="{{ asset('manual/fecha') }}">Fecha concurso</a></li>
					</ul>
					<ul class="side-nav" id="mobile-demo">
						<li><a href="{{ url('home') }}"><i class="fa fa-home fa-fw" aria-hidden="true"></i>Home</a></li>
						<li><a href="{{ asset('manual/estudiantes') }}">Estudiantes</a></li>
						<li><a href="{{ asset('manual/actividades') }}">Actividades</a></li>
						<li><a href="{{ asset('manual/bancos') }}">Bancos</a></li>
						<li><a href="{{ asset('manual/excel') }}">Subir promedios</a></li>
						<li><a href="{{ asset('manual/carreras') }}">Carreras</a></li>
						<li><a href="{{ asset('manual/usuarios') }}">Usuarios</a></li>
						<li><a href="{{ asset('manual/fecha') }}">Fecha concurso</a></li>
					</ul>
				</div>
			</nav>
		</div>
	</header> 

	@yield('content')
</body>
<script type="text/javascript" src="{{ asset('js/include/jquery.min.js') }}"></script>
<script src="{{ asset('js/include/materialize.min.js') }}"></script>
@yield('scripts')
</html>
