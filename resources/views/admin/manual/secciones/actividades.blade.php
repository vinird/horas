@extends('admin.manual.secciones.sub.app')
@section('title', 'Actividades')

@section('content')

<div class="container">
  <h2 class="center-align">Actividades</h2>
  <hr>
  <br>
  <br>
  <br>
  <br>
  <div class="row">

    <div class="col s12 m12 l4 parrafo animated fadeInLeft">
      <h2><i class="fa fa-list-ol fa-fw"></i>&nbsp;Lista</h2>
      <hr>
      <p>En la sección de actividades se encuentran todos los proyectos en los cuales los estudiantes pueden participar para realizar las horas. Puede modificar o eliminar cualquier actividad seleccionando alguno de los íconos a la derecha.<br>
      </p>
    </div>
    <div class="col m12 l1 "></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image animated fadeInRight" src="{{ asset('img/manualImages/actividades01.PNG') }}" alt="">
  </div>

  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo">
      <h2><i class="fa fa-cloud-upload fa-fw"></i>&nbsp;Agregar</h2>
      <hr>
      <p>Para agregar actividades debe ir a la sección de Administración en la parte izquierda de la pantalla, luego seleccionar la opción de "Actividades y/o proyectos", le aparecerá un formulario solicitandole la información necesaria para añadir la actividad; cuando finalice sólo debe selecionar "Agregar" y aparecerá un mensaje de confirmación.
      </p>
    </div>
    <br><br>
    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image" src="{{ asset('img/manualImages/actividades02.PNG') }}" alt="">
  </div>

  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo">
      <h2><i class="fa fa-pencil-square-o fa-fw"></i>&nbsp;Modificar</h2>
      <hr>
      <p>Si selecciona la opción para modificar las actividades se abrirá una ventana en donde podrá modificar alguna de las opciones de esa actividad.<br>
      </p>
    </div>

    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image" src="{{ asset('img/manualImages/actividades03.PNG') }}" alt="">
  </div>

  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo">
      <h2><i class="fa fa-eraser fa-fw"></i>&nbsp;Eliminar</h2>
      <hr>
      <p>Si selecciona la opción de eliminar, le aparecerá un mensaje emergente donde tiene que confirmar la eliminación de la actividad o si fue por error seleccione la opción de cancelar.
      </p>
    </div>
    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image" src="{{ asset('img/manualImages/actividades04.PNG') }}" alt="">
  </div>
  <div class='row'></div>

  <br><br>
  <hr>
  <br><br>
  <a class="waves-effect waves-light btn-large valign-wrapper" href="{{ url('manual') }}" style="
  width: 100%; margin-bottom:25px;"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;<b>Regresar al Manual</b></a>
</div>

@endsection
@section('scripts')
<script>
  $(document).ready(function(){
    $(".button-collapse").sideNav();
    $('.materialboxed').materialbox();
    var pantalla = $(window).width();
    if(pantalla <= 992){
      $("#fixed").removeClass("navbar-fixed");
    }
  });
</script>
@endsection
