@extends('admin.manual.secciones.sub.app')
@section('title', 'Fechas')

@section('content')

<div class="container">
  <h2 class="center-align">Fechas de concurso</h2>
  <hr>
  <br>
  <br>
  <br>
  <br>
  <div class="row">

    <div class="col s12 m12 l4 parrafo animated fadeInLeft">
      <h2><i class="fa fa-search fa-fw"></i>&nbsp;Paso 1</h2>
      <hr>
      <p>La sección de Fecha de concurso podrá encontrarlo en la parte izquierda de la pantalla como lo ve en la imagen.
        <br>
      </p>
    </div>
    <div class="col m12 l1 "></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image animated fadeInRight" src="{{ asset('img/manualImages/fecha01.png') }}" alt="">
  </div>

  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo">
      <h2><i class="fa fa-pencil-square-o fa-fw"></i>&nbsp;Paso 2</h2>
      <hr>
      <p>Al seleccionar la opción de "modificar fechas" le aparecerá en la pantalla una ventana con las distintas opciones relacionadas a las fechas de concurso.<br>
      </p>
    </div>

    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image" src="{{ asset('img/manualImages/fecha02.png') }}" alt="">
  </div>

  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo">
      <h2><i class="fa fa-pencil-square-o fa-fw"></i>&nbsp;Paso 3</h2>
      <hr>
      <p>Para modificar las fechas de concurso solo debe seleccionar tanto la fecha apertura como la fecha de cierre.
      </p>
    </div>
    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image" src="{{ asset('img/manualImages/fecha03.png') }}" alt="">
  </div>



  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo">
      <h2><i class="fa fa-pencil-square-o fa-fw"></i>&nbsp;Paso 4</h2>
      <hr>
      <p>En la parte de abajo de la ventana puede colocar el mensaje que quiere que le apareza al usuario cuando entra al sistema para participar por las horas, este mensaje es editable. Cuando termine solo seleccione el botón "Modificar fechas"<br>
      </p>
    </div>
    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image" src="{{ asset('img/manualImages/fecha04.png') }}" alt="">
  </div>

  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo">
      <h2><i class="fa fa-thumbs-o-up fa-fw"></i>&nbsp;Paso 5</h2>
      <hr>
      <p>Luego de presionar el botón se le mostrará un mensaje de confirmación de cambio de fechas.
      </p>
    </div>
    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image" src="{{ asset('img/manualImages/fecha05.png') }}" alt="">
  </div>
    <div class='row' id="seccion"></div>
  <br><br>
  <hr>
  <br><br>
  <a class="waves-effect waves-light btn-large valign-wrapper" href="{{ url('manual') }}" style="
  width: 100%; margin-bottom:25px;"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;<b>Regresar al Manual</b></a>
</div>

@endsection
@section('scripts')
<script>
  $(document).ready(function(){
    $(".button-collapse").sideNav();
    $('.materialboxed').materialbox();
    var pantalla = $(window).width();
    if(pantalla <= 992){
      $("#fixed").removeClass("navbar-fixed");
    }
  });
</script>
@endsection
