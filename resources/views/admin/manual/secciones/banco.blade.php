@extends('admin.manual.secciones.sub.app')
@section('title', 'Bancos')

@section('content')

<div class="container">
  <h2 class="center-align">Bancos</h2>
  <hr>
  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo animated fadeInLeft">
      <h2><i class="fa fa-cloud-upload fa-fw" aria-hidden="true"></i>&nbsp;Agregar</h2>
      <hr>
      <p>Para agregar un banco debe ir a Administración y seleccionar la opción de Bancos.
        Cuando seleccione esta opción le aparecerá una ventana en donde deberá ingresar el nombre del banco y a continuación seleccionar la opción de "Agregar Banco", luego le aparecerá un mensaje de corfimación y para asegurarse de que se agregó el banco puede ir a la tabla de bancos y buscar el nombre del banco que recién agregó.
      </p>
    </div>
    <br><br><br><br><br><br>

    <div class="col m12 l1"></div>

    <img class="materialboxed col s12 m12 l7 z-depth-2 image animated fadeInRight" src="{{ asset('img/manualImages/Bancos00.png') }}" alt="">
  </div>
  <br><br><br>
  <div class="row">
    <div class="col s12 m12 l4 parrafo animated fadeInLeft">
      <br><br><br><br><br>
      <h2><i class="fa fa-list-ol fa-fw" aria-hidden="true"></i>&nbsp;Listas</h2>
      <hr>
      <p>Debe ir a la tabla Bancos en donde puede ver la lista de los bancos disponibles y seleccionar la opción de modificar o eliminar.<br>
      </p>
    </div>
    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image animated fadeInRight" src="{{ asset('img/manualImages/banco01.PNG') }}" alt="">
  </div>

  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo">
      <h2><i class="fa fa-pencil-square-o fa-fw" aria-hidden="true"></i>&nbsp;Modificar</h2>
      <hr>
      <p>En la opción de modificar puede cambiar el nombre del banco.<br>
        <b>Nota: </b> No puede darle el nombre a un banco que ya existe.
      </p>
    </div>

    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image" src="{{ asset('img/manualImages/banco02.PNG') }}" alt="">
  </div>

  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo">
      <h2><i class="fa fa-eraser fa-fw" aria-hidden="true"></i>&nbsp;Eliminar</h2>
      <hr>
      <p>Si selecciona la opción de eliminar aparecerá un mensaje de confirmación para proceder.<br>
        Seleccione la opción "Eliminar banco" para eliminarlo.
      </p>
    </div>
    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image" src="{{ asset('img/manualImages/banco03.PNG') }}" alt="">
  </div>
    <div class='row'></div>

  <br><br>
  <hr>
  <br><br>

  <a class="waves-effect waves-light btn-large valign-wrapper" href="{{ url('manual') }}" style="
  width: 100%; margin-bottom:25px;"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;<b>Regresar al Manual</b></a>
</div>

@endsection
@section('scripts')
<script>
  $(document).ready(function(){
    $(".button-collapse").sideNav();
    $('.materialboxed').materialbox();
    var pantalla = $(window).width();
    if(pantalla <= 992){
      $("#fixed").removeClass("navbar-fixed");
    }
  });
</script>
@endsection
