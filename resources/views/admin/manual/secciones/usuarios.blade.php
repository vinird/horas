@extends('admin.manual.secciones.sub.app')
@section('title', 'Usuarios')

@section('content')

<div class="container">
  <h2 class="center-align">Usuarios</h2>
  <hr>
  <br>
  <br>
  <br>
  <br>
  <div class="row">

    <div class="col s12 m12 l4 parrafo animated fadeInLeft">
      <h2><i class="fa fa-search fa-fw"></i>&nbsp;Usuarios</h2>
      <hr>
      <p>Si selecciona la sección de Administración y luego Ver usuarios en la parte izquierda de la pantalla, le aparecerá una ventana con los usuarios de la aplicación.<br>
      </p>
    </div>
    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image animated fadeInRight" src="{{ asset('img/manualImages/usuarios01.png') }}" alt="">
  </div>

  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo">
      <h2><i class="fa fa-list-ol"></i>&nbsp;Ver lista</h2>
      <hr>
      <p>Aquí podrá ver la lista de los usuarios que puede utilizar la aplicación y se le muestran 3 opciones, agregar un nuevo usuario, eliminarlos y modificarlos.<br>
      </p>
    </div>

    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image" src="{{ asset('img/manualImages/usuarios02.PNG') }}" alt="">
  </div>

  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo">
      <h2><i class="fa fa-cloud-upload fa-fw"></i>&nbsp;Agregar</h2>
      <hr>
      <p>Si selecciona la opción de nuevo usuario se le abrirá una ventana con el formulario que ve a la derecha, debe llenarlo con la información solicitada.
        <b>Nota: </b> Debe elegir el tipo de usuario según su función, ya sea de la gestión actividades, niveles o promedios. Si la opción de activo no está seleccionada el usuario no podrá acceder al sistema.
      </p>
    </div>
    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image" src="{{ asset('img/manualImages/usuarios03.PNG') }}" alt="">
  </div>



  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo">
      <h2><i class="fa fa-pencil-square-o fa-fw"></i>&nbsp;Modificar</h2>
      <hr>
      <p>Si selecciona esta opción se le abrirá una ventana con las opciones que puede modificar del usuario, cuando finaliza de hacer los cambios selecciona el botón modificar y se relizarán los cambios.
      </p>
    </div>
    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image" src="{{ asset('img/manualImages/usuarios04.PNG') }}" alt="">
  </div>

  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo">
      <h2><i class="fa fa-eraser fa-fw"></i>&nbsp;Eliminar</h2>
      <hr>
      <p>Si selecciona la opción de eliminar le aparecerá un mensaje preguntando si desea eliminar al usuario, si desea eliminarlo proceda a selecccionar la opción de eliminar u omita en caso de error.
      </p>
    </div>
    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image" src="{{ asset('img/manualImages/usuarios05.png') }}" alt="">
  </div>
    <div class='row'></div>
  <br><br>
  <hr>
  <br><br>
  <a class="waves-effect waves-light btn-large valign-wrapper" href="{{ url('manual') }}" style="
  width: 100%; margin-bottom:25px;"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;<b>Regresar al Manual</b></a>
</div>

@endsection
@section('scripts')
<script>
  $(document).ready(function(){
    $(".button-collapse").sideNav();
    $('.materialboxed').materialbox();
    var pantalla = $(window).width();
    if(pantalla <= 992){
      $("#fixed").removeClass("navbar-fixed");
    }
  });
</script>
@endsection
