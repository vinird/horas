@extends('admin.manual.secciones.sub.app')
@section('title', 'Estudiantes')

@section('content')

<div class="container">
  <h2 class="center-align">Estudiantes</h2>
  <hr>
  <br>
  <br>
  <br>
  <br>
  <div class="row">

    <div class="col s12 m12 l4 list-group parrafo animated fadeInLeft">
      <h2 class="list-group-item"><i class="fa fa-id-card fa-fw"></i>&nbsp;Paso 1</h2>
      <hr>
      <p>Para solicitar las horas debe de llenar el formulario que se ve a la izquierda con la información que se le solicita. <br>
      </p>
    </div>
    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image animated fadeInRight" src="{{ asset('img/manualImages/01.png') }}" alt="">
  </div>

  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo" id="parrafo">
      <h2><i class="fa fa-cloud-upload fa-fw" aria-hidden="true"></i>&nbsp;Paso 2</h2>
      <hr>
      <p>En esta sección se deben subir los archivos solicitados.<br>
      </p>
    </div>

    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image" src="{{ asset('img/manualImages/02.png') }}" alt="">


  </div>

  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo">
      <h2><i class="fa fa-exclamation-triangle fa-fw" aria-hidden="true"></i>&nbsp;Paso 3</h2>
      <hr>
      <p>Debe ingresar su carné, su número de cuenta del banco y elegir el banco a utilizar. <br>
        <b>Nota:</b> En el número de cuenta debe ingresar únicamente números.<br>
      </p>
    </div>
    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image" src="{{ asset('img/manualImages/03.png') }}" alt="">
  </div>


  <div class="row" id="seccion">
    <div class="col m12 l4 parrafo">
      <h2><i class="fa fa-thumbs-o-up fa-fw" aria-hidden="true"></i>&nbsp;Paso 4</h2>
      <hr>
      <p>Aquí se muestran las actividades, solo debe elegir la actividad para concursar y aceptar
        los términos y condiciones.<br>
      </p>
    </div>
    <div class="col m12 l1"></div>
    <img class="materialboxed col s12 m12 l7 z-depth-2 image" src="{{ asset('img/manualImages/04.png') }}" alt="">
  </div>
  <div class='row'></div>
  <br><br>
  <hr>
  <br><br>

  <a class="waves-effect waves-light btn-large valign-wrapper" href="{{ url('manual') }}" style="
  width: 100%; margin-bottom:25px;"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;<b>Regresar al Manual</b></a>
</div>
<br><br>

@endsection
@section('scripts')
<script>
  $(document).ready(function(){
    $(".button-collapse").sideNav();
    $('.materialboxed').materialbox();
    var pantalla = $(window).width();
    if(pantalla <= 992){
      $("#fixed").removeClass("navbar-fixed");
    }
  });
</script>
@endsection
