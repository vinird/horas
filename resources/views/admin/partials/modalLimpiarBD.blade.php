<!-- Modal limpiar información del sistema -->
<div id="limpiarBd" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Limpiar información del sistema</h4>
      </div>
      <form action="/limpiarBase" class="form-horizontal" method="POST" name="formLimpiarBD" autocomplete="off">
        <div class="modal-body">
        	<!-- Token para formularios de Laravel -->
        	{{ csrf_field() }}
        	<!--  -->
        	<div class="form-group form-group-sm">
        		<p for="confirmarRespaldo" class="col-sm-12">Se realizará una limpieza de la base de datos que conlleva la eliminación de actividades y estudiantes, así como los documentos dados para participación en el concurso. Esta acción no es reversible. Para continuar escriba su contraseña.</p>
        	</div>
        	<div class="form-group form-group-sm @if($errors->has('confirmarContrasenaRespaldo')) has-error @endif">
        		<label for="confirmarContrasenaRespaldo" class="control-label col-sm-3">Contraseña:</label>
        		<div class="col-sm-9">
        			<input type="password" class="form-control" name="confirmarContrasenaRespaldo" placeholder="Contraseña" required>
              <span style="color:red" ng-show="formLimpiarBD.confirmarContrasenaRespaldo.$dirty && formLimpiarBD.confirmarContrasenaRespaldo.$invalid">
                  <span class="error" ng-show="formLimpiarBD.confirmarContrasenaRespaldo.$error.required">La contraseña es requerida para esta acción</span>
              </span>
              @if($errors->has('confirmarContrasenaRespaldo'))
        				<span class="help-block">{{ $errors->first('confirmarContrasenaRespaldo') }}</span>
        			@endif
        		</div>
        	</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-danger" ng-disabled="formLimpiarBD.$invalid" ng-class="{disabled: formLimpiarBD.$invalid}">Limpiar información</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
