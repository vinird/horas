<div class="modal fade" id="excel" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Ingresar hoja de cálculo con los promedios</h4>
      </div>
      <form action="/importPromedio" method="POST" enctype="multipart/form-data">
          <!-- token para formularios de Laravel -->
          {{ csrf_field() }}
          <!--  -->
          <div class="modal-body">
              <div class="form-group @if($errors->has('promedios')) has-error @endif">
                  <label for="promedio">Subir archivo</label>
                  <input type="file" id="promedio" name="promedios" accept=",.xml,.csv,.xlsx">
                  <p class="help-block">Solo archivos .xml .csv .xlsx</p>
              </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-success">Agregar</button>
          </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<div class="modal fade" id="excelNiveles" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Ingresar hoja de cálculo con los niveles</h4>
      </div>
      <form action="/importNiveles" method="POST" enctype="multipart/form-data">
          <!-- token para formularios de Laravel -->
          {{ csrf_field() }}
          <!--  -->
          <div class="modal-body">
              <div class="form-group @if($errors->has('promedios')) has-error @endif">
                  <label for="niveles">Subir archivo</label>
                  <input type="file" id="nivel" name="niveles" accept="xm,.xml,.csv,.xlsx">
                  <p class="help-block">Solo archivos .xml .csv .xlsx</p>
              </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-success">Agregar</button>
          </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
