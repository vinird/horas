<div class="row tile_count">

  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-users"></i> Estudiantes concursando</span>
    <div class="count">
      @if(isset($estudiantes))
      {{ count($estudiantes) }}
      @endif
    </div>
    <span class="count_bottom">
      <i class="red">
        @if(isset($estudiantes))
        {{ count($estudiantes->where('promedio', '<', 8 )->where('promedio', '!=', null)) }}
        @endif
      </i> Promedios inferior a 8.0 </span>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-tasks"></i> Actividades y/o proyectos</span>
      <div class="count">
        @if(isset($actividades))
        {{ count($actividades) }}
        @endif
      </div>
      @if(isset($actividades))
      <span class="count_bottom">
        Estudiante:
        <i class="green">
          {{ count($actividades->where('fkDesignacion', '+', 1)) }}
        </i>
        Asistente:
        <i class="green">
          {{ count($actividades->where('fkDesignacion', '+', 2)) }}
        </i><br>
        Graduado:
        <i class="green">
          {{ count($actividades->where('fkDesignacion', '+', 3)) }}
        </i>
      </span>
      @endif
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> Accesos al sistema</span>
      <div class="count">
        @if(isset($cantidadlogs))
        {{ $cantidadlogs }}
        @endif
      </div>
      @if(isset($primerAcceso))
      <span class="count_bottom">Primer acceso: <i class="green">
        {{ $primerAcceso }}
      </i></span>
      @endif
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> Usuarios</span>
      <div class="count">
        @if(isset($cantidadUsuarios))
        {{ $cantidadUsuarios }}
        @endif
      </div>
      <span class="count_bottom">Usuarios activos: <i class="green">
        @if(isset($usuariosActivos))
        {{ $usuariosActivos }}
        @endif
      </i></span>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12 tile_stats_count">
      <span class="count_top"><i class="fa fa-clock-o"></i> Formulario disponible hasta  </span>
      <div class="count" id="periodoSistemaCount">
        @if(isset($periodoSistema))
        {{ $periodoSistema->fechaFinal }}
        @endif
      </div>
      <span class="count_bottom">Activo desde: <i class="green">
        @if(isset($periodoSistema))
        {{ $periodoSistema->fechaInicio }}
        @endif
      </i> </span>
    </div>
  </div>
@include('vendor.flash.message')
