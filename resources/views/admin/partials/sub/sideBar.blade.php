 <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  @if(auth()->user()->user_type == 1 || auth()->user()->user_type == 4)
  <div class="menu_section">
    <h3>General</h3>
    <ul class="nav side-menu">
      <li><a><i class="fa fa-home"></i> Administración <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a data-toggle="modal" data-target="#modalAgregarActividad">Actividades y/o proyectos</a></li>
          @if(auth()->user()->user_type == 1)
          <li><a data-toggle="modal" data-target="#modalAgregarArea">Áreas de actividades y/o proyectos</a></li>
          <li><a data-toggle="modal" data-target="#modalAgregarBanco">Bancos</a></li>
          <li><a data-toggle="modal" data-target="#modalAgregarCarrera">Carreras</a></li>
          <li><a data-toggle="modal" data-target="#modalUsuarios">Ver usuarios</a></li>
          @endif
        </ul>
      </li>
    </ul>
  </div>
  @endif

  @if(auth()->user()->user_type == 1)
  <div class="menu_section">
    <h3>Concurso</h3>
    <ul class="nav side-menu">
      <li><a><i class="fa fa-calendar"></i>Fecha de concurso<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a data-toggle="modal" data-target="#modalPeriodoSistema" data-periodoSistema='[{"fechaInicio":"{{$periodoSistema->fechaInicio}}", "fechaFinal":"{{$periodoSistema->fechaFinal}}"}]'>Modificar fechas</a></li>
        </ul>
      </li>
    </ul>
  </div>
  @endif

  @if(auth()->user()->user_type == 1 || auth()->user()->user_type == 2 || auth()->user()->user_type == 3)
  <div class="menu_section">
    <h3>Base de datos</h3>
    <ul class="nav side-menu">
      <li><a><i class="fa fa-database"></i>Cargar hoja de cálculo<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
        @if(auth()->user()->user_type == 1 || auth()->user()->user_type == 3)
          <li><a data-toggle="modal" data-target="#excel">Subir promedios</a></li>
          
          @endif
          @if(auth()->user()->user_type == 1 || auth()->user()->user_type == 2)
          <li><a data-toggle="modal" data-target="#excelNiveles"> Subir niveles</a></li>
          @endif
        </ul>
      </li>
      @if(auth()->user()->user_type == 1)
      <li><a><i class="fa fa-database"></i>Gestión de información<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a data-toggle="modal" data-target="#limpiarBd">Limpiar base de datos</a></li>
        </ul>
      </li>
      @endif
    </ul>
  </div>
  @endif

  @if(auth()->user()->user_type == 1)
  <div class="menu_section">
    <h3>Concurso</h3>
    <ul class="nav side-menu">
      <li><a><i class="fa fa-file-excel-o"></i>Reportes<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="reporteGeneral">Reporte general</a></li>
          <li><a href="reporteEstudiantesSeleccionados">Reporte de estudiantes seleccionados</a></li>
        </ul>
      </li>
    </ul>
  </div>
  @endif

</div>
