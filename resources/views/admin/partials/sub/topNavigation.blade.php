<?php 
$md5Mail = md5(trim(auth()->user()->email));
$gravatarURL ="https://www.gravatar.com/avatar/".$md5Mail.".jpg?s=100" ;
$conn = @fsockopen("www.google.com", 80);
$conected = false;
if($conn)
{
 $conected = true;
} 
?>

<div class="top_nav">
  <div class="nav_menu">
    <nav>
      <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
      </div>

      <ul class="nav navbar-nav navbar-right">
        <li class="">
          <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            @if($conected)
              <img src="{{$gravatarURL}}">
            @endif
            {{ Auth::user()->name }}
            <span class=" fa fa-angle-down"></span>
          </a>
          <ul class="dropdown-menu dropdown-usermenu pull-right">

            <li><a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out pull-right"></i> Salir </a></li>

            <li>
              <a href="{{ url('manual') }}" ><i class="fa fa-file-text-o pull-right" aria-hidden="true"></i> Manual de usuario</a> 
            </li>

            <li>
              <a href="" data-toggle="modal" data-target="#modalModificarContrasena"><i class="fa fa-key pull-right" aria-hidden="true"></i> Cambiar contraseña</a> 
            </li>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
            </form>

          </ul>
        </li>

        <li role="presentation" class="dropdown">
          <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-bell"></i>
            @if(isset($notificaciones))
            @if(count($notificaciones) > 0)
            <span class="badge bg-green">{{count($notificaciones)}}</span>
            @endif
            @endif
          </a>
          <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
          @if(isset($notificaciones))
          @if(count($notificaciones) > 0)
          <li>
              <div class="text-center">
                <a href="deleteNotification">
                  <strong>Eliminar</strong>
                  <i class="fa fa-trash"></i>
                </a>
              @else 
              <li class="text-center">
                <strong>No hay notificaciones que mostrar</strong>
              </li>
              </div>
            </li>
            @endif
            @endif
            @if(isset($notificaciones))
            @if(count($notificaciones) > 0)
            @foreach($notificaciones as $notificacion)
            <li>
              <a>                
                <span>
                  <span>{{$notificacion->nombre}}</span>
                  <span class="time">{{$notificacion->created_at}}</span>
                </span>
                <span class="message">
                  {{$notificacion->descripcion}}
                </span>
              </a>
            </li>    
            @endforeach
            @endif      
            @endif
          </ul>
        </li>
      </ul>
    </nav>
  </div>
</div>