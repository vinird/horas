<!-- Modal modificar periodo de acceso al sistema -->
<div id="modalPeriodoSistema" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modificar periodo de acceso al sistema</h4>
      </div>
      <form action="/actualizarPeriodo" class="form-horizontal" method="POST" name="formPeriodoSistema" autocomplete="off">
        <div class="modal-body">
        	<!-- Token para formularios de Laravel -->
        	{{ csrf_field() }}
        	<!--  -->
          <!-- Método de envío -->
          <input type="hidden" name="_method" value="PUT">
          <!--  -->
        	<div class="form-group form-group-sm @if($errors->has('fechaInicio')) has-error @endif">
        		<label for="fechaInicio" class="control-label col-sm-3">Fecha de apertura:</label>
        		<div class="col-sm-9">
        			<input type="text" class="form-control" name="fechaInicio" placeholder="Fecha de acceso al sistema" required>
              <span style="color:red" ng-show="formPeriodoSistema.fechaInicio.$dirty && formPeriodoSistema.fechaInicio.$invalid">
                  <span class="error" ng-show="formPeriodoSistema.fechaInicio.$error.required">La fecha de apertura es requerida</span>
              </span>
              @if($errors->has('fechaInicio'))
        				<span class="help-block">{{ $errors->first('fechaInicio') }}</span>
        			@endif
        		</div>
        	</div>
        	<div class="form-group form-group-sm @if($errors->has('fechaFinal')) has-error @endif">
        		<label for="fechaFinal" class="control-label col-sm-3">Fecha de cierre:</label>
        		<div class="col-sm-9">
        			<input type="text" class="form-control" name="fechaFinal" placeholder="Fecha de cierre del sistema" required>
              <span style="color:red" ng-show="formPeriodoSistema.fechaFinal.$dirty && formPeriodoSistema.fechaFinal.$invalid">
                  <span class="error" ng-show="formPeriodoSistema.fechaFinal.$error.required">La fecha de cierre es requerida</span>
              </span>
              @if($errors->has('fechaFinal'))
        				<span class="help-block">{{ $errors->first('fechaFinal') }}</span>
        			@endif
        		</div>
        	</div>
        	<hr>
          <div class="form-group form-group-sm @if($errors->has('mensajeSistema')) has-error @endif">
            <label for="mensajeSistema" class="control-label col-sm-3">Mensaje del sistema:</label>
            <div class="col-sm-9">
              <textarea class="form-control" name="mensajeSistema" placeholder="Mensaje que se mostrará al entrar al sistema" ng-model="periodoSistema.mensaje" required></textarea>
              <span style="color:red" ng-show="formPeriodoSistema.mensajeSistema.$dirty && formPeriodoSistema.mensajeSistema.$invalid">
                  <span class="error" ng-show="formPeriodoSistema.mensajeSistema.$error.required">El mensaje del sistema es requerido</span>
              </span>
              @if($errors->has('mensajeSistema'))
                <span class="help-block">{{ $errors->first('mensajeSistema') }}</span>
              @endif
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-warning" ng-disabled="formPeriodoSistema.$invalid" ng-class="{disabled: formPeriodoSistema.$invalid}">Modificar fechas</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
