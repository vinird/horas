<div class="col-xs-12">

  <div class="" ng-init="periodoSistema={{$periodoSistema}}; actividades={{$actividades}}; estudiantes={{$estudiantes}}; areas={{$areas}}; carreras={{$carreras}}; bancos={{$bancos}}; designaciones={{$designaciones}}; relacionActividadEstudiante={{$relacionActividadEstudiante}}; usuarios={{$usuarios}}; user_types={{ $user_types }}; userId={{ auth()->user()->id }}; this_user_type={{ auth()->user()->user_type }};">


    @if(auth()->user()->user_type == 1 || auth()->user()->user_type == 2 || auth()->user()->user_type == 3)
      <!-- Tabla estudiantes  -->
      @include('admin.estudiantes.tablaEstudiantes')
      <!--  -->
    @endif
    <br>
    @if(auth()->user()->user_type == 1 || auth()->user()->user_type == 4 )
      <!-- Tabla actividades -->
      @include('admin.actividades.tablaActividades')
      <!--  -->
    @endif
    <br>
    @if(auth()->user()->user_type == 1)
      <div class="row">
        @include('admin.carreras.tablaCarreras')
        @include('admin.areas.tablaAreas')
        @include('admin.bancos.tablaBancos')
      </div>
    @endif

  </div>

  <!-- Modal eliminar -->
  <div id="modalEliminar" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="tituloModal"></h4>
        </div>
        <form id="formularioEliminar" action="" class="form-horizontal" method="POST" enctype="multipart/form-data" autocomplete="off">
          <div class="modal-body">
            <!-- Token para formularios de Laravel -->
            {{ csrf_field() }}
            <!--  -->
            <!-- Método de envío -->
            <input type="hidden" name="_method" value="DELETE">
            <p id="textoModal" class="text-center"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button id="botonModal" type="submit" class="btn btn-danger"></button>
          </form>
        </div>
      </div>
    </div>

    <!-- /.modal -->

    <!-- /.modal -->
  </div>
    <!-- Modal detalles -->
    <div id="modalDetalle" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modalDetalleTitle">Detalle información</h4>
          </div>
          <div class="modal-body">
              <label id="modalDetalleLabel"></label>
              <p id="modalDetalleTexto"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>
@if(auth()->user()->user_type == 1)
  @include('admin.usuarios.vistaUsuarios')
@endif
@include('admin.usuarios.contrasena')
