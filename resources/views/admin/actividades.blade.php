@extends('layouts.app')

@section('content')
<!-- Mensajes de alerta, en el controlador se llaman utilizando flash('mensaje') -->



      <form action="{{ route('actividades.store') }}" class="form-horizontal" method="POST" autocomplete="off" id="formularioActividad">
        <!-- token para formularios de Laravel -->
        {{ csrf_field() }}
        <!--  -->
        <div class="form-group form-group-sm @if($errors->has('ref')) has-error @endif">
          <label for="ref" class="control-label col-sm-3">Referencia:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" name="ref" placeholder="Número de referencia de actividad o proyecto" value="{{ $nuevaRef }}" disabled required>
            @if($errors->has('ref'))
              <span class="help-block">{{ $errors->first('ref') }}</span>
            @endif
          </div>
        </div>
        <div class="form-group form-group-sm">
          <label for="sigla" class="control-label col-sm-3">Sigla:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" name="sigla" placeholder="Sigla de actividad o proyecto">
          </div>
        </div>
        <div class="form-group form-group-sm @if($errors->has('titulo')) has-error @endif	">
          <label for="titulo" class="control-label col-sm-3">Título:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" name="titulo" placeholder="Título de actividad o proyecto" required>
            @if($errors->has('titulo'))
              <span class="help-block">{{ $errors->first('titulo') }}</span>
            @endif
          </div>
        </div>
        <div class="form-group form-group-sm @if($errors->has('horas')) has-error @endif">
          <label for="horas" class="control-label col-sm-3">Total de horas:</label>
          <div class="col-sm-9">
            <input type="number" class="form-control" name="horas" value="1" min="1" max="20" required>
            @if($errors->has('horas'))
              <span class="help-block">{{ $errors->first('horas') }}</span>
            @endif
          </div>
        </div>
        <div class="form-group form-group-sm @if($errors->has('area')) has-error @endif">
          <label for="area" class="control-label col-sm-3">Área:</label>
          <div class="col-sm-9">
            <select class="form-control" name="area" required>
              @if(isset($areas))
              @if(count($areas) > 0)
              @foreach($areas as $area)
              <option value="{{ $area->id }}">{{ $area->nombre }}</option>
              @endforeach
              @endif
              @endif
            </select>
            @if($errors->has('area'))
              <span class="help-block">{{ $errors->first('area') }}</span>
            @endif
          </div>
        </div>
        <div class="form-group form-group-sm @if($errors->has('tipo')) has-error @endif">
          <label for="tipo" class="control-label col-sm-3">Tipo:</label>
          <div class="col-sm-9">
            @if(isset($designaciones))
            @if(count($designaciones) > 0)
            @foreach($designaciones as $designacion)
            <label class="radio-inline">
              <input type="radio" name="tipo" value="{{ $designacion->id }}" required> {{ $designacion->nombre }}
            </label>
            @endforeach
            @endif
            @endif
            @if($errors->has('banco'))
              <span class="help-block">{{ $errors->first('tipo') }}</span>
            @endif
          </div>
        </div>
        <hr>
        <div class="form-group form-group-sm @if($errors->has('descrip')) has-error @endif">
          <label for="descrip" class="control-label col-sm-3">Descripción:</label>
          <div class="col-sm-9">
            <textarea class="form-control" name="descrip" placeholder="Descripción de actividad o proyecto" required></textarea>
            @if($errors->has('descrip'))
              <span class="help-block">{{ $errors->first('descrip') }}</span>
            @endif
          </div>
        </div>
        <div class="form-group form-group-sm @if($errors->has('req')) has-error @endif">
          <label for="req" class="control-label col-sm-3">Requisitos:</label>
          <div class="col-sm-9">
            <textarea class="form-control" name="req" placeholder="Requisitos de actividad o proyecto" required></textarea>
            @if($errors->has('req'))
              <span class="help-block">{{ $errors->first('req') }}</span>
            @endif
          </div>
        </div>
        <hr>
        <div class="col-sm-offset-3">
          <button type="submit" class="btn btn-success btn-sm" id="boton_enviar">Agregar actividad</button>
        </div>
      </form>


@endsection
