<!-- Modal modificar actividades -->
<div id="modalActividadesModificar" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog " style="width: 96%;" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modificar Actividad</h4>
      </div>
      <form action="@{{'actividades/' + idActividad }}" class="form-horizontal" method="POST" autocomplete="off" id="formularioModificarActividad">
        <div class="modal-body">
          <!-- Token para formularios de Laravel -->
          {{ csrf_field() }}
          <!--  -->
          <!-- Método de envío -->
          <input type="hidden" name="_method" value="PUT">
          <!--  -->
          <div class="form-group form-group-sm">
            <label for="ref" class="control-label col-sm-2">Referencia:</label>
            <div class="col-sm-4 col-xs-12">
              <input type="text" class="form-control" name="ref" placeholder="Número de referencia de actividad o proyecto" ng-model="idActividad" disabled required>
            </div>
          </div>
          <div class="form-group form-group-sm @if($errors->has('titulo')) has-error @endif	">
            <label for="titulo" class="control-label col-sm-2">Título:</label>
            <div class="col-sm-4 col-xs-12">
              <input type="text" class="form-control" name="titulo" placeholder="Título de actividad o proyecto" ng-model="tituloActividad" required>
              @if($errors->has('titulo'))
                <span class="help-block">{{ $errors->first('titulo') }}</span>
              @endif
            </div>
          </div>
          <div class="form-group form-group-sm @if($errors->has('horas')) has-error @endif">
            <label for="horas" class="control-label col-sm-2">Total de horas:</label>
            <div class="col-sm-4 col-xs-12">
              <input type="number" class="form-control" name="horas" value="1" min="1" max="20" ng-model="horasActividad" required>
              @if($errors->has('horas'))
                <span class="help-block">{{ $errors->first('horas') }}</span>
              @endif
            </div>
          </div>
          <div class="form-group form-group-sm @if($errors->has('area')) has-error @endif">
            <label for="area" class="control-label col-sm-2">Áreas:</label>
            <div class="col-sm-4 col-xs-12">
              <select class="form-control" name="area" required>
                @if(isset($areas))
                @if(count($areas) > 0)
                @foreach($areas as $area)
                <option value="{{ $area->id }}" ng-selected="{{ $area->id }} == areaActividad">{{ $area->nombre }}</option>
                @endforeach
                @endif
                @endif
              </select>
              @if($errors->has('area'))
                <span class="help-block">{{ $errors->first('area') }}</span>
              @endif
            </div>
          </div>
          <div class="form-group form-group-sm @if($errors->has('Designación')) has-error @endif">
            <label for="Designación" class="control-label col-sm-2">Designación:</label>
            <div class="col-sm-9 col-xs-12">
              @if(isset($designaciones))
              @if(count($designaciones) > 0)
              @foreach($designaciones as $designacion)
              <label class="radio-inline">
                <input type="radio" name="designacion" value="{{ $designacion->id }}" ng-checked="{{ $designacion->id }} == designacionActividad" required> {{ $designacion->nombre }}
              </label>
              @endforeach
              @endif
              @endif
              @if($errors->has('banco'))
                <span class="help-block">{{ $errors->first('Designación') }}</span>
              @endif
            </div>
          </div>
          <hr>
          <div class="form-group form-group-sm @if($errors->has('descrip')) has-error @endif">
            <label for="descrip" class="control-label col-lg-2">Descripción:</label>
            <div class="col-lg-10 col-md-12">
              <textarea class="form-control textareaTrumbowyg" id="contentDescripcion" name="descrip" required>@{{ descripcionActividad }}</textarea>
              @if($errors->has('descrip'))
                <span class="help-block">{{ $errors->first('descrip') }}</span>
              @endif
            </div>
          </div>
          <div class="form-group form-group-sm @if($errors->has('req')) has-error @endif">
            <label for="req" class="control-label col-lg-2">Requisitos:</label>
            <div class="col-lg-10 col-md-12">
              <textarea class="form-control textareaTrumbowyg" id="contentRequisitos" name="req" ng-model="requisitosActividad" required></textarea>
              @if($errors->has('req'))
                <span class="help-block">{{ $errors->first('req') }}</span>
              @endif
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-warning btn-sm">Modificar actividad</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- MODAL AGREGAR ACTIVIDAD -->
<div class="modal fade" id="modalAgregarActividad" tabindex="-1" role="dialog">
  <div class="modal-dialog" style="width: 96%;" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Agregar nueva actividad</h4>
      </div>
      <div class="modal-body">
        <form action="{{ route('actividades.store') }}" class="form-horizontal" method="POST" autocomplete="off" id="formularioAgregarActividad">
              <!-- token para formularios de Laravel -->
              {{ csrf_field() }}
              <!--  -->
              <div class="form-group form-group-sm @if($errors->has('designacion')) has-error @endif">
                <label for="designacion" class="control-label col-sm-2">Designación:</label>
                <div class="col-sm-4 col-xs-12">
                  <select class="form-control" name="designacion">
                  @if(isset($designaciones))
                    @if(count($designaciones) > 0)
                      @foreach($designaciones as $designacion)
                        <option value="{{$designacion->id}}">{{$designacion->nombre}}</option>
                      @endforeach
                    @endif
                  @endif
                  </select>
                  @if($errors->has('designacion'))
                  <span class="help-block">{{ $errors->first('designacion') }}</span>
                  @endif
                </div>
              </div>
              <div class="form-group form-group-sm @if($errors->has('titulo')) has-error @endif">
                <label for="titulo" class="control-label col-sm-2">Título:</label>
                <div class="col-sm-4 col-xs-12">
                  <input type="text" class="form-control" name="titulo" placeholder="Título de la actividad" required>
                  @if($errors->has('titulo'))
                  <span class="help-block">{{ $errors->first('titulo') }}</span>
                  @endif
                </div>
              </div>
              <div class="form-group form-group-sm @if($errors->has('area')) has-error @endif">
                <label for="area" class="control-label col-sm-2">Área:</label>
                <div class="col-sm-4 col-xs-12">
                  <select class="form-control" name="area">
                    @if(isset($areas))
                    @if(count($areas) > 0)
                    @if(isset($relAreaUser))
                      @if(count($relAreaUser) > 0)
                        @foreach($areas as $area)

                          @foreach($relAreaUser as $relacion)
                            @if($area->id == $relacion->area )
                              <option value="{{$area->id}}">{{$area->nombre}}</option>
                            @endif
                          @endforeach

                        @endforeach
                      @else

                        @foreach($areas as $area)
                          <option value="{{$area->id}}">{{$area->nombre}}</option>
                        @endforeach

                      @endif
                    @else
                    @endif
                    @endif
                  @endif
                  </select>
                  @if($errors->has('area'))
                  <span class="help-block">{{ $errors->first('area') }}</span>
                  @endif
                </div>
              </div>
              <div class="form-group form-group-sm @if($errors->has('horas')) has-error @endif">
                <label for="horas" class="control-label col-sm-2">Horas:</label>
                <div class="col-sm-4 col-xs-12">
                  <input type="number" class="form-control" name="horas" min="1" max="20" placeholder="Horas de la actividad" required>
                  @if($errors->has('horas'))
                  <span class="help-block">{{ $errors->first('horas') }}</span>
                  @endif
                </div>
              </div>
              <div class="form-group form-group-sm @if($errors->has('descripcion')) has-error @endif">
                <label for="descripcion" class="control-label col-lg-2">Descripción:</label>
                <div class="col-lg-10 col-md-12">
                  <textarea class="form-control textareaTrumbowyg" name="descripcion" required></textarea>
                  @if($errors->has('descripcion'))
                  <span class="help-block">{{ $errors->first('descripcion') }}</span>
                  @endif
                </div>
              </div>
              <div class="form-group form-group-sm @if($errors->has('requisitos')) has-error @endif">
                <label for="requisitos" class="control-label col-lg-2">Requisitos:</label>
                <div class="col-lg-10 col-md-12">
                  <textarea class="form-control textareaTrumbowyg" name="requisitos" required></textarea>
                  @if($errors->has('requisitos'))
                  <span class="help-block">{{ $errors->first('requisitos') }}</span>
                  @endif
                </div>
              </div>

              <div class="pull-right">
                <button type="submit" class="btn btn-success btn-sm" id="boton_enviar">Agregar actividad</button>
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancelar</button>
              </div>
            </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<!-- Modal estudiantes - actividades -->
<div id="modalEstudiantesActividades" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Estudiantes participando en la actividad</h4>
      </div>
      <div class="modal-body">
        <h4 class="text-center" id="tituloModalEstudiantesActividades"></h4>
        <div ng-if="relEstudianteActivida.length > 0">
          <small><i class="fa fa-asterisk" aria-hidden="true"> </i> Estudiantes elegidos para trabajar en la actividad o proyecto</small>
          <br><br>
          <table class="table table-hover">
            <thead>
              <th>Nombre</th>
              <th>Carnet</th>
              <th>Correo</th>
              <th>Nivel</th>
              <th>Promedio</th>
              <th class="text-center">Aprobar solicitud</th>
            </thead>
            <tbody>
              <tr ng-repeat="estudiante in relEstudianteActivida" ng-class="{'success': estudiante.elegido == true}">
                <td><span ng-if="estudiante.elegido == true"><i class="fa fa-asterisk" aria-hidden="true"> </i> </span> @{{ estudiante.nombre }} @{{ estudiante.apellidos }}</td>
                <td>@{{ estudiante.carnet }}</td>
                <td>@{{ estudiante.correo }}</td>
                <td>@{{ estudiante.nivel }}</td>
                <td>@{{ estudiante.promedio }}</td>
                <td class="text-center">
                  <span ng-if="estudiante.elegido == true">
                    <form action="@{{ 'eliminarEstudianteElegido' }}" method="POST">
                      <!-- token para formularios de Laravel -->
                      {{ csrf_field() }}
                      <!--  -->
                      <input name="fkActividad" type="text" class="hidden" ng-model="actividadIdRelEstudiante">
                      <input name="fkEstudiante" type="text" class="hidden" value="@{{ estudiante.id }}">
                      <button class="btn btn-sm btn-primary">Quitar</button>
                    </form>
                  </span>
                  <span ng-if="estudiante.elegido == false">
                    <form action="@{{ 'setEstudianteElegido' }}" method="POST">
                      <!-- token para formularios de Laravel -->
                      {{ csrf_field() }}
                      <!--  -->
                      <input name="fkActividad" type="text" class="hidden" ng-model="actividadIdRelEstudiante">
                      <input name="fkEstudiante" type="text" class="hidden" value="@{{ estudiante.id }}">
                      <button class="btn btn-sm btn-default">Elegir</button>
                    </form>
                  </span>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
