  <div class="" ng-controller="formularioActividad">
    <!-- contenedor -->
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
          <h2>Actividades y/o proyectos <small>Lista de actividades</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-sort" aria-hidden="true"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li>
                    <a ng-click="myOrderBy('ref')">Ref.</a>
                  </li>
                  <li>
                    <a ng-click="myOrderBy('fkDesignacion')">Designación</a>
                  </li>
                  <li>
                    <a ng-click="myOrderBy('titulo')">Título</a>
                  </li>
                  <li>
                    <a ng-click="myOrderBy('fkArea')">Área</a>
                  </li>
                  <li>
                    <a ng-click="myOrderBy('horas')">Horas</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="dashboard-widget-content">
              <div class="col-xs-12">


                <div class="table-responsive center-block" id="infoActividades">
                  <div class="col-lg-6">
                    <div class="input-group">
                      <input ng-model="filtroActividades" type="text" class="form-control" placeholder="Buscar...">
                      <span class="input-group-btn">
                        <button class="btn btn-default" disabled style="cursor: default" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                      </span>
                    </div><!-- /input-group -->
                  </div><!-- /.col-lg-6 -->
                  <br>
                  <br>
                  <br>
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>Ref.</th>
                        <th>Designación</th>
                        <th>Título</th>
                        <th>Área</th>
                        <th>Horas</th>
                        <th class="text-center">Estudiantes en actividad</th>
                        <th class="text-center">Descripción</th>
                        <th class="text-center">Requisitos</th>
                        <th id="thPro"></th>
                      </tr>
                    </thead>
                    <tbody>

                      <tr pagination-id="actividadesPagination" dir-paginate="actividad in actividades | orderBy : orderActividad | filter : filtroActividades | itemsPerPage: actividadesXPagina">
                        <td>@{{actividad.ref}}</td>
                        <td ng-repeat="designacion in designaciones" ng-if="designacion.id == actividad.fkDesignacion">
                          @{{designacion.nombre}}
                        </td>
                        <td>@{{actividad.titulo}}</td>
                        <td ng-repeat="area in areas" ng-if="area.id == actividad.fkArea">
                          <i class="fa fa-tag" aria-hidden="true" style="color: @{{area.color}}"></i> @{{area.nombre}}
                        </td>
                        <td>@{{actividad.horas}}</td>

                        <td class="text-center"><a ng-click="cargarEstudiantesActividades(actividad.id)" class="btn btn-default btn-xs">abrir <i class="fa fa-external-link" aria-hidden="true">

                        </i></a></td>
                        <td class="text-center">
                          <a href=""><i class="fa fa-eye" data-toggle="modal" data-target="#modalDetalle" ng-click="cargarDetalleModal(actividad.titulo, 'Descripcion:', actividad.descripcion)" aria-hidden="true"></i></a>
                        </td>
                        <td class="text-center">
                          <a href=""><i class="fa fa-eye" data-toggle="modal" data-target="#modalDetalle" ng-click="cargarDetalleModal(actividad.titulo, 'Requisitos:', actividad.requisitos)" aria-hidden="true"></i></a>
                        </td>
                        <td>
                          <span ng-if="this_user_type == 1 || actividad.created_by == userId">
                            <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalActividadesModificar" ng-click="editarActividad(actividad.id, actividad.titulo, actividad.horas, actividad.fkArea, actividad.fkDesignacion, actividad.descripcion, actividad.requisitos)"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                          </span>
                          @if(auth()->user()->user_type == 1)
                          <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalEliminar" ng-click="eliminarActividad(actividad.id, actividad.titulo)"><i class="fa fa-trash" aria-hidden="true"></i></button>
                          @endif
                        </td>
                      </tr>
                    </tbody>
                  </table>
                 <!--  <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalAgregarActividad">Agregar
                    <i class="fa fa-plus" aria-hidden="true"></i>
                  </button> -->
                  <center><dir-pagination-controls pagination-id="actividadesPagination"></dir-pagination-controls></center>
                </div>


              </div>
              <!-- <div id="world-map-gdp" class="col-md-8 col-sm-12 col-xs-12" style="height:230px;"></div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Fin de contenedor -->



    @include('admin.actividades.modalActividades')
  </div>
