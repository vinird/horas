@extends('layouts.app')
<script src="/js/jscolor.js"></script>
@section('content')
<!-- Mensajes de alerta, en el controlador se llaman utilizando flash('mensaje') -->
@if (session()->has('flash_notification.message'))
@endif
<div class="modal fade" id="modalAgregarCategoria" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="container">
          <div class="row">
            <form action="{{ route('categorias.store') }}" class="form-horizontal" method="POST" autocomplete="off" id="formularioCategoria" ng-controller="formularioCategoria">
              <!-- token para formularios de Laravel -->
              {{ csrf_field() }}
              <!--  -->
              <div class="form-group form-group-sm @if($errors->has('nombre')) has-error @endif">
                <label for="nombre" class="control-label col-sm-3">Nombre:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="nombre" placeholder="Nombre de la área" required>
                  @if($errors->has('nombre'))
                  <span class="help-block">{{ $errors->first('nombre') }}</span>
                  @endif
                </div>
              </div>
              <div class="form-group form-group-sm @if($errors->has('color')) has-error @endif">
                <label for="color" class="control-label col-sm-3">Color:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control jscolor" name="color" value="41ADE7" placeholder="Color de la área" required>
                  @if($errors->has('color'))
                  <span class="help-block">{{ $errors->first('color') }}</span>
                  @endif
                </div>
              </div>

              <div class="col-sm-offset-3">
                <button type="submit" class="btn btn-success btn-sm" id="boton_enviar">Agregar área</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection