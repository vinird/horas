<!DOCTYPE html>
<html lang="es" ng-app="Horas">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  <!-- Styles -->
  <link href="/css/app.css" rel="stylesheet" />
  <link rel="shortcut icon" type="image/x-icon" href="/img/ucr-favicon.png"/>
  <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css" />
  <link rel="stylesheet" type="text/css" href="/css/include/bootstrap-datetimepicker.min.css" />
  <link rel="stylesheet" type="text/css" href="{{ asset('css/include/trumbowyg.min.css') }}">

  <!-- Template custom styles -->
  <link rel="stylesheet" type="text/css" href="{{ asset('css/include/custom.min.css') }}">

  <!-- Scripts -->
  <script src = "/js/include/jquery.min.js"></script>
  <script src = "/js/include/angular.min.js"></script>
  <script src = "/js/include/dirPagination.js"></script>
  <script src = "/js/include/moment.min.js"></script>
  <script src = "/js/include/bootstrap.min.js"></script>
  <script src = "/js/include/bootstrap-datetimepicker.min.js"></script>
  <script src = "/js/include/trumbowyg.min.js"></script>
  <script src = "/js/include/es.js"></script>

  <script src= "/js/controllers/homeController.js"></script>
  <script src="/js/controllers/estudiante.js"></script>
  <script src = "/js/controllers/area.js"></script>
  <script src = "/js/controllers/actividad.js"></script>
  <script src = "/js/controllers/banco.js"></script>
  <script src = "/js/controllers/usuario.js"></script>
  <script src = "/js/controllers/carrera.js"></script>
  <script src = "/js/script.js"></script>

  <script>
    window.Laravel = <?php echo json_encode([
      'csrfToken' => csrf_token(),
      ]); ?>
  </script>

  <title>Horas estudiante y horas asistente</title>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="" class="site_title"><i class="fa fa-calendar-o" aria-hidden="true"></i> <span>HORAS</span></a>
            </div>

            <div class="clearfix"></div>


            <br />

            <!-- sidebar menu -->
            @include('admin.partials.sub.sideBar')
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
        @include('admin.partials.sub.topNavigation')
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          @include('admin.partials.sub.topData')
          <!-- /top tiles -->
          <br />
          <div class="row">

            @include('admin.partials.home')

          </div>
        </div>
        <!-- footer content -->
      </div>
    </div>
    <div class="fonNar"></div>
    <footer>
      <div class="pull-left">
        <img src="/img/firma-footer.png">
      </div>
      <div class="pull-right firmaHoras">
        <h5>&#169; UCR - Horas estudiante y horas asistente</h5>
      </div>
      <div class="clearfix"></div>
    </footer>

    <!-- Template custom script -->
    <script src="{{ asset('js/include/custom.min.js') }}"></script>
    <!--  -->

    <!-- Llama modal para cambiar periodos del sistema -->
    <!-- Llama modales para insertar promedios y niveles -->
    @include("admin.partials.modalesExcel")

    @if(auth()->user()->user_type == 1)
      @include("admin.partials.modalPeriodoSistema")

      <!-- Llama modal para limpiar base de datos -->
      @include("admin.partials.modalLimpiarBD")
    @endif
  </body>
  </html>
