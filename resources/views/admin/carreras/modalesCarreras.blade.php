<!-- MODAL AGREGAR CARERA -->
<div class="modal fade" id="modalAgregarCarrera" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Agregar nueva carrera</h4>
            </div>
            <div class="modal-body">
                <form action="{{ route('carreras.store') }}" class="form-horizontal" method="POST" autocomplete="off" id="formularioCarrera" name="formAgregarCarrera">
                    <!-- token para formularios de Laravel -->
                {{ csrf_field() }}
                <!--  -->
                    <div class="form-group form-group-sm @if($errors->has('nombre')) has-error @endif">
                        <label for="nombre" class="control-label col-sm-3">Nombre:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nombre" placeholder="Nombre de la carrera" required>
                            <span style="color:red" ng-show="formAgregarCarrera.nombre.$dirty && formAgregarCarrera.nombre.$invalid">
                                <span class="error" ng-show="formAgregarCarrera.nombre.$error.required">El nombre de la carrera es requerido</span>
                            </span>
                            @if($errors->has('nombre'))
                                <span class="help-block">{{ $errors->first('nombre') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group form-group-sm @if($errors->has('sigla')) has-error @endif">
                        <label for="sigla" class="control-label col-sm-3">Sigla:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="sigla" placeholder="Sigla de la carrera" required>
                            <span style="color:red" ng-show="formAgregarCarrera.sigla.$dirty && formAgregarCarrera.sigla.$invalid">
                                <span class="error" ng-show="formAgregarCarrera.sigla.$error.required">El sigla de la carrera es requerido</span>
                            </span>
                            @if($errors->has('sigla'))
                                <span class="help-block">{{ $errors->first('sigla') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success btn-sm" ng-disabled="formAgregarCarrera.$invalid" ng-class="{disabled: formAgregarCarrera.$invalid}">Agregar carrera</button>
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<!-- MODAL MODIFICAR BANCO -->
<div class="modal fade" id="modalModificarCarrera" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modificar carrera</h4>
            </div>
            <div class="modal-body">
                <form action="@{{ 'carreras/' + modificarCarreraID }}" class="form-horizontal" method="POST" autocomplete="off" id="formularioModificarCarrera" name="formModificarCarrera">
                    <!-- token para formularios de Laravel -->
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PUT">
                <!--  -->
                    <div class="form-group form-group-sm @if($errors->has('nombre')) has-error @endif">
                        <label for="nombre" class="control-label col-sm-3">Nombre:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control validate" name="nombre" placeholder="Nombre de la carrear" ng-model="modificarNombreCarrera" required>
                            <span style="color:red" ng-show="formModificarCarrera.nombre.$dirty && formModificarCarrera.nombre.$invalid">
                                <span class="error" ng-show="formModificarCarrera.nombre.$error.required">El nombre de la carrera es requerido</span>
                            </span>
                            @if($errors->has('nombre'))
                                <span class="help-block">{{ $errors->first('nombre') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group form-group-sm @if($errors->has('sigla')) has-error @endif">
                        <label for="sigla" class="control-label col-sm-3">Sigla:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" ng-model="modificarSiglaCarrera" name="sigla" placeholder="Sigla de la carrera" required>
                            <span style="color:red" ng-show="formAgregarCarrera.sigla.$dirty && formAgregarCarrera.sigla.$invalid">
                                <span class="error" ng-show="formAgregarCarrera.sigla.$error.required">El sigla de la carrera es requerido</span>
                            </span>
                            @if($errors->has('sigla'))
                                <span class="help-block">{{ $errors->first('sigla') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-warning btn-sm" id="boton_enviar" ng-disabled="formModificarCarrera.$invalid" ng-class="{disabled: formModificarCarrera.$invalid}">Modificar carrera</button>
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
