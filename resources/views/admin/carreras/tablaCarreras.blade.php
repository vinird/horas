<div ng-controller="carreraCtrl">
 <!-- contenedor -->
 <div class="col-lg-4 col-md-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Carreras <br><small>Lista de carreras</small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-sort" aria-hidden="true"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li>
              <a ng-click="myOrderBy('nombre')">Nombre</a> 
            </li>
            <li>
              <a ng-click="myOrderBy('sigla')">Sigla</a> 
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <div class="dashboard-widget-content">
        <div class="col-xs-12">


          <div class="table-responsive center-block" id="nombreBancos">
            <br>
            <table class="table table-hover table-bordered">
              <thead>
                <tr>
                  <th id="thNom">Nombre de la carrera</th>
                  <th>Sigla</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="carrera in carreras | orderBy : orderCarrera">
                  <td>@{{carrera.nombre}}</td>
                  <td>@{{carrera.sigla}}</td>
                  <td>
                    <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalModificarCarrera" ng-click="cargarModalModificarCarrera(carrera.id, carrera.nombre, carrera.sigla)">
                      <i class="fa fa-pencil" aria-hidden="true"></i>
                    </button>
                    <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalEliminar" ng-click="eliminarCarrera(carrera.id, carrera.nombre)"><i class="fa fa-trash" aria-hidden="true"></i></button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalAgregarBanco">Agregar
            <i class="fa fa-plus" aria-hidden="true"></i>
          </button> -->


        </div>
        <!-- <div id="world-map-gdp" class="col-md-8 col-sm-12 col-xs-12" style="height:230px;"></div> -->
      </div>
    </div>
  </div>
</div>
<!-- Fin de contenedor -->
@include('admin.carreras.modalesCarreras')
</div>
