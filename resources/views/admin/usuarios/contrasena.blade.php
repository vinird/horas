<div id="modalModificarContrasena" class="modal fade" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close center-text" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <p>Cambio de contraseña</p>
      </div>
      <form  action="{{ url('contrasena') }}" class="form-horizontal" method="POST">
        <div class="modal-body">
          <!-- Token para formularios de Laravel -->
          {{ csrf_field() }}
          <!--  -->
          <!-- Método de envío -->
          <!--  -->
          <div class="form-group">
            <label for="antiguaContrasena" class="col-sm-5 control-label">Antigua contraseña: </label>
            <div class="col-sm-6">
              <input type="password" class="form-control input-sm" name="antiguaContrasena" required>
            </div>
          </div>
          <hr>
          <div class="form-group">
            <label for="nuevaContrasena" class="col-sm-5 control-label">Nueva contraseña: </label>
            <div class="col-sm-6">
              <input type="password" class="form-control input-sm" name="nuevaContrasena" required minlength="6">
            </div>
          </div>
          <div class="form-group">
            <label for="confirmarNuevaContrasena" class="col-sm-5 control-label">Confirmar nueva contraseña: </label>
            <div class="col-sm-6">
              <input type="password" class="form-control input-sm" name="confirmarNuevaContrasena" required minlength="6">
              <span class="help-block">Las contraseñas deben tener 6 carácteres o más.</span>
            </div>
          </div>


        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-sm btn-warning" >Modificar</button>
      </div>
      </form>
    </div>
  </div>
</div>
