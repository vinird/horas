<div ng-controller="usuarioCtrl" ng-init="relAreaUser={{ $relAreaUser }};">
<div id="modalUsuarios" class="modal fade" role="dialog" >
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
        <div class="modal-body table-responsive">

        <button class="btn btn-primary btn-sm" type="button" data-toggle="collapse" data-target="#collapseAgregarUsuario" aria-expanded="false" aria-controls="collapseExample">
          Nuevo usuario <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>
        </button>
        <hr>
        <div class="collapse" id="collapseAgregarUsuario">
          <div class="well">

            Formulario
            <form action="{{ url('usuarios') }}" method="POST" class="form-horizontal">
              {{ csrf_field() }}
              <div class="form-group">
                <label for="nombre" class="col-sm-2 control-label">Nombre: </label>
                <div class="col-sm-10">
                  <input type="text" class="form-control input-sm" name="nombre" placeholder="Digite el nombre.." required>
                </div>
              </div>
              <div class="form-group">
                <label for="apellido" class="col-sm-2 control-label">Apellido: </label>
                <div class="col-sm-10">
                  <input type="text" class="form-control input-sm" name="apellido" placeholder="Digite el apellido.." required>
                </div>
              </div>
              <div class="form-group">
                <label for="correo" class="col-sm-2 control-label">Correo: </label>
                <div class="col-sm-10">
                  <input type="email" class="form-control input-sm" name="correo" placeholder="Correo electrónico.." required>
                </div>
              </div>
              <div class="form-group">
                <label for="tipo" class="col-sm-2 control-label">Tipo de usuario: </label>
                <div class="col-sm-10">
                  <select name="tipo" class="form-control input-sm" ng-click="tipoChange(this)">
                    <option value="">-- Seleccione una opción --</option>
                      <option ng-repeat="user_t in user_types" value="@{{ user_t.id }}"  ng-if="user_t.id != 1" class="addUserTypeChoice">@{{ user_t.nombre }}</option>
                  </select>
                </div>
              </div>

              <div class="form-group hide" id="divAreasUsuarios">
                <label for="areas" class="col-sm-3 control-label">Áreas: </label>
                <div class="col-sm-9">
                  <!-- <select name="area" class="form-control input-sm" multiple="multiple" > -->
                    <span ng-repeat="area in areas">
                      <input name="areas[@{{ area.id }}]" type="checkbox" value="@{{ area.id }}" class="selectAreasUser"> @{{ area.nombre }}&nbsp;&nbsp;&nbsp;
                    </span>
                  <!-- </select> -->
                  <span class="help-block">
                    <p>Seleccione el tipo de área de actividades a la que pertenece este usuario</p>
                  </span>
                </div>
              </div>

              <div class="form-group">
                <label for="contrasena" class="col-sm-2 control-label">Contraseña: </label>
                <div class="col-sm-10">
                  <input type="password" class="form-control input-sm" name="contrasena" placeholder="Contraseña" required minlength="6">
                </div>
              </div>
              <div class="form-group">
                <label for="confirmarContrasena" class="col-sm-2 control-label">Confirmar: </label>
                <div class="col-sm-10">
                  <input type="password" class="form-control input-sm" name="confirmarContrasena" placeholder="Confirmar contraseña" required minlength="6">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <div class="checkbox">
                    <label>
                      <input name="activo" type="checkbox"> Activo
                    </label>
                  </div>
                  <span class="help-block">Si el usuario no está activo no podrá ingresar al sistema.</span>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-success btn-sm">Agregar</button>
                </div>
              </div>
            </form>
          </div>
        </div>
          <!-- Token para formularios de Laravel -->
          <!--  -->
          <!-- Método de envío -->
          <table class="table table-hover ">
            <thead>
              <tr>
                <th id="thNom">Nombre</th>
                <th id="thAp">Apellido</th>
                <th id="thMail">Correo electrónico</th>
                <th id="thTip">Tipo</th>
                <th>Estado</th>
                <th class="text-center">Áreas de actividades</th>
                <th id="thPro"></th>
              </tr>
            </thead>
            <tbody>
            <form id="formularioUsuariosVista" action="{{ url('usuarios/delete') }}" method="POST" autocomplete="off">
              {{ csrf_field() }}
              <input type="text" name="id" value="" class="hide" id="inputDeletUserId">
            </form>


              <tr ng-repeat="usuario in usuarios">
                <td>@{{usuario.name}}</td>
                <td>@{{usuario.apellidos}}</td>
                <td>@{{usuario.email}}</td>
                <td ng-repeat="user_type in user_types" ng-if="user_type.id == usuario.user_type">
                  @{{ user_type.nombre }}
                </td>
                <td>
                  <div ng-if="usuario.activo == 1">
                    <div class="text-success">Activo</div>
                  </div>
                  <div ng-if="usuario.activo != 1">
                    <div class="text-danger">Desactivado</div>
                  </div>
                </td>
                <td class="text-center" ng-click="setPopOverData(usuario.id)">
                  <a ng-if="usuario.user_type == 4" href="" data-toggle="popover" data-trigger="focus" title=" -- Nombres de las áreas del usuario -- " data-html="true" data-placement="top" data-content='
                    <i class="popOverAreasUser"><br></i>
                  '><i class="fa fa-eye" aria-hidden="true"></i>
                  </a>
                </td>
                <td>
                  <button type="button" class="btn btn-default btn-xs" data-toggle="collapse" data-target=".collapseModificarUsuario" aria-expanded="false" ng-click="modificarUsuario(usuario.id, usuario.name, usuario.apellidos, usuario.email)" >
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                  </button>
                  <button type="button" class="btn btn-default btn-xs popover-class" ng-click="eliminarUsuario(usuario.id,usuario.email)" data-toggle="popover" data-trigger="focus" title="¿Desea eliminar el usuario?" data-html="true" data-placement="left" data-content="

                  <a  class='btn btn-danger btn-sm btn-group-justified' onclick='btnSubmitEliminarUser();'>Eliminar</a>

                  " ><i class="fa fa-trash" aria-hidden="true" ></i></button>

       <!--            <a tabindex="0" class="btn btn-lg btn-danger" role="button" data-toggle="popover" data-trigger="focus" title="Dismissible popover" data-content="And here's some amazing content. It's very engaging. Right?">Dismissible popover</a>
 -->
                </td>
              </tr>

            </tbody>
          </table>
          <div class="collapse collapseModificarUsuario">
            <div class="well">
            Modificar usuario

            <form action="@{{ 'usuarios/'+modificarId }}" method="POST" class="form-horizontal">
              {{ csrf_field() }}
              <input name="_method" type="hidden" value="PUT">
              <div class="form-group">
                <label for="nombre" class="col-sm-2 control-label">Nombre: </label>
                <div class="col-sm-10">
                  <input type="text" class="form-control input-sm" name="nombre" ng-model="modficarNombre" required>
                </div>
              </div>
              <div class="form-group">
                <label for="apellido" class="col-sm-2 control-label">Apellido: </label>
                <div class="col-sm-10">
                  <input type="text" class="form-control input-sm" name="apellido" ng-model="modificarApellido" required>
                </div>
              </div>
              <div class="form-group">
                <label for="correo" class="col-sm-2 control-label">Correo: </label>
                <div class="col-sm-10">
                  <input type="email" class="form-control input-sm" name="correo" ng-model="modificarCorreo" required>
                </div>
              </div>
              <div class="form-group">
                <label for="activo" class="col-sm-2 control-label">Estado: </label>
                <div class="col-sm-10">
                  <select name="activo" class="form-control input-sm" >
                    <option value="">-- Seleccione una opción --</option>
                    <option value="0" class="text-default">Desactivado</option>
                    <option value="1" class="text-success">Activado</option>
                  </select>
                  <span class="help-block">Si el usuario no está activo no podrá ingresar al sistema.</span>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-warning btn-sm">Modificar</button>
                </div>
              </div>
            </form>

            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
  </div>
</div>
</div>
  @include("admin.usuarios.modalesUsuario")
