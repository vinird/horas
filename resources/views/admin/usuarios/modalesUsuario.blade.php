<div id="modalEliminar" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="tituloModal"></h4>
      </div>
      <form id="formularioEliminar" action="" class="form-horizontal" method="POST" enctype="multipart/form-data" autocomplete="off">
        <div class="modal-body">
          <!-- Token para formularios de Laravel -->
          {{ csrf_field() }}
          <!--  -->
          <!-- Método de envío -->
          <input type="hidden" name="_method" value="DELETE">
          <p id="textoModal"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
          <button id="botonModal" type="submit" class="btn btn-danger"></button>
      </div>
      </form>
    </div>
  </div>
</div>
