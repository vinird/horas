<div class="modal fade" id="modalModificarArea" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modificar área de la actividad</h4>
      </div>
      <div class="modal-body">
        <form action="@{{ 'areas/'+ modificarAreaID }}" class="form-horizontal" method="POST" autocomplete="off" id="formularioArea">
          <!-- token para formularios de Laravel -->
          {{ csrf_field() }}
          <!--  -->
          <!-- Método de envío -->
            <input type="hidden" name="_method" value="PUT">
          <!--  -->
          <div class="form-group form-group-sm @if($errors->has('nombre')) has-error @endif">
            <label for="nombre" class="control-label col-sm-3">Nombre:</label>
            <div class="col-sm-9">
            <input type="text" class="form-control" name="nombre" placeholder="Nombre del área" ng-model="modificarNombreArea" required>
              @if($errors->has('nombre'))
              <span class="help-block">{{ $errors->first('nombre') }}</span>
              @endif
            </div>
          </div>
          <div class="form-group form-group-sm @if($errors->has('color')) has-error @endif">
            <label for="color" class="control-label col-sm-3">Color:</label>
            <div class="col-sm-9">
              <input type="color" class="form-control jscolor" name="color" placeholder="Color del área" ng-model="modificarColorArea" required>
              @if($errors->has('color'))
              <span class="help-block">{{ $errors->first('color') }}</span>
              @endif
            </div>
          </div>

          <div class="pull-right">
            <button type="submit" ng-disabled="!modificarNombreArea" class="btn btn-warning btn-sm" id="boton_enviar">Modificar área</button>
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancelar</button>
          </div>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<!-- MODAL AGREGAR AREA -->
<div class="modal fade" id="modalAgregarArea" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Agregar nueva área</h4>
      </div>
      <div class="modal-body">
        <form action="{{ route('areas.store') }}" class="form-horizontal" method="POST" autocomplete="off" id="formularioArea">
              <!-- token para formularios de Laravel -->
              {{ csrf_field() }}
              <!--  -->
              <div class="form-group form-group-sm @if($errors->has('nombre')) has-error @endif">
                <label for="nombre" class="control-label col-sm-3">Nombre:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="nombre" placeholder="Nombre del área" ng-model="someText" required>
                  @if($errors->has('nombre'))
                  <span class="help-block">{{ $errors->first('nombre') }}</span>
                  @endif
                </div>
              </div>
            <div class="form-group form-group-sm @if($errors->has('color')) has-error @endif">
                <label for="color" class="control-label col-sm-3">Color:</label>
                <div class="col-sm-9">
                  <input type="color" class="form-control" name="color" value="#2095f2" placeholder="Color del area" required>
                  @if($errors->has('color'))
                  <span class="help-block">{{ $errors->first('color') }}</span>
                  @endif
                </div>
              </div>

              <div class="pull-right">
                <button ng-disabled="!someText" type="submit" class="btn btn-success btn-sm" id="boton_enviar">Agregar área</button>
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancelar</button>
              </div>
            </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
