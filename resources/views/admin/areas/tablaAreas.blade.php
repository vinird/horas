<div ng-controller="areasCtrl">


  <!-- contenedor -->

  <div class="col-lg-4 col-md-6 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Áreas de actividades y/o proyectos <br class=""> <small>Lista de áreas</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-sort" aria-hidden="true"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a ng-click="myOrderBy('nombre')">Nombre</a>
              </li>
              <li><a ng-click="myOrderBy('color')">Color</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="dashboard-widget-content">
          <div class="col-xs-12">


            <div class="table-responsive center-block" id="infoEstudiantes">
              <br>
              <table class="table table-hover table-bordered">
                <thead>
                  <tr>
                    <th id="thNom">Nombre</th>
                    <th></th>
                    <th id="thPro"></th>
                  </tr>
                </thead>
                <tbody>
                  <tr ng-repeat="area in areas | orderBy : orderArea">
                    <td>@{{area.nombre}}</td>
                    <td><i class="fa fa-tag" aria-hidden="true" style="color: @{{area.color}}"></i></td>
                    <td>
                      <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalModificarArea" ng-click="cargarModalModificar(area.id, area.nombre, area.color)">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                      </button>
                      <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalEliminar" ng-click="eliminarArea(area.id, area.nombre)"><i class="fa fa-trash" aria-hidden="true"></i></button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalAgregarArea">Agregar
              <i class="fa fa-plus" aria-hidden="true"></i>
            </button> -->


          </div>
          <!-- <div id="world-map-gdp" class="col-md-8 col-sm-12 col-xs-12" style="height:230px;"></div> -->
        </div>
      </div>
    </div>
  </div>
  <!-- Fin de contenedor -->



  @include("admin.areas.modalesArea")
</div>
