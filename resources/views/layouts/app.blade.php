<!DOCTYPE html>
<html lang="es" ng-app="Horas">
<head>
    <meta charset="utf-8" /> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet" />
    <link rel="shortcut icon" type="image/x-icon" href="/img/ucr-favicon.png"/>
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="/css/include/bootstrap-datetimepicker.min.css" />

    <!-- Template custom styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/include/custom.min.css') }}">
    <!--  -->

    <!-- Scripts -->
    <script src="/js/app.js"></script>
    <script src = "/js/include/jquery.min.js"></script>
    <script src = "/js/include/angular.min.js"></script>
    <script src = "/js/include/moment.min.js"></script>
    <script src="/js/include/es.js"></script>
    <script src = "/js/include/bootstrap-datetimepicker.min.js"></script>
    <script src= "/js/controllers/homeController.js"></script>
    <script src="/js/controllers/estudiante.js"></script>
    <script src = "/js/controllers/area.js"></script>
    <script src = "/js/controllers/actividad.js"></script>
    <script src = "/js/controllers/banco.js"></script>
    <script src = "/js/controllers/usuario.js"></script>
    <script src = "/js/script.js"></script>

    <title>Horas estudiante y horas asistente</title>

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <style type="text/css">

        html, body {
            background-color: #fff;
            letter-spacing: .5px;
        }
        .navbar-header{
            background-color: #00C0F3;
        }

        .fonNav{
            background: #00c0f3;
            padding-bottom: 5px;
        }
        .menu-bars{
            font-size: 23px;
            color: #fff;
        }

        .content {
            text-align: center;
        }

        .nav-link {
            color: #fff;
            padding: 0 25px;
            font-size: 11px;
            letter-spacing: .2rem;
            margin-top: 8px;
            text-decoration: none;
            text-transform: uppercase;
            margin-right: 10px;
        }
        .navbar-brand img{
            margin-left: 20px;
        }
        .home-icon i{
            font-size: 18px;
        }
        .nav-link:hover{
            background-color: transparent!important;
            color: #005da4;
        }
    </style>
</head>
<body>
    <header>
        <nav class="navbar fonNav">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <i class="fa fa-bars menu-bars" aria-hidden="true"></i>
                    </button>
                    <a class="navbar-brand" href="/"><img src="/img/firma-ucr.png"></a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        @if (Route::has('login'))
                        <li><a href="{{ url('/login') }}" class="nav-link">Ingresar</a></li>
                        @endif
                        <li><a class="home-icon nav-link" href="/"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </header>
    <br>
    @include('flash::message')

    @yield('content')
    <br><br>
</body>
</html>
