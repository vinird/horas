<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Example 2</title>
    <link rel="stylesheet" type="text/css" href="css/pdf.css">
  </head>
  <body>

    <main>
      <div id="details" class="clearfix">
        <div id="invoice">
          <h1>PDF</h1>
          <div class="date">Fecha del documento: {{ $date }}</div>
        </div>
      </div>

      <table>
        <tbody>
          <tr>
            <td><strong>Nombre:</strong></td><td>{{$estudiante->nombre}}</td>
            <td><strong>Apellido:</strong></td><td>{{$estudiante->apellidos}}</td>
          </tr>
          <tr>
            <td><strong>Cedula:</strong></td><td>{{$estudiante->cedula}}</td>
            <td><strong>Carné:</strong></td><td>{{$estudiante->carnet}}</td>
          </tr>
          <tr>
            <td><strong>Correo:</strong></td><td>{{$estudiante->correo}}</td>
          </tr>
        </tbody>
      </table>

      <br><br>
      <h3>Actividades</h3>
      <table >
        <thead>
          <tr>
            <th>Id</th>
            <th>Titulo</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($actividades as $actividad)
          <tr>
            <td>
              {{$actividad[0]->id}}
            </td>
            <td>
              {{$actividad[0]->titulo}}
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
  </body>
</html>
