<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Horas estudiante y horas asistente</title>
    <link rel="shortcut icon" type="image/x-icon" href="/img/ucr-favicon.png"/>
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css">

    <!-- bootstrap -->
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    <script src = "/js/app.js"></script>

    <!-- Styles -->
    <style>
        @font-face {
          font-family: myriad;
          src: url(fonts/Myriad-Pro.ttf);
        }

        html, body {
            background-color: #fff;
            font-family: 'myriad';
            letter-spacing: .5px;
        }

        .fonNav{
            background: #00c0f3;
            padding-bottom: 5px;
        }
        .menu-bars{
            font-size: 23px;
            color: #fff;
        }

        .content {
            text-align: center;
        }

        .nav-link {
            color: #fff;
            padding: 0 25px;
            font-size: 11px;
            letter-spacing: .2rem;
            margin-top: 8px;
            text-decoration: none;
            text-transform: uppercase;
        }
        .navbar-brand img{
            margin-left: 20px;
        }
        @keyframes btn-formulario {
            from {background-color: white;}
            to {background-color: #43ADE7;}
        }
        .links2{
            padding-bottom: 80px;
        }
        .links2 > a {
            color: #333;
            font-size: 18px;
            font-weight: 300;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: capitalize;
            display: inline-block;
            border: solid #00c0f3 2px;
            padding-top: 5px;
            padding-bottom: 5px;
            padding-left: 30px;
            padding-right: 30px;
            animation-duration: color 3s;
            -webkit-transition: 0.4s;
            -o-transition: 0.4s;
            transition: 0.4s;
        }
        .nav-link:hover{
            background-color: transparent!important;
            color: #005da4;
            font-weight: bold;
        }

        .title{
            padding-top: 80px;
        }
        .title p{
            font-size: 28px;
            color: #333;
        }
        .title #mensaje{
            color: #555;
            padding-top: 15px;
            padding-bottom: 30px;
        }
        .title #fechas{
            font-size: 22px;
        }
        .m-b-md {
            margin-bottom: 30px;
        }
        .mensaje h1 {
            color: #666;
            font-size: 20px;
        }
        .links2 a:hover{
            background-color: #005da4;
            color: #fff;
            border: transparent 2px solid;
        }
    </style>
</head>
<body>

  <div class="content">
      <header>
            <nav class="navbar fonNav">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <i class="fa fa-bars menu-bars" aria-hidden="true"></i>
                        </button>
                        <a class="navbar-brand" href=""><img src="/img/firma-ucr.png"></a>
                    </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                @if (Route::has('login'))
                        <li><a href="{{ url('/login') }}" class="nav-link">Ingresar</a></li>
                @endif
                    </ul>
                </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </header>
    <!-- Mensajes de alerta, en el controlador se llaman utilizando flash('mensaje') -->
    @if (session()->has('flash_notification.message'))
    <br/>
    <div class="mensaje">
        <h1>{!! session('flash_notification.message') !!}</h1>
    </div>
    @endif
    <div class="title m-b-md">
        <p>Sistema de Registro de Horas Estudiante y Horas Asistente</p>
        <p id="mensaje">{{ $periodoSistema->mensaje }}</p>
        @if ($esAccesible === true)
        <p id="fechas">Fecha de apertura: {{ $periodoSistema->fechaInicio }}
        <br/>Fecha de cierre: {{ $periodoSistema->fechaFinal }}</p>
        @endif
    </div>

    <div class="links2">
      @if ($esAccesible === true)
        <a href="formularioEstudiantes" class="btn ">Abrir formulario</a>
      @endif
    </div>
  </div>
</body>
</html>
