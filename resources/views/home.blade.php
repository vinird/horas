@extends('layouts.admin_include')
@extends('layouts.app')
@section('content')
<div class="container-fluid" ng-init="periodoSistema={{$periodoSistema}}; actividades={{$actividades}}; estudiantes={{$estudiantes}}; areas={{$areas}}; carreras={{$carreras}}; bancos={{$bancos}}; designaciones={{$designaciones}};">
  <div class="row" ng-controller="formularioEstudiante">
    <div class="col-xs-12">
      <div class="panel panel-primary center-block" id="panelInfoEstudiantes">
        <div class="panel-heading">Datos de estudiantes participantes
          <div class="pull-right">
            <button class="btn btn-primary btn-xs colapsarBoton" type="button" data-toggle="collapse" data-target="#collapseTablaEstudiantes" aria-expanded="true" colapso="false" aria-controls="collapseTablaEstudiantes"><i class="fa fa-plus " aria-hidden="true"></i></button>
          </div>
          <br>
        </div>
        <div class="panel-body collapse" id="collapseTablaEstudiantes">
          <div class="table-responsive center-block" id="infoEstudiantes">
            <div class="col-lg-6">
              <div class="input-group">
                <input ng-model="filtroEstudiantes" type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                </span>
              </div><!-- /input-group -->
            </div><!-- /.col-lg-6 -->
            <br>
            <br>
            <br>
            <table class="table table-hover table-bordered">
              <thead>
                <tr>
                  <th id="thNom">Nombre</th>
                  <th>Apellidos</th>
                  <th>Carnet</th>
                  <th>Cédula</th>
                  <th>Carrera</th>
                  <th>Copia de cédula</th>
                  <th>Curriculum</th>
                  <th>Exp. Académico</th>
                  <th>Informe</th>
                  <th>Cuenta Bancaria</th>
                  <th>Banco</th>
                  <th>Nivel</th>
                  <th>Promedio</th>
                  <th>Actividad</th>
                  <th id="thPro"></th>
                </tr>
              </thead>
              <tbody>

                <tr ng-repeat="estudiante in estudiantes | filter : filtroEstudiantes">
                  <td>@{{estudiante.nombre}}</td>
                  <td>@{{estudiante.apellidos}}</td>
                  <td>@{{estudiante.carnet}}</td>
                  <td>@{{estudiante.cedula}}</td>

                  <td ng-repeat="carrera in carreras" ng-if="carrera.id == estudiante.fkCarrera">
                    @{{carrera.nombre}}
                  </td>
                  <td><a href="getDocumentos/cedulas@{{estudiante.URLDocumentoCedula}}" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a></td>
                  <td><a href="getDocumentos/cartas@{{estudiante.URLDocumentosPersonales}}" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a></td>
                  <td><a href="getDocumentos/expedientesAcademicos@{{estudiante.URLExpedienteAcademico}}" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a></td>
                  <td><a href="getDocumentos/informesMatricula@{{estudiante.URLInformeMatricula}}"  target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a></td>
                  <td>@{{estudiante.cuentaBancaria}}</td>
                  <td ng-repeat="banco in bancos" ng-if="banco.id == estudiante.fkBanco">
                    @{{banco.nombre}}
                  </td>
                  <td>@{{ estudiante.nivel }}</td>
                  <td>@{{ estudiante.promedio }}</td>
                  <td><a href=""><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                  <td>
                    <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalEstudianteModificar" ng-click="editarEstudiante( estudiante.id, estudiante.carnet, estudiante.cedula, estudiante.nombre, estudiante.apellidos, estudiante.promedio, estudiante.nivel)"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalEliminar" ng-click="eliminarEstudiante(estudiante.id, estudiante.nombre, estudiante.apellidos)"><i class="fa fa-trash" aria-hidden="true"></i></button>

                  </td>
                </tr>
              </tbody>
            </table>
            <nav aria-label="Page navigation" class="">
              <div class="text-center">
                <ul class="pagination">
                  <li>
                    <a href="#" aria-label="Previous">
                      <span aria-hidden="true">&laquo;</span>
                    </a>
                  </li>
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#">5</a></li>
                  <li>
                    <a href="#" aria-label="Next">
                      <span aria-hidden="true">&raquo;</span>
                    </a>
                  </li>
                </ul>
              </div>
            </nav>
          </div>
        </div>
      </div><!-- FIN DE LA TABLA DATOS DE ESTUDIANTES -->
    </div>
    @include('admin.modalEstudiantes')
  </div>
</div>
  <!-- Tabla actividades -->
  <!-- <div class="container-fluid" > -->
    <div class="row" ng-controller="formularioActividad">
      <div class="col-xs-12">
        <div class="panel panel-info center-block" id="panelInfoActividades">
          <div class="panel-heading">Datos de actividades
            <div class="pull-right">
              <button class="btn btn-info btn-xs colapsarBoton" type="button" data-toggle="collapse" data-target="#collapseTablaActividades" aria-expanded="true" colapso="false" aria-controls="collapseTablaActividades"><i class="fa fa-plus" aria-hidden="true"></i></button>
            </div>
            <br>
          </div>
          <div class="panel-body collapse" id="collapseTablaActividades">
            <div class="table-responsive center-block" id="infoActividades">
              <div class="col-lg-6">
                <div class="input-group">
                  <input ng-model="filtroActividades" type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                  </span>
                </div><!-- /input-group -->
              </div><!-- /.col-lg-6 -->
              <br>
              <br>
              <br>
              <table class="table table-hover table-bordered">
                <thead>
                  <tr>
                    <th>Referencia</th>
                    <th>Designación</th>
                    <th>Título</th>
                    <th>Área</th>
                    <th>Horas</th>
                    <th>Descripción</th>
                    <th>Requisitos</th>
                    <th id="thPro"></th>
                  </tr>
                </thead>
                <tbody>

                  <tr ng-repeat="actividad in actividades | filter : filtroActividades">
                    <td>@{{actividad.id}}</td>
                    <td ng-repeat="designacion in designaciones" ng-if="designacion.id == actividad.fkDesignacion">
                      @{{designacion.nombre}}
                    </td>
                    <td>@{{actividad.titulo}}</td>
                    <td ng-repeat="area in areas" ng-if="area.id == actividad.fkArea">
                      @{{area.nombre}}
                    </td>
                    <td>@{{actividad.horas}}</td>
                    <td><a href=""><i class="fa fa-eye" data-toggle="modal" data-target="#modalDetalle" ng-click="cargarDetalleModal('Descripcion:', actividad.descripcion)" aria-hidden="true"></i></a></td>
                    <td><a href=""><i class="fa fa-eye" data-toggle="modal" data-target="#modalDetalle" ng-click="cargarDetalleModal('Requisitos:', actividad.requisitos)" aria-hidden="true"></i></a></td>
                    <td>
                      <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalActividadesModificar" ng-click="editarActividad(actividad.id, actividad.titulo, actividad.horas, actividad.fkArea, actividad.fkDesignacion, actividad.descripcion, actividad.requisitos)"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                      <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalEliminar" ng-click="eliminarActividad(actividad.id, actividad.titulo)"><i class="fa fa-trash" aria-hidden="true"></i></button>
                    </td>
                  </tr>
                </tbody>
              </table>
              <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalAgregarActividad">Agregar
                <i class="fa fa-plus" aria-hidden="true"></i>
              </button>
              <nav aria-label="Page navigation" class="">
                <div class="text-center">
                  <ul class="pagination">
                    <li>
                      <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                      </a>
                    </li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li>
                      <a href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </nav>
            </div>
          </div>
        </div><!-- FIN DE LA TABLA DATOS DE ESTUDIANTES -->
      </div>
    @include('admin.modalActividades')
    </div>
<!-- </div> -->

<div class="row">
  @include('admin.areas.tablaAreas')
  @include('admin.bancos.tablaBancos')
</div>





<!-- Modal eliminar -->
<div id="modalEliminar" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="tituloModal"></h4>
      </div>
      <form id="formularioEliminar" action="" class="form-horizontal" method="POST" enctype="multipart/form-data" autocomplete="off">
        <div class="modal-body">
          <!-- Token para formularios de Laravel -->
          {{ csrf_field() }}
          <!--  -->
          <!-- Método de envío -->
          <input type="hidden" name="_method" value="DELETE">
          <p id="textoModal"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
          <button id="botonModal" type="submit" class="btn btn-danger"></button>
        </form>
      </div>
    </div>
  </div>

<!-- /.modal -->

<!-- Modal detalles -->
<div id="modalDetalle" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Detalle información</h4>
      </div>
      <div class="modal-body">
        <div class="form-group form-group-sm">
          <label id="lblDetalle" class="control-label col-sm-3"></label>
          <p id="txtDetalle" class="col-sm-9">
          </p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- /.modal -->
</div>
@endsection
