<!-- Large modal -->
<div class="modal fade" id="modalSeleccionarActividades" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg" role="document" style="width: 96%; margin: 2%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Proyectos y Actividades</h4>
			</div>
			<div class="modal-body">
				
				<!-- Incluye la tabla con las actividades o proyectos -->
				@include('estudiante.partials.sub.tablaModal')
				<!--  -->

			</div>
			<div class="modal-footer">
				<!-- <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button> -->
				<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Guardar</button>
			</div>
		</div>
	</div>
</div>

<!-- Small modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalConfirmarActividadesForm">Small modal</button> -->

<div class="modal fade" id="modalConfirmarActividadesForm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body text-center">
				<h5 class="modal-title">¿Está seguro(a) que desea concursar para estas actividades?</h5>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">No</button>
				<button type="button" class="btn btn-success btn-sm" data-dismiss="modal" ng-click="cerrarModal()">Si</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalAlertaSeleccionarActividad" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header text-center">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 id="value-title-alerta">¿Desea enviar el formulario con estos datos?</h4>
			</div>
			<div class="modal-body">
				<h4 class="modal-title text-danger text-center"> @{{ textoModalAlerta }}</h4>
				<div id="seccionConfirmacion" class="hidden">
					<p><strong>Nombre:</strong> @{{ nombre +" "+ apellidos }}</p>
					<p><strong>Carné: </strong> @{{ carnet }}</p>
					<p><strong>Cédula: </strong> @{{ cedula }}</p>
					<p><strong>Correo: </strong> @{{ correo }}</p>
					<p><strong>Numero de cuenta:</strong> @{{ cuentaBancaria }}</p>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
				<button type="button" class="btn btn-primary hidden btn-sm" id="btnEnviarFormularioModal">Enviar datos</button>
			</div>
		</div>
	</div>
</div>

