<div class="table-responsive">
	<table class="table table-striped jambo_table bulk_action">
		<thead>
			<th class="text-center">Seleccionar</th>
			<th class="text-center">Designación</th>
			<th class="text-center">Actividades o Proyectos</th>
			<th class="text-center">N/horas</th>
			<th class="text-center">Requisitos Especiales</th>
		</thead>
		<tbody class="tablaActividades">
		
			<tr data-ng-repeat="actividad in actividades | orderBy:'fkDesignacion'"> 
				@{{ actividad.id }}
				<td class="text-center">
					<div class="checkbox">
						<label>
							<input name="actividades[ @{{ actividad.id }} ]" ng-click="selectChange(actividad)" type="checkbox" value="@{{ actividad.id }}" value-title="@{{ actividad.titulo }}" class="actividadesGroup" > </label>
					</div>
				</td>
				
				<td class="text-center" ng-repeat="designacion in designaciones" ng-if="actividad.fkDesignacion == designacion.id">
					@{{ designacion.nombre }}
				</td>

				<td class="text-justify" ng-bind-html="actividad.descripcion | unsafe"></td>
				<td class="text-center">@{{ actividad.horas }}</td>
				<td class="text-justify" ng-bind-html="actividad.requisitos | unsafe"></td>
			</tr>
		</tbody>
	</table>
</div>