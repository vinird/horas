<!DOCTYPE html>
<html lang="es">
<head>
	<link rel="shortcut icon" type="image/x-icon" href="/img/ucr-favicon.png"/>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Horas Estudiante/Asistente</title>

	<!-- angular -->
	<script src="/js/include/jquery.min.js"></script>
	<script src="/js/include/angular.min.js"></script>
	<!-- <script src="/js/include/angular-sanitize.min.js"></script> -->
	<script src="/js/include/dimmer.min.js"></script>
	<script src="/js/controllers/estudianteFormApp.js"></script>
	<script src="/js/controllers/estudiante.js"></script>
	<!--  -->


	
	<link rel="stylesheet" type="text/css" href="/css/app.css">
	<!-- Template custom styles -->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/include/custom.min.css') }}">
	<!--  -->
	<link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css"> 
	<link rel="stylesheet" type="text/css" href="/css/include/dimmer.min.css"> 
	<link rel="stylesheet" type="text/css" href="/css/include/loader.min.css"> 

	<link rel="stylesheet" type="text/css" href="/css/include/estudiante.css"> 


</head>
<body ng-app="Horas">
	<!-- dimmer -->
	<div class="ui dimmer">
		<div class="ui loader"></div>
	</div>
	<!--  -->
	<header>
		 <nav class="navbar fonNav navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <i class="fa fa-bars menu-bars" aria-hidden="true"></i>
                        </button>                       
                        <a class="navbar-brand" href="/"><img src="/img/firma-ucr.png"></a>
                    </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">      
                    <ul class="nav navbar-nav navbar-right">
                    	<li><a class="home-icon nav-link" href="/"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                		<li id="">
							<a href="formularioEstudiantes" class="actualizarForm nav-link"><i class="fa fa-undo" aria-hidden="true"></i></a>
						</li>                		
                    </ul>
                </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br class="hidden-sm hidden-md hidden-lg">
	</header>
	<main>
		<div class="container">	
			<div class="">
				<div class="col-xs-12 col-sm-8 col-sm-offset-2">
					<div class="x_panel  center-block" id="panelEstudiante">
						<div class="x_title text-center">Formulario de solicitud de horas estudiante, asistente y asistente de posgrado.</div>
						<div class="x_content">