 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">

 	<title>Horas estudiante y horas asistente</title>

 	<!-- Fonts -->
 	<!-- Styles -->
 	<style>
 		@font-face {
          font-family: myriad;
          src: url(/fonts/Myriad-Pro.ttf);
        }
 		body {
 			font-family: "myriad";
 			font-size: 14px;
 			line-height: 20px;
 			font-weight: 400;
 			color: #373737;
 			background: #f2f2f2;
 			margin:0px;
 			-webkit-font-smoothing: antialiased;
 			font-smoothing: antialiased;
 		}
		header{
			width: 100%;
			height: 60px;			
			padding-bottom: 10px;
		}
		.barraHeader{
			width: 100%;
			height: 20px;
			background: #204c6f;			
		}
		header.colorHeader{
			background: #41ADE7;
		}
 		.wrapper {
 			padding: 40px;
 			background: #fff;
 			width: 85%!important;
 			margin: auto;
 			margin-top: 0;
 			box-shadow: 0px 1px 4px 1px rgba(0, 0, 0, 0.2);
 		}
 		
 		.table {
 			margin: 0 0 40px 0;
 			width: 100%;
 			box-shadow: 0px 1px 4px 1px rgba(0, 0, 0, 0.2);
 			display: table;
 			background: #f5f5f5;
 		}
 		@media screen and (max-width: 580px) {
 			.table {
 				display: block;
 			}
 		}

 		.row {
 			display: table-row;
 			background: #f5f5f5;
 		}
 		.row:nth-of-type(odd) {
 			background: #f5f5f5;
 		}
 		.row.header {
 			font-weight: 900;
 			color: #ffffff;
 			background: #ea6153;
 			font-style: oblique;
 		}
 		.row.green {
 			background: #27ae60;
 		}
 		.row.blue {
 			background: #2980B9;
 		}
 		@media screen and (max-width: 580px) {
 			.row {
 				padding: 8px 0;
 				display: block;
 			}
 		}

 		.cell {
 			padding: 6px 12px;
 			display: table-cell;
 		}
 		@media screen and (max-width: 580px) {
 			.cell {
 				padding: 2px 12px;
 				display: block;
 			}
 		}

 		.alert{
 			background: #C7FFC8;
 			padding: 3px;
 			padding-left: 30px;
 			border-left: 4px solid #008506;
 			margin-top: 20px;
 		}
 		#numCuenta{
 			text-align: center;
 		}
 		.datosPersonales{
 			padding: 5px 30px;
 		}
		.firma {
			text-align: center;
		}
 	</style>
 </head>
 <body>
 	<div class="wrapper">
 		<header class="colorHeader">
 			<img src="/img/firma-ucr.svg">
 		</header>
 		<div class="barraHeader"></div>
 		<div class="alert">
 			<h2>Confirmación de envío de formulario</h2>
 		</div>
 		<div class="datosPersonales">
 			<p>
 				<strong>
 					La información será revisada y de ser seleccionado(a) un funcionario(a) de la Universidad de Costa Rica se pondrá en contacto con usted.
 				</strong>
 			</p>
 			<p>
 				Los datos ingresados en el sistema son los siguientes:
 			</p>
 			<ul>
 				@if(isset($data))
 				<li>Nombre: {{ $data->nombre }}</li>
 				<li>Apellidos: {{ $data->apellidos }}</li>
 				<li>Cédula: {{ $data->cedula }}</li>
 				<li>Carné: {{ $data->carnet }}</li>
 				<li>Cuenta bancaria: {{ $data->cuentaBancaria }}</li>
 				@endif
 			</ul>
 		</div>
 		<hr>
 		<br>
 		<h4>Actividades y/o proyectos seleccionados</h4>

 		<div class="table">

 			<div class="row header blue">
 				<div class="cell">
 					Ref
 				</div>
 				<div class="cell">
 					Descripción
 				</div>
 				<div class="cell">
 					Requisitos
 				</div>
 				<div class="cell">
 					Horas
 				</div>
 			</div>
 			@if(isset($actividades))
 			@if(count($actividades))
 			@foreach($actividades as $actividad)
 			<div class="row">
 				<div class="cell">
 					{!! $actividad->id !!}
 				</div>
 				<div class="cell">
 					{!! $actividad->descripcion !!}
 				</div>
 				<div class="cell">
 					{!! $actividad->requisitos !!}
 				</div>
 				<div class="cell">
 					{!! $actividad->horas !!}
 				</div>
 			</div>
 			@endforeach
 			@endif
 			@endif
 		</div>
 		<br>
 		<br>
 		<div class="firma">
 			<img src="/img/firma-correo.png">
 		</div>
 	</div>
 </body>
 </html>
