<!-- Incluye el head y header -->
@include('estudiante.partials.head')
<!--  -->

<div id="wizard" class="form_wizard wizard_horizontal">
	<ul class="wizard_steps anchor">
		<li>
			<a href="#step-1" class="done" isdone="1" rel="1">
				<label class="step_no">1</label>
				<span class="step_descr">
					Paso 1<br />
					<small>Datos personales</small>
				</span>
			</a>

		</li>
		<li><a href="#step-2" class="done" isdone="1" rel="2">
			<label class="step_no">2</label>
			<span class="step_descr">
				Paso 2<br />
				<small>Documentos</small>
			</span>
		</a></li>
		<li><a href="#step-3" class="done" isdone="1" rel="3">
			<label class="step_no">3</label>
			<span class="step_descr">
				Paso 3<br />
				<small>Banco y carnet</small>
			</span>                   
		</a></li>
		<li><a href="#step-4" class="done" isdone="1" rel="4">
			<label class="step_no">4</label>
			<span class="step_descr">
				Paso 4<br />
				<small>Actividades</small>
			</span>                   
		</a></li>
	</ul>
	<div ng-controller="formularioEstudiante" ng-init="actividades={{ $actividades }}; designaciones={{ $designaciones }}">
		<form action="/newEstudiante" class="form-horizontal" method="POST" enctype="multipart/form-data" autocomplete="off" id="formularioEstudiante" name="estudiante">
			<!-- token para formularios de Laravel -->
			{{ csrf_field() }}
			<div id="step-1">   
				<br>
				<div class="form-group form-group-sm @if($errors->has('nombre')) has-error @endif" ng-class="{ 'has-error' : estudiante.nombre.$invalid && !estudiante.nombre.$pristine }">
					<label for="nombre" class="control-label col-sm-3">Nombre:</label>
					<div class="col-sm-7 col-xs-12">
						<input type="text" class="form-control" name="nombre" placeholder="Digite su nombre" value="{{ old('nombre') }}" ng-model="nombre" required>
						@if($errors->has('nombre'))
						<span class="help-block">{{ $errors->first('nombre') }}</span>
						@endif
					</div>
				</div>
				<div class="form-group form-group-sm @if($errors->has('apellidos')) has-error @endif" ng-class="{ 'has-error' : estudiante.apellidos.$invalid && !estudiante.apellidos.$pristine }">
					<label for="apellidos" class="control-label col-sm-3">Apellidos:</label>
					<div class="col-sm-7 col-xs-12">
						<input type="text" class="form-control" name="apellidos" placeholder="Digite su apellido" value="{{ old('apellidos') }}" ng-model="apellidos" required>
						@if($errors->has('apellidos'))
						<span class="help-block">{{ $errors->first('apellidos') }}</span>
						@endif
					</div>
				</div>
				<div class="form-group form-group-sm @if($errors->has('correo')) has-error @endif" ng-class="{ 'has-error' : estudiante.correo.$invalid && !estudiante.correo.$pristine }">
					<label for="correo" class="control-label col-sm-3">Correo electrónico:</label>
					<div class="col-sm-7 col-xs-12">
						<input type="email" class="form-control" name="correo" placeholder="Digite su correo electrónico" value="{{ old('correo') }}" ng-model="correo" required>
						@if($errors->has('correo'))
						<span class="help-block">{{ $errors->first('correo') }}</span>
						@endif
					</div>
				</div>
				<div class="form-group form-group-sm @if($errors->has('cedula')) has-error @endif" ng-class="{ 'has-error' : estudiante.cedula.$invalid && !estudiante.cedula.$pristine || estudiante.cedula.$error.pattern && !estudiante.cedula.$pristine}">
					<label for="cedula" class="control-label col-sm-3">Cédula:</label>
					<div class="col-sm-7 col-xs-12">
						<input type="text" class="form-control" name="cedula" placeholder="Digite su numero de cédula" value="{{ old('cedula') }}" ng-model="cedula" required>
						@if($errors->has('cedula'))
						<span class="help-block">{{ $errors->first('cedula') }}</span>
						@endif
					</div>
				</div>
				<div class="form-group form-group-sm @if($errors->has('carrera')) has-error @endif" ng-class="{ 'has-error' : estudiante.carrera.$invalid && !estudiante.carrera.$pristine}">
					<label for="carrera" class="control-label col-sm-3">Carrera:</label>
					<div class="col-sm-7 col-xs-12">
						<select class="form-control" name="carrera" ng-model="carrera" required>
							<option value="">-- Seleccione una opción --</option>
							@if(isset($carreras))
							@if(count($carreras) > 0)
							@foreach($carreras as $carrera)
							<option value="{{ $carrera->id }}">{{ $carrera->nombre }}</option>
							@endforeach
							@endif
							@endif
						</select>
						@if($errors->has('carrera'))
						<span class="help-block">{{ $errors->first('carrera') }}</span>
						@endif
					</div>
				</div>
			</div>
			<div id="step-2">
				<br>
				<div class="form-group form-group-sm @if($errors->has('docCedula')) has-error @endif">
					<label for="docCedula" class="control-label col-sm-3">Copia de la cédula: 
					</label>
					<a tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-content="Debe ingresar un documento con la fotocopia de la cédula por ambos lados en una misma página.">
						<i class="fa fa-question-circle" aria-hidden="true"></i>
					</a>
					<div class="col-md-1 col-xs-12">
						<input type="file" name="docCedula" accept=".docx, .pdf, .doc, .odt" ng-model="docCedula"> 
						@if($errors->has('docCedula'))
						<span class="help-block">{{ $errors->first('docCedula') }}</span>
						@endif
					</div>
				</div>
				<hr>
				<div class="form-group form-group-sm @if($errors->has('docCarta')) has-error @endif">
					<label for="docCarta" class="control-label col-sm-3">Curriculum y atestados: 
					</label>
					<a tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-content="Debe ingresar un documento con el curriculum y atestados (NO INCLUIR TÍTULO DE COLEGIO O ESCUELA).">
						<i class="fa fa-question-circle" aria-hidden="true"></i>
					</a>
					<div class="col-md-1 col-xs-12">
						<input type="file" name="docCarta" accept=".docx, .pdf, .doc, .odt" ng-model="docCarta"> 
						@if($errors->has('docCarta'))
						<span class="help-block">{{ $errors->first('docCarta') }}</span>
						@endif
					</div>
				</div>
				<hr>
				<div class="form-group form-group-sm @if($errors->has('docExpedAcademico')) has-error @endif">
					<label for="docExpedAcademico" class="control-label col-sm-3">Expediente académico:
					</label>
					<a tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-content="Debe ingresar un documento con el expediente académico (impreso desde e-matrícula).">
						<i class="fa fa-question-circle" aria-hidden="true"></i>
					</a>
					<div class="col-md-1 col-xs-12">
						<input type="file" name="docExpedAcademico" accept=".docx, .pdf, .doc, .odt" ng-model="docExpedAcademico">
						@if($errors->has('docExpedAcademico'))
						<span class="help-block">{{ $errors->first('docExpedAcademico') }}</span>
						@endif
					</div>
				</div>
				<hr>
				<div class="form-group form-group-sm @if($errors->has('docInformeMatricula')) has-error @endif">
					<label for="docInformeMatricula" class="control-label col-sm-3">Informe de matrícula:</label>
					<a tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-content="Debe ingresar un documento informe de matrícula (impreso desde e-matrícula).">
						<i class="fa fa-question-circle" aria-hidden="true"></i>
					</a>
					<div class="col-md-1 col-xs-12">
						<input type="file" name="docInformeMatricula" accept=".docx, .pdf, .doc, .odt" ng-model="docInformeMatricula">
						@if($errors->has('docInformeMatricula'))
						<span class="help-block">{{ $errors->first('docInformeMatricula') }}</span>
						@endif
					</div>
				</div>
			</div>                      
			<div id="step-3">
				<br>
				<div class="form-group form-group-sm @if($errors->has('carnet')) has-error @endif" id="div_carnet"  ng-class="{ 'has-error' : estudiante.carnet.$invalid && !estudiante.carnet.$pristine}">
					<label for="carnet" class="control-label col-sm-3">Carné:</label>
					<div class="col-sm-7 col-xs-12">
						<input type="text" class="form-control" name="carnet" placeholder="Digite su carné estudiantil" value="{{ old('carnet') }}" id="carnet" ng-model="carnet" ng-change="getInfo_estudiante()" required>
						<span id="helpBlock_carnet" class="help-block hidden">Este carné ya se encuentra registrado.</span>
						@if($errors->has('carnet'))
						<span class="help-block">{{ $errors->first('carnet') }}</span>
						@endif
					</div>
				</div>
				<hr>
				<div class="form-group form-group-sm @if($errors->has('banco')) has-error @endif @if($errors->has('cuentaBancaria')) has-error @endif"  ng-class="{ 'has-error' : estudiante.cuentaBancaria.$invalid && !estudiante.cuentaBancaria.$pristine}">
					<label for="cuentaBancaria" class="control-label col-sm-3">Número de cuenta:</label>
					<div class="col-sm-7 col-xs-12">
						<input type="text" class="form-control" name="cuentaBancaria" placeholder="Digite su cuenta bancaria" value="{{ old('cuentaBancaria') }}" ng-model="cuentaBancaria" ng-pattern="number" required>
						@if($errors->has('cuentaBancaria'))
						<span class="help-block">{{ $errors->first('cuentaBancaria') }}</span>
						@endif
						<p ng-show="estudiante.cuentaBancaria.$error.pattern && !estudiante.cuentaBancaria.$pristine" class="help-block">Solo puede ingresar números.</p>
						@if(isset($bancos))
						@if(count($bancos) > 0)
						@foreach($bancos as $banco)
						<label class="radio-inline">
							<input type="radio" name="banco" value="{{ $banco->id }}" required ng-model="banco"> {{ $banco->nombre }}
						</label>
						@endforeach
						@endif
						@endif
						@if($errors->has('banco'))
						<span class="help-block">{{ $errors->first('banco') }}</span>
						@endif
					</div>
				</div>
				<br><br>
			</div>
			<div id="step-4">
				<br>
				<div class="form-group form-group-sm @if($errors->has('actividades')) has-error @endif">
					<label for="horas" class="control-label col-sm-4">Actividades y/o proyectos:</label>
					<div class="col-sm-8">
						<a class="btn btn-primary btn-sm pull-left" ng-click="abrirModal()"><i class="fa fa-external-link" aria-hidden="true"> </i> Mostrar actividades </a>
						@if($errors->has('actividades'))
						<span class="help-block">{{ $errors->first('actividades') }}</span>
						@endif
						<span class="help-block hidden" id="help-blockActividades" style="color: green;">Actividades y/o proyectos seleccionados</span>
						@include('estudiante.partials.modalActividades')
						<div ng-repeat="data in actividadesSeleccionadas" class="text-success">
							@{{ data }}
						</div>
					</div>
				</div> 
				<hr>
				<div class="form-group form-group-sm @if($errors->has('terminos_y_condiciones')) has-error @endif"">
					<label class="control-label col-sm-4">
						<a href="{{ asset('documents/horas_estudiante_asistente_posgrado.pdf') }}" target="_blanck"> Acepta los terminos y condiciones.</a>
						@if($errors->has('terminos_y_condiciones'))
						<span class="help-block">{{ $errors->first('terminos_y_condiciones') }}</span>
						@endif
					</label>
					<div class="col-sm-8 ">
						<div class="pull-left">
							<input type="checkbox" name="terminos_y_condiciones" ng-click="aceptaTerminos()" id="checkboxAcepto">&nbsp;&nbsp;
							<button ng-click="revisarFormularioEstudiantes()" onclick="event.preventDefault();" class="btn btn-success btn-sm" id="boton_enviar" disabled>¡Concursar!</button>
						</div>
					</div>
				</div>   
				<br><br>                    
			</div>
		</form>	
	</div>
</div>


<!-- Incluye el footer -->
@include('estudiante.partials.footer')