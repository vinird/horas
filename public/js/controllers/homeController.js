var app = angular.module("Horas", ['angularUtils.directives.dirPagination']);

app.controller("homeCtrl", function($scope){
});

$(document).ready(function() {
	$('input[name="fechaInicio"]').datetimepicker({
		format: 'YYYY-MM-DD HH:mm',
		locale: 'es'
	});
	$('input[name="fechaFinal"]').datetimepicker({
			useCurrent: false,
			format: 'YYYY-MM-DD HH:mm',
			locale: 'es'
	});

	$('input[name="fechaInicio"]').on("dp.change", function (e) {
			$('input[name="fechaFinal"]').data("DateTimePicker").minDate(e.date);
	});
	$('input[name="fechaFinal"]').on("dp.change", function (e) {
			$('input[name="fechaInicio"]').data("DateTimePicker").maxDate(e.date);
	});

	$('a[data-target="#modalPeriodoSistema"]').on("click", function() {
		$('input[name="fechaInicio"]').data("DateTimePicker").defaultDate($(this).data('periodosistema')[0].fechaInicio);
		$('input[name="fechaFinal"]').data("DateTimePicker").defaultDate($(this).data('periodosistema')[0].fechaFinal);
	});

	function cargarInfoModal (action, titulo, mensaje, boton){
		$("#formularioEliminar").attr("action", action);
		$("#tituloModal").text(titulo);
		$("#textoModal").text(mensaje);
		$("#botonModal").text(boton);
	}

	function cargarDetalleModal (detalle, texto){
	  $("#lblDetalle").text(detalle);
	  $("#txtDetalle").text(texto);
	}
});
