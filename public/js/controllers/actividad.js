app.controller('formularioActividad', ['$scope', '$http', function(s, h){
	s.actividadesXPagina = 10;

	s.editarActividad = function(id, titulo, horas, area, designacion, descripcion, requisitos) {
    s.idActividad = id;
    s.tituloActividad = titulo;
    s.horasActividad = horas;
    s.areaActividad = area;
    s.designacionActividad = designacion;
    s.descripcionActividad = descripcion;
    s.requisitosActividad = requisitos;

    $("#contentDescripcion").trumbowyg('html', descripcion);
    $("#contentRequisitos").trumbowyg('html', requisitos);
  };

  s.myOrderBy = function(order) {
        s.orderActividad = order;
    }

  function cargarInfoModal (action, titulo, mensaje, boton){
        $("#formularioEliminar").attr("action", action);
        $("#tituloModal").text(titulo);
        $("#textoModal").text(mensaje);
        $("#botonModal").text(boton);
    }

	s.eliminarActividad = function(id, titulo) {
		cargarInfoModal("actividades/" + id, "Eliminar actividad", "¿Está seguro de eliminar " + titulo + " de manera permanente?", "Eliminar actividad");
	};

	s.cargarDetalleModal = function(titulo, detalle, texto) {
        $('#modalDetalleTitle').text(titulo);
        $('#modalDetalleLabel').text(detalle);
        $('#modalDetalleTexto').html(texto);
	};

    s.cargarEstudiantesActividades = function(id)
    {
        s.actividadIdRelEstudiante = id;
        relExist = [];
        s.relacionActividadEstudiante.forEach( function(element, index) {
            if(element.fkActividad == id)
            {
                relExist.push(element);
            }
        });
        s.relEstudianteActivida = [];
        s.estudiantes.forEach( function(estudiante, index) {
            relExist.forEach( function(rel, index) {
                if(rel.fkEstudiante == estudiante.id)
                {
                    if(rel.elegido == 1)
                    {
                        estudiante.elegido = true;
                    } else
                    {
                        estudiante.elegido = false;
                    }
                    s.relEstudianteActivida.push(estudiante);
                }
            });
        });
        if(s.relEstudianteActivida.length == 0)
        {
            $('#tituloModalEstudiantesActividades').text('No hay estudiantes participando en esta actividad')
            $('#modalEstudiantesActividades').modal('toggle');
        } else
        {
            $('#tituloModalEstudiantesActividades').text('')
            $('#modalEstudiantesActividades').modal('toggle');
        }
    }
}]);

$(document).ready(function(){
    $('.textareaTrumbowyg').trumbowyg();
});
