app.filter('unsafe', function($sce) { return $sce.trustAsHtml; });
app.controller('formularioEstudiante', ['$scope', '$http', function(s, h){

	s.estudiantesXPagina = 10;
	s.textoModalAlerta = "";
	s.actividadesSeleccionadas = [];

	s.getInfo_estudiante = function()
	{
		s.consultar_api();
	};


	// Ordena la tabla
	s.myOrderBy = function(order) {
        s.orderEstudiante = order;
    }

    // Carga el modal para ver actividades
    s.cargarDetalleModal = function(titulo, detalle, id){
    	texto = s.obtenerActividades(id);
    	$('#modalDetalleTitle').text(titulo);
        $('#modalDetalleLabel').text(detalle);
        $('#modalDetalleTexto').html(texto);
    };

    s.obtenerActividades = function(id){
    	texto = "<br>";
    	s.relacionActividadEstudiante.forEach(function(data){
    		if (data.fkEstudiante == id) {
    			s.actividades.forEach(function(dataActividades){
    				if (dataActividades.id == data.fkActividad) {
	    				texto = texto + '<strong>Referencia: </strong>' + dataActividades.ref +'<br><strong> Actividad y/o proyecto: </strong>' +  dataActividades.titulo +"<br><br>";
    				}
    			})
    		}
    	})
    	return texto;
    };

	// Consulta el carnet en la base de datos
	s.consultar_api = function(){
		if( s.carnet !== null ) // Verifica que el input carnet no este vacio
		{
			// Consulta al API
			h.get( 'api/carnet/' + s.carnet )
			.then(function(response){
				// Evalua los datos de la consulta
				if(response.data == 1)
				{
					s.carnet_invalido(); // Muestra el error
				} else
				{
					s.carnet_valido(); // Borra el error
				}
			});
		}
	};

	// Sucede cuando el carnet existe
	s.carnet_invalido = function(){
		$('#div_carnet').addClass('has-error');
		$('#helpBlock_carnet').removeClass('hidden');
	};

	// Sucede cuando el carnet es valido
	s.carnet_valido = function(){
		$('#div_carnet').removeClass('has-error');
		$('#helpBlock_carnet').addClass('hidden');
	};

	s.editarEstudiante = function(id, carnet, cedula, nombre, apellidos, promedio, nivel) {
		s.idEstudiante = id;
		s.carnetEstudiante = carnet;
		s.cedulaEstudiante = cedula;
		s.nombreEstudiante = nombre;
		s.apellidosEstudiante = apellidos;
		s.promedioEstudiante = promedio;
		s.nivelEstudiante = nivel;
	};

	function cargarInfoModal (action, titulo, mensaje, boton){
		$("#formularioEliminar").attr("action", action);
		$("#tituloModal").text(titulo);
		$("#textoModal").text(mensaje);
		$("#botonModal").text(boton);
	}

	s.eliminarEstudiante = function(id, nombre, apellidos) {
		cargarInfoModal("estudiantes/" + id, "Eliminar estudiante", "¿Está seguro de eliminar a " + nombre + " " + apellidos + " de manera permanente?", "Eliminar estudiante");
	};

	//////////////////////////////////////////////////////////
	// Abre el modal para selecionar actividades
	s.abrirModal = function(){
		$('#modalSeleccionarActividades').modal('toggle');
	}

	// Evalua si se han seleccionado actividades y muestra un modal
	// TODO: leer losgica, cantidad de actividades
	s.abrirModalPequenoConfirmacion = function() {
		if($('.actividadesGroup:checked').length > 0){
			$('#modalConfirmarActividadesForm').modal('toggle');
		} else {
			s.textoModalAlerta = "Seleccione una actividad";
			$('#modalAlertaSeleccionarActividad').modal('toggle');
		}
	}


	s.selectChange = function(obj){
		// s.actividadesSeleccionadas.forEach(function(data){
		// 	if(data.id == obj.id){
		// 		s.actividadesSeleccionadas.splice(obj,1)
		// 		throw new Error("Stopping the function!");
		// 	}
		// }, this);


		// s.actividadesSeleccionadas.push(obj);
		// console.log(s.actividadesSeleccionadas);
		// console.log(s.actividadesSeleccionadas);

		// var _selected = $( "input:checked" ).attr('value-title');
		s.actividadesSeleccionadas = [];
		$('input:checked.actividadesGroup').each(function() {
		    s.actividadesSeleccionadas.push($(this).attr('value-title'));
		});
	}

	// Sucede cuando se confirman las actividades selecionadas
	s.cerrarModal = function(){
		$('#modalSeleccionarActividades').modal('toggle');
		$('#selectActividades').attr('disabled', 'true');
		$('#help-blockActividades').removeClass('hidden');
	}

	// Regex
	s.number = "[0-9]*";

	// Habilita y deshabilita el boton de enviar formulario
	s.countAcepto = 0;
	s.aceptaTerminos = function(){
		if(s.countAcepto % 2 == 0){
			$('#boton_enviar').removeAttr('disabled');
		} else {
			$('#boton_enviar').prop('disabled', true);
		}
		s.countAcepto ++;
	}

	// Revisa todo el formulario
	s.revisarFormularioEstudiantes = function(){
		$('#seccionConfirmacion').addClass('hidden');
		s.textoModalAlerta = "";
		$('#value-title-alerta').show();
		if( $('.actividadesGroup:checked').length > 0 ) //Verifica si hay actividades seleccionadas
		{
			$('#btnEnviarFormularioModal').addClass('hidden'); // Oculta el boton para submit
			if(s.estudiante.$invalid) // Verifica si el formulario es INVALIDO
			{
				$('#value-title-alerta').hide();
				s.textoModalAlerta = "Debe completar todos los campos."; // Muestra mensaje
				$('#modalAlertaSeleccionarActividad').modal('toggle');
			}
			else // Sucede si el formulario es valido
			{
				if( $("input[name='docCedula']").val() != "" && $("input[name='docExpedAcademico']").val() != "" && $("input[name='docCarta']").val() != "" && $("input[name='docInformeMatricula']").val() != "" ) // Evalua que se hayan ingresado todos los archivos
				{
					// Muestra los datos del estudiante en un modal
					$('#seccionConfirmacion').removeClass('hidden');
					$('#btnEnviarFormularioModal').removeClass('hidden');
					$('#modalAlertaSeleccionarActividad').modal('toggle');
					// $('#formularioEstudiante').submit();
				} else
				{	// Le indica al estudiante que debe ingresar todos los documentos
					$('#value-title-alerta').hide();
					s.textoModalAlerta = "Ingrese todos los documentos.";
					$('#modalAlertaSeleccionarActividad').modal('toggle');
				}
			}
		} else
		{	// Le indica al estudiante que debe seleccionar actividaes
			$('#value-title-alerta').hide();
			s.textoModalAlerta = "Debe seleccionar mínimo una actividad y/o proyecto.";
			$('#modalAlertaSeleccionarActividad').modal('toggle');
		}

	}
	/////////

}]);

jQuery(document).ready(function($) {

	$('#btnEnviarFormularioModal').click(function(event) {
		$('#modalAlertaSeleccionarActividad').modal('toggle');
		console.log('enviando');
		$( "#formularioEstudiante" ).submit();
		$('body').dimmer('show');
	});



});
