app.controller('areasCtrl', ['$scope', function (s) {

	s.cargarModalModificar = function(id, nombre, color){
		s.modificarAreaID = id;
		s.modificarNombreArea = nombre;
		s.modificarColorArea = color;
	};

	s.myOrderBy = function(order) {
		s.orderArea = order;
	}

	function cargarInfoModal (action, titulo, mensaje, boton){
		$("#formularioEliminar").attr("action", action); 
		$("#tituloModal").text(titulo);
		$("#textoModal").text(mensaje);
		$("#botonModal").text(boton);
	}

	s.eliminarArea = function(id, nombre) {
		cargarInfoModal("areas/" + id, "Eliminar Área", "¿Está seguro de eliminar la área: " + nombre + " de manera permanente?", "Eliminar área");
	};
}])
