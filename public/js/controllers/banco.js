app.controller('bancoCtrl', ['$scope', function (s) {
	s.cargarModalModificar = function(id, nombre){
		s.modificarBancoID = id;
		s.modificarNombreBanco = nombre;
	};

	s.myOrderBy = function(order) {
        s.orderBanco = order;
    }

  function cargarInfoModal (action, titulo, mensaje, boton){
		$("#formularioEliminar").attr("action", action);
		$("#tituloModal").text(titulo);
		$("#textoModal").text(mensaje);
		$("#botonModal").text(boton);
	}

  s.eliminarBanco = function(id, nombre) {
		cargarInfoModal("bancos/" + id, "Eliminar Banco", "¿Está seguro de eliminar el banco: " + nombre + " de manera permanente?", "Eliminar banco");
	};

}]);
