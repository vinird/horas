app.controller('carreraCtrl', ['$scope', function (s) {
	s.cargarModalModificarCarrera = function(id, nombre, sigla){
		s.modificarCarreraID     = id;
		s.modificarNombreCarrera = nombre;
		s.modificarSiglaCarrera  = sigla;
	};

	s.myOrderBy = function(order) {
        s.orderCarrera = order;
    }

  function cargarInfoModal (action, titulo, mensaje, boton){
		$("#formularioEliminar").attr("action", action);
		$("#tituloModal").text(titulo);
		$("#textoModal").text(mensaje);
		$("#botonModal").text(boton);
	}

  s.eliminarCarrera = function(id, nombre) {
		cargarInfoModal("carreras/" + id, "Eliminar Carrera", "¿Está seguro de eliminar la carrera: " + nombre + " de manera permanente?", "Eliminar carrera");
	};

}]);
