app.controller('usuarioCtrl', ['$scope', function (s) {

	function cargarInfoModal (action, titulo, mensaje, boton){
		$("#formularioEliminar").attr("action", action);
		$("#tituloModal").text(titulo);
		$("#textoModal").text(mensaje);
		$("#botonModal").text(boton);
	}

	s.eliminarUsuario = function(id, email) {
		$('#inputDeletUserId').val(id);
	};

	s.modificarUsuario = function(id, nombre, apellido, correo){
		s.modificarId = id;
		s.modficarNombre = nombre;
		s.modificarApellido = apellido;
		s.modificarCorreo = correo;
	}

	s.tipoChange = function()
	{
		tipoUsuarioSelccionado = $('.addUserTypeChoice:selected').val();
		if(tipoUsuarioSelccionado == '4')
		{
			$('#divAreasUsuarios').removeClass('hide');
		} else 
		{
			$(".selectAreasUser").prop("checked", false);
			$('#divAreasUsuarios').addClass('hide');
		}
	} 

	s.setPopOverData = function(id)
	{
		s.PopOverData = "";
		s.relAreaUser.forEach( function(rel, index) {
			if(rel.user == id)
			{
				s.areas.forEach( function(area, index) {
					if(rel.area == area.id)
					{
						s.PopOverData += area.nombre + " | " ;
					}
				});

			}
			text = s.PopOverData.slice(0, -2);
			$('.popOverAreasUser').text(text);
		});
	}


}]);

jQuery(document).ready(function($) {
	$(function () {
		$('[data-toggle="popover"]').popover()
	})
});

function btnSubmitEliminarUser(){
	$('#formularioUsuariosVista').submit();

}
