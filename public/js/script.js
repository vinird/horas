$(document).ready(function(){	

	$('.colapsarBoton').click(function(){
		var valorAttrColapso = $(this).attr('colapso');
		
		if(valorAttrColapso == 'false'){
			$('i', this).removeClass('fa-plus');
			$('i', this).addClass('fa-minus');
			$(this).attr('colapso', 'true');			
		} else {			
			$('i', this).removeClass('fa-minus');
			$('i', this).addClass('fa-plus');
			$(this).attr('colapso', 'false');			
		}		
	})
});