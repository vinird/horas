# HORAS

## Url del proyecto: http://horas.nubila.tech
* Usuario: admin@admin.com
* Contrase�a: password

## Requerimientos de Laravel
* Composer
* MySQL
* PHP >= 5.6.4
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension

## Requerimientos de Laravel Excel
* PHPOffice PHPExcel >= 1.8.0 (included by composer.json)
* PHP extension php_zip enabled (required if you need PHPExcel to handle .xlsx .ods or .gnumeric files)
* PHP extension php_xml enabled
* PHP extension php_gd2 enabled (optional, but required for exact column width autocalculation)

## Instalaci�n 
* Clonar el c�digo fuente 
```
#!

git clone https://vinird@bitbucket.org/vinird/horas.git
```
* Copiar el archivo .env.example a .env
* Cambiar las variables de entorno, Host, Database, User, Password.
* Crear base de datos para el proyecto.

* Instalar dependencias.
```
#!
composer install
```

* Generar llave.
```
#!
php artisan key:generate
```

* Ejecutar migraciones
```
#!

php artisan migrate
```

* Sembrar la base de datos
```
#!

php artisan db:seed
```

* Activar servidor
```
#!

php artisan serve
```