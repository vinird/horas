<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint; 
use Illuminate\Database\Migrations\Migration;

class CreateActividadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actividads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref')->nullable();
            $table->string('sigla', 20)->nullable();
            $table->string('titulo', 100);
            $table->integer('horas');
            $table->longText('descripcion');
            $table->longText('requisitos');
            // Llave foranea
            $table->integer('fkDesignacion')->unsigned();
            $table->foreign('fkDesignacion')->references('id')->on('designacions');
            $table->integer('fkArea')->unsigned();
            $table->foreign('fkArea')->references('id')->on('areas');
            $table->integer('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users');
            //
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('actividads');
    }
}
