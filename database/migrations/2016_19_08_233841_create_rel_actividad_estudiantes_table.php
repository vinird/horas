<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelActividadEstudiantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rel_actividad_estudiantes', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('elegido')->nullable();
            // Llaves foranas
            $table->integer('fkActividad')->unsigned();
            $table->integer('fkEstudiante')->unsigned();
            $table->foreign('fkActividad')->references('id')->on('actividads');
            $table->foreign('fkEstudiante')->references('id')->on('estudiantes');
            // 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rel_actividad_estudiantes');
    }
}
