<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstudiantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudiantes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 100);
            $table->string('apellidos', 100);
            $table->string('correo', 200);
            $table->string('cedula', 20);
            $table->string('cuentaBancaria', 100);
            $table->string('carnet', 10)->unique();
            $table->string('URLDocumentoCedula', 200);
            $table->string('URLDocumentosPersonales', 200);
            $table->string('URLExpedienteAcademico', 200);
            $table->string('URLInformeMatricula', 200);
            $table->string('nivel', 50)->nullable();
            $table->float('promedio')->nullable();
            // Llaves foraneas
            $table->integer('fkBanco')->unsigned();
            $table->integer('fkCarrera')->unsigned();
            $table->foreign('fkBanco')->references('id')->on('bancos');
            $table->foreign('fkCarrera')->references('id')->on('carreras');
            //
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('estudiantes');
    }
}
