<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // $this->call(UsersTableSeeder::class);
            // Areas
        DB::table('areas')->insert([
            'nombre'      => "Dirección",
            'color'       => "#F13434",
            ]);
        DB::table('areas')->insert([
            'nombre'      => "Docencia",
            'color'       => "#6536FF",
            ]);
        DB::table('areas')->insert([
            'nombre'      => "Investigación",
            'color'       => "#00C599",
            ]);
        DB::table('areas')->insert([
            'nombre'      => "Acción Social",
            'color'       => "#00D86B",
            ]);
        DB::table('areas')->insert([
            'nombre'      => "Vida Estudiantil",
            'color'       => "#D2D000",
            ]);

            // User_type
        DB::table('user_types')->insert([
            'id' => 1,
            'nombre' => 'Administrador',
            ]);
        DB::table('user_types')->insert([
            'id' => 2,
            'nombre' => 'Usuario de niveles',
            ]);
        DB::table('user_types')->insert([
            'id' => 3,
            'nombre' => 'Usuario de promedios',
            ]);
        DB::table('user_types')->insert([
            'id' => 4,
            'nombre' => 'Usuario de actividades',
            ]);
        // Usuarios
        DB::table('users')->insert([
            'name'      => "Administrador",
            'apellidos' => "Apellidos",
            'activo'    => 1,
            'email'     => "admin@admin.com",
            'password'  => bcrypt('password'),
            'user_type' => 1,
            ]);
        DB::table('users')->insert([
            'name'      => "Invitado",
            'apellidos' => "Apellidos",
            'activo'    => null,
            'email'     => "invitado@invitado.com",
            'password'  => bcrypt('password'),
            'user_type' => 1,
            ]);

        
        // Bancos
        DB::table('bancos')->insert([
            'nombre'      => "Banco Nacional",
            ]);
        DB::table('bancos')->insert([
            'nombre'      => "Banco Costa Rica",
            ]);
        DB::table('bancos')->insert([
            'nombre'      => "Banco Popular",
            ]);

        // Carreras
        DB::table('carreras')->insert([
            'nombre'      => "Informática Empresarial",
            'sigla'       => "IF",
            ]);
        DB::table('carreras')->insert([
            'nombre'      => "Dirección de Empresas",
            'sigla'       => "DN",
            ]);
        DB::table('carreras')->insert([
            'nombre'      => "Informática y Tecnología Multimedia",
            'sigla'       => "TM",
            ]);
        DB::table('carreras')->insert([
            'nombre'      => "Enseñanza del Inglés",
            'sigla'       => "LM",
            ]);
        DB::table('carreras')->insert([
            'nombre'      => "Bibliotecología",
            'sigla'       => "BI",
            ]);
        DB::table('carreras')->insert([
            'nombre'      => "Turismo Ecológico",
            'sigla'       => "TE",
            ]);
        DB::table('carreras')->insert([
            'nombre'      => "Gestión Cultural",
            'sigla'       => "GC",
            ]);
        DB::table('carreras')->insert([
            'nombre'      => "Enseñanza del Castellano y la Literatura",
            'sigla'       => "FL",
            ]);
        DB::table('carreras')->insert([
            'nombre'      => "Administración Aduanera",
            'sigla'       => "XP",
            ]);
        DB::table('carreras')->insert([
            'nombre'      => "Ingeniería Eléctrica",
            'sigla'       => "IE",
            ]);
        DB::table('carreras')->insert([
            'nombre'      => "Inglés con Formación en Gestión Empresarial",
            'sigla'       => "IFGE",
            ]);

        // Periodo
        DB::table('periodos')->insert([
            'fechaInicio' => "2016-08-31 17:22:32",
            'fechaFinal'  => "2016-08-31 17:22:32",
            'mensaje'     => "Hola esto es un mensaje modificable",
            ]);

        // Estudiantes
        DB::table('estudiantes')->insert([
            'nombre'                  => "Nombre1",
            'apellidos'               => "Apellido",
            'correo'                  => "example@mail.com",
            'carnet'                  => "B41111",
            'cedula'                  => "1234563",
            'cuentaBancaria'          => '12873218733',
            'URLDocumentoCedula'      => 'https://tree.taiga.io/',
            'URLDocumentosPersonales' => "https://tree.taiga.io/",
            'URLInformeMatricula'     => "https://tree.taiga.io/",
            'URLExpedienteAcademico'  => "https://tree.taiga.io/",
            'nivel'                   => "No aplica",
            'promedio'                => 9.0,
            'fkBanco'                 => 1,
            'fkCarrera'               => 1,
            ]);
        DB::table('estudiantes')->insert([
            'nombre'                  => "Nombre2",
            'apellidos'               => "Apellido",
            'correo'                  => "example@mail.com",
            'carnet'                  => "B42222",
            'cedula'                  => "1234562",
            'cuentaBancaria'          => '12873218733',
            'URLDocumentoCedula'      => 'https://tree.taiga.io/',
            'URLDocumentosPersonales' => "https://tree.taiga.io/",
            'URLInformeMatricula'     => "https://tree.taiga.io/",
            'URLExpedienteAcademico'  => "https://tree.taiga.io/",
            'nivel'                   => "No aplica",
            'promedio'                => 8.3,
            'fkBanco'                 => 2,
            'fkCarrera'               => 2,
            ]);
        DB::table('estudiantes')->insert([
            'nombre'                  => "Nombre3",
            'apellidos'               => "Apellido",
            'correo'                  => "example@mail.com",
            'carnet'                  => "B433334",
            'cedula'                  => "123456",
            'cuentaBancaria'          => '12873218733',
            'URLDocumentoCedula'      => 'https://tree.taiga.io/',
            'URLDocumentosPersonales' => "https://tree.taiga.io/",
            'URLInformeMatricula'     => "https://tree.taiga.io/",
            'URLExpedienteAcademico'  => "https://tree.taiga.io/",
            'nivel'                   => "No aplica",
            'promedio'                => 9.5,
            'fkBanco'                 => 3,
            'fkCarrera'               => 3,
            ]);


          //Designaciones
        DB::table('designacions')->insert([
            'nombre' => 'Estudiante',
            ]);
        DB::table('designacions')->insert([
            'nombre' => 'Asistente',
            ]);
        DB::table('designacions')->insert([
            'nombre' => 'Asistente Graduado',
            ]);

        // Actividades
        DB::table('actividads')->insert([
            'ref'           => 1,
            'sigla'         => null,
            'fkDesignacion' => 1,
            'titulo'        => 'Asistencia a la coordinación de Informática Empresarial',
            'horas'         => 6,
            'descripcion'   => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            'requisitos'    => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            'fkArea'        => 1,
            ]);
        DB::table('actividads')->insert([
            'ref'           => 2,
            'sigla'         => null,
            'fkDesignacion' => 2,
            'titulo'        => 'Asistencia en la oficina de Registo',
            'horas'         => 12,
            'descripcion'   => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            'requisitos'    => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            'fkArea'        => 2,
            ]);
        DB::table('actividads')->insert([
            'ref'           => 3,
            'sigla'         => null,
            'fkDesignacion' => 3,
            'titulo'        => 'Asistencia en la oficina de becas',
            'horas'         => 8,
            'descripcion'   => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            'requisitos'    => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            'fkArea'        => 3,
            ]);

    	// Relacion actividadesEstudiantes
        DB::table('rel_actividad_estudiantes')->insert([
            'fkActividad'  => 1,
            'fkEstudiante' => 1,
            ]);
        DB::table('rel_actividad_estudiantes')->insert([
            'fkActividad'  => 3,
            'fkEstudiante' => 2,
            ]);
        DB::table('rel_actividad_estudiantes')->insert([
            'fkActividad'  => 2,
            'fkEstudiante' => 3,
            ]);
        DB::table('rel_actividad_estudiantes')->insert([
            'fkActividad'  => 1,
            'fkEstudiante' => 3,
            ]);

    }
}
