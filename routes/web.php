<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
	$log = new App\Log();
	$log->ip = $_SERVER['REMOTE_ADDR'];
	$log->userAgent = $_SERVER['HTTP_USER_AGENT'];
	$log->save();

	$periodoSistema = App\Periodo::all()->first();
	$esAccesible = Carbon\Carbon::now()->between(Carbon\Carbon::parse(App\Periodo::all()->first()->fechaInicio), Carbon\Carbon::parse(App\Periodo::all()->first()->fechaFinal));
	return view('welcome', ['periodoSistema' => $periodoSistema, 'esAccesible' => $esAccesible]);
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/main', 'HomeController@main');

//////////// Estudiantes //////////////
// Crea todas las rutas para las funciones del controlador "Estudiantes"
Route::resource('/estudiantes', 'Estudiantes');
// Ruta para datos del formulario de estudiantes
Route::post('newEstudiante', 'Estudiantes@newEstudiante');
// Ruta para cargar el formulario para los estudiantes
Route::get('/formularioEstudiantes', 'Estudiantes@formulario');
// Ruta para descargar o visualizar los documentos del estudiante.
Route::get('/getDocumentos/{carpetaDocumentos}/{carpetaFechas}/{documentos}', 'Estudiantes@getDocumentos');
///////////////////////////////////////

//////////// Actividades //////////////
// Crea todas las rutas para las funciones del controlador "Actividades"
Route::resource('/actividades', 'Actividades');
Route::post('/setEstudianteElegido', 'Actividades@setEstudianteElegido');
Route::post('/eliminarEstudianteElegido', 'Actividades@eliminarEstudianteElegido');
//////////////////////////////////////

//////////// Areas //////////////
// Crea todas las rutas para las funciones del controlador "Areas"
Route::resource('/areas', 'Areas');
///////////////////////////////////////

 //////////// Bancos //////////////
 // Crea todas las rutas para las funciones del controlador "Bancos"
 Route::resource('/bancos', 'Bancos');
 ///////////////////////////////////////

 //////////// Carreras //////////////
 // Crea todas las rutas para las funciones del controlador "Carreras"
 Route::resource('/carreras', 'Carreras');
 ///////////////////////////////////////

// Ruta para actualizar las fechas de acceso al sistema
Route::put('actualizarPeriodo', 'Periodos@update');
///////////////////////////////////////

//////////////CARGAR ARCHIVOS DE EXCEL///////////////////
Route::post('importPromedio', 'ImportPromedios@importPromedio');

//////////////CARGAR ARCHIVOS DE EXCEL PARA NIVELES DE ESTUDIANTE///////////////////
Route::post('importNiveles', 'importNiveles@importNiveles');

//////////// Usuarios //////////////
// Crea todas las rutas para las funciones del controlador "Bancos"
Route::resource('/usuarios', 'Users');
Route::post('/usuarios/delete', 'Users@delete');
Route::post('/contrasena', 'HomeController@contrasena');
///////////////////////////////////////


// Ruta para limpiar la base de datos
Route::post('limpiarBase', 'GestionBD@clean');
 ///////////////////////////////////////

//////////// Temporal para template //////////////
// Crea todas las rutas para las funciones del controlador "Bancos"
// Route::get('/main', function(){
// 	return view('admin.main');
// });
Route::get("mail", function(){
	return view("estudiante.mail.template");
});
///////////////////////////////////////

///////////////////////Pdf///////////////////
Route::get('pdf/{id}', 'PdfController@invoice');

//////////NOTIFICACIONES////////////////////////
Route::get('deleteNotification', 'Notificaciones@destroy');

////////////////// Excel Reportes /////////////////
Route::get('reporteGeneral', 'Reportes@reporteGeneral');
Route::get('reporteEstudiantesSeleccionados', 'Reportes@reporteEstudiantesSeleccionados');

///////////////// Manual de usuario ////////////////
Route::get('manual', function(){
	return view('admin.manual.index');
});
Route::get('manual/actividades', function(){
	return view('admin.manual.secciones.actividades');
});
Route::get('manual/bancos', function(){
	return view('admin.manual.secciones.banco');
});
Route::get('manual/carreras', function(){
	return view('admin.manual.secciones.carreras');
});
Route::get('manual/estudiantes', function(){
	return view('admin.manual.secciones.estudiantes');
});
Route::get('manual/fecha', function(){
	return view('admin.manual.secciones.fecha');
});
Route::get('manual/usuarios', function(){
	return view('admin.manual.secciones.usuarios');
});
Route::get('manual/excel', function(){
	return view('admin.manual.secciones.subir');
});