<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. A "local" driver, as well as a variety of cloud
    | based drivers are available for your choosing. Just store away!
    |
    | Supported: "local", "ftp", "s3", "rackspace"
    |
    */

    'default' => 'local',

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => 's3',

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => 'your-key',
            'secret' => 'your-secret',
            'region' => 'your-region',
            'bucket' => 'your-bucket',
        ],

        // Disco que almacena las fotocopias de las cedulas de los estudiantes
        'cedulas' => [
            'driver' => 'local',
            'root' => storage_path('app/public/cedulas'),
            'visibility' => 'public',
        ],
        //
        // Disco para guardar las cartas de los estudiantes que contienen la fotocopia de cedula, solicitud y atestados
        'cartas' => [
            'driver' => 'local',
            'root' => storage_path('app/public/cartas'),
            'visibility' => 'public',
        ],
        //
        // Diso que guarda los expedientes academicos de los estudiantes
        'expedientesAcademicos' => [
            'driver' => 'local',
            'root' => storage_path('app/public/expedientesAcademicos'),
            'visibility' => 'public',
        ],
        //
        // Disco que almacena los informes de matriculas de los estudiantes
        'informesMatricula' => [
            'driver' => 'local',
            'root' => storage_path('app/public/informesMatricula'),
            'visibility' => 'public',
        ],
        //

        

    ],

];
