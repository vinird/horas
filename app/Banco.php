<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banco extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
    ];

    // Relacion con estudiantes
    public function estudiante()
    {
    	return $this->hasMany('App\Estudiante');
    }
}
