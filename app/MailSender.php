<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class MailSender
{
	/**
	 * Contruc of the class
	 */
    public function __construct(){

    }

    /**
     * @param  Nombre de la plantilla blade
     * @param  Quien envía el correo
     * @param  Envia los datos del usuario a la vista
     * @param  Quien recibe el correo 
     * @param  Nombre de quien recibe el correo
     * @param  El asunto del correo
     * @return Callback (no controlado)
     */
    public static function enviarCorreo($plantilla, $data, $actividades, $de, $para, $paraNombre, $asunto)
    {
    	\Mail::send($plantilla, ["data" => $data, "actividades" => $actividades], function($mensaje) use ($de, $para, $paraNombre, $asunto){
    		$mensaje->from($de)
    		->to($para, $paraNombre)
    		->subject($asunto);
    	});
    }
}
