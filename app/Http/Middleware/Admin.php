<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->user_type !=1 )
        {
            flash('Su usuario no tiene los permisos para acceder a estas funciones', 'danger'); 
            return back();
        }
        return $next($request);
    }
}
