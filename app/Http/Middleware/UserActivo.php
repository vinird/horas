<?php

namespace App\Http\Middleware;

use Closure;

class UserActivo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->activo != 1)
        {
            flash('Su usuario no está activado, por favor comuníquese con el administrador para que active su usuario.', 'danger'); 
            auth()->logout();
            return back();
        }
        return $next($request);
    }
}
