<?php

namespace App\Http\Controllers;

use App\Estudiante;
use App\Actividad;
use App\RelActividadEstudiante;
use Illuminate\Http\Request;

class PdfController extends Controller
{
  public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
        $this->middleware('userActivo');
    }
    
  public function invoice($id)
{
  $estudiante = Estudiante::find($id);
  $rel_actividades = RelActividadEstudiante::where('fkEstudiante',$id)->get();
  $actividades = [];
  foreach ($rel_actividades as $actividad) {
    array_push($actividades,(Actividad::where('id',($actividad->fkActividad))->get()));
  }
  $data = $this->getData();
  $date = date('Y-m-d');
  $view =  \View::make('pdf.invoice', compact('data', 'date', 'actividades', 'estudiante'));
  $pdf = \App::make('dompdf.wrapper');
  $pdf->loadHTML($view);
  return $pdf->stream('invoice');
  // return $pdf->download('invoice');
}

public function getData()
{
  $data =  [
      'quantity'      => '1' ,
      'description'   => 'some ramdom text',
      'price'   => '500',
      'total'     => '500'
  ];
  return $data;
}
}
