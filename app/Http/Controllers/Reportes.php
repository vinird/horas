<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\Estudiante;
use App\Actividad;
use App\Carrera;
use App\Banco;
use App\Area;
use App\RelActividadEstudiante;

class Reportes extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	$this->middleware('auth');
    	$this->middleware('userActivo');
    }

    public function reporteGeneral()
    {
      Excel::create('Reporte_General'.date('_Y-m-d_h:i:s_A'), function($excel) {
        $excel->sheet('Reporte', function($sheet) {
          // Datos
          $estudiantes = Estudiante::all();
          $actividades = Actividad::all()->sortBy("fkArea");
          $areas = Area::all();
          $relEstAct = RelActividadEstudiante::all();

          //Cabezera del documento
          $head= array('Nombre', 'Apellidos', 'Correo', 'Cédula','Carné','Nivel','Promedio','Carrera');
          $headSecundario= array('', '', '', '','','','','');
          $actividadTmp=new actividad();
          foreach ($actividades as $index=>$actividad) {
            $areaTmp= Area::find($actividad->fkArea);
            if($index==0){
              array_push($head,$areaTmp->nombre);
              $areaTmp=$actividad;
            }else{
              if($actividadTmp->fkArea==$actividad->fkArea){
                array_push($head,'');
              }else{
                array_push($head,$areaTmp->nombre);
                $areaTmp=$actividad;
              }
            }
            $actividadTmp=$actividad;
            array_push($headSecundario,$actividad->ref);
          }

          //Llenado del documento
          $excEstudiantes= array();
          $estudiante_actividad= array();
          $marcaAtividades= array();
          foreach ($actividades as $actividad) {
            array_push($marcaAtividades,'');
          }
          foreach ($estudiantes as $estudiante){
            $estudiante->fkCarrera=Carrera::find($estudiante->fkCarrera)->nombre;
            $estudiante->fkBanco=Banco::find($estudiante->fkBanco)->nombre;
            $tempEstudiante=$estudiante->toArray();
            unset($tempEstudiante['id'],
                  $tempEstudiante['cuentaBancaria'],
                  $tempEstudiante['URLDocumentoCedula'],
                  $tempEstudiante['URLDocumentosPersonales'],
                  $tempEstudiante['URLExpedienteAcademico'],
                  $tempEstudiante['URLInformeMatricula'],
                  $tempEstudiante['fkBanco'],
                  $tempEstudiante['created_at'],
                  $tempEstudiante['updated_at']);

            $relaciones= RelActividadEstudiante::where('fkEstudiante',$estudiante->id)->get();
            $actividadesEstudiante= array();
            foreach ($relaciones as $relacion) {
              array_push($actividadesEstudiante,Actividad::find($relacion->fkActividad));
            }
            $contador=0;
            foreach ($actividades as $actividad) {
              foreach ($actividadesEstudiante as $actividadEst)  {
                if($actividad==$actividadEst){
                  $marcaAtividades[$contador]='x';
                }
              }
              $contador=$contador+1;
            }
            $datos=array_merge($tempEstudiante,$marcaAtividades);
            array_push($excEstudiantes,$datos);
            foreach ($marcaAtividades as $index => $marca) {
              $marcaAtividades[$index]='';
            }
          }

          $sheet->row(1, $head);
          $sheet->row(2, $headSecundario);
          $sheet->fromArray($excEstudiantes, null, 'A3', true , false);
        });
      })->export('xlsx');

      // generadorReportes(Estudiante::all());
    }

    public function reporteEstudiantesSeleccionados()
    {

    	Excel::create('Reporte_Estudiantes_Seleccionados'.date('_Y-m-d_h:i:s_A'), function($excel) {
        $excel->sheet('Reporte', function($sheet) {
          // Datos

          $estudiantes = Estudiante::join('rel_actividad_estudiantes','estudiantes.id','rel_actividad_estudiantes.fkEstudiante')->where('elegido',1)->get();
          $actividades = Actividad::all()->sortBy("fkArea");
          $areas = Area::all();
          $relEstAct = RelActividadEstudiante::all();

          //Cabezera del documento
          $head= array('Nombre', 'Apellidos', 'Correo', 'Cédula','Carné','Nivel','Promedio','Carrera');
          $headSecundario= array('', '', '', '','','','','');
          $actividadTmp=new actividad();
          foreach ($actividades as $index=>$actividad) {
            $areaTmp= Area::find($actividad->fkArea);
            if($index==0){
              array_push($head,$areaTmp->nombre);
              $areaTmp=$actividad;
            }else{
              if($actividadTmp->fkArea==$actividad->fkArea){
                array_push($head,'');
              }else{
                array_push($head,$areaTmp->nombre);
                $areaTmp=$actividad;
              }
            }
            $actividadTmp=$actividad;
            array_push($headSecundario,$actividad->ref);
          }

          //Llenado del documento
          $excEstudiantes= array();
          $estudiante_actividad= array();
          $marcaAtividades= array();
          foreach ($actividades as $index => $actividad) {
            array_push($marcaAtividades,'');
          }
          foreach ($estudiantes as $estudiante){
            $estudiante->fkCarrera=Carrera::find($estudiante->fkCarrera)->nombre;
            $estudiante->fkBanco=Banco::find($estudiante->fkBanco)->nombre;
            $tempEstudiante=$estudiante->toArray();
            unset($tempEstudiante['id'],
                  $tempEstudiante['cuentaBancaria'],
                  $tempEstudiante['URLDocumentoCedula'],
                  $tempEstudiante['URLDocumentosPersonales'],
                  $tempEstudiante['URLExpedienteAcademico'],
                  $tempEstudiante['URLInformeMatricula'],
                  $tempEstudiante['fkBanco'],
                  $tempEstudiante['created_at'],
                  $tempEstudiante['updated_at'],
                  $tempEstudiante['elegido'],
                  $tempEstudiante['fkEstudiante'],
                  $tempEstudiante['fkActividad']);
            $relaciones= RelActividadEstudiante::where('fkEstudiante',$estudiante->id)->get();
            $actividadesEstudiante= array();
            foreach ($relaciones as $relacion) {
              array_push($actividadesEstudiante,Actividad::find($relacion->fkActividad));
            }
            $contador=0;
            foreach ($actividades as $actividad) {
              foreach ($actividadesEstudiante as $actividadEst)  {
                if($actividad==$actividadEst){
                  $marcaAtividades[$contador]='x';
                }
              }
              $contador=$contador+1;
            }
            $datos=array_merge($tempEstudiante,$marcaAtividades);
            array_push($excEstudiantes,$datos);
            foreach ($marcaAtividades as $index => $marca) {
              $marcaAtividades[$index]='';
            }
          }

          $sheet->row(1, $head);
          $sheet->row(2, $headSecundario);
          $sheet->fromArray($excEstudiantes, null, 'A3', true , false);
        });
    	})->export('xlsx');
    }
}

//No se como hacer que resiva parametros si saben seria bueno implementarlo para disminur el codigo
// function generadorReportes($listaEstudiantes){
//   Excel::create('Reporte_General'.date('_Y-m-d_h:i:s_A'), function($excel) {
//     $excel->sheet('Reporte', function($sheet) {
//       // Datos
//       $estudiantes = $listaEstudiantes;
//       $actividades = Actividad::all()->sortBy("fkArea");
//       $areas = Area::all();
//       $relEstAct = RelActividadEstudiante::all();
//
//       //Cabezera del documento
//       $head= array('Id', 'Nombre', 'Apellidos', 'Correo', 'Cédula','Carné','Nivel','Promedio','Carrera');
//       $headSecundario= array('', '', '', '', '','','','','');
//       $actividadTmp=new actividad();
//       foreach ($actividades as $index=>$actividad) {
//         $areaTmp= Area::find($actividad->fkArea);
//         if($index==0){
//           array_push($head,$areaTmp->nombre);
//           $areaTmp=$actividad;
//         }else{
//           if($actividadTmp->fkArea==$actividad->fkArea){
//             array_push($head,'');
//           }else{
//             array_push($head,$areaTmp->nombre);
//             $areaTmp=$actividad;
//           }
//         }
//         $actividadTmp=$actividad;
//         array_push($headSecundario,$actividad->ref);
//       }
//
//       //Llenado del documento
//       $excEstudiantes= array();
//       $estudiante_actividad= array();
//       $marcaAtividades= array();
//       foreach ($actividades as $index => $actividad) {
//         array_push($marcaAtividades,'');
//       }
//       foreach ($estudiantes as $estudiante){
//         $estudiante->fkCarrera=Carrera::find($estudiante->fkCarrera)->nombre;
//         $estudiante->fkBanco=Banco::find($estudiante->fkBanco)->nombre;
//         $tempEstudiante=$estudiante->toArray();
//         unset($tempEstudiante['cuentaBancaria'],$tempEstudiante['URLDocumentoCedula'],$tempEstudiante['URLDocumentosPersonales'],$tempEstudiante['URLExpedienteAcademico'],$tempEstudiante['URLInformeMatricula'],$tempEstudiante['fkBanco'],$tempEstudiante['created_at'],$tempEstudiante['updated_at']);
//         $relaciones= RelActividadEstudiante::where('fkEstudiante',$estudiante->id)->get();
//         $actividadesEstudiante= array();
//         foreach ($relaciones as $relacion) {
//           array_push($actividadesEstudiante,Actividad::find($relacion->fkActividad));
//         }
//         foreach ($actividades as $index=>$actividad) {
//           foreach ($actividadesEstudiante as $actividadEst)  {
//             if($actividad==$actividadEst){
//               $marcaAtividades[$index]='x';
//             }
//           }
//         }
//         $datos=array_merge($tempEstudiante,$marcaAtividades);
//         array_push($excEstudiantes,$datos);
//       }
//
//       $sheet->row(1, $head);
//       $sheet->row(2, $headSecundario);
//       $sheet->fromArray($excEstudiantes, null, 'A3', true , false);
//     });
//   })->export('xlsx');
// }
