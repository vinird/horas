<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

//Modelos
use App\Periodo;
use Carbon\Carbon;

class Periodos extends Controller
{
  public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
        $this->middleware('userActivo');
    }
  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request)
  {
      $this->validate($request, [
          'fechaInicio'    => 'required|date|before:fechaFinal',
          'fechaFinal'     => 'required|date|after:fechaInicio',
          'mensajeSistema' => 'required|max:65535'
        ]
      );

      $fechaSistema=Periodo::find(1);

      $fechaSistema->fechaInicio = Carbon::parse($request->fechaInicio);
      $fechaSistema->fechaFinal  = Carbon::parse($request->fechaFinal);
      $fechaSistema->mensaje     = $request->mensajeSistema;

      if(!$fechaSistema->save())
      {
        flash('Error al actualizar las fechas de acceso al sistema.', 'danger'); // Si no se guarda despliega esta alerta
        return back();
      }
      else
      {
        flash('Fechas de acceso al sistema actualizadas exitosamente.', 'success');
      }

      // Retorna
      return back();
  }
}
