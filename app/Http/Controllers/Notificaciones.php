<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

// Modelos
use App\Notificacion;
// Namespace para consultas a la base de datos
use DB;

class Notificaciones extends Controller
{

    // ELIMINAR NOTIFICACIONES
    public function destroy()
    {
      $this->middleware('userActivo');
      if(auth()->user()->user_type == 1)
      {
        Notificacion::getQuery()->delete();  
      }
      return back();
    }
}
