<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use Auth;

use App\RelAreaUser;

class Users extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
        $this->middleware('userActivo');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $users = User::all();
      return view('admin.usuarios.vistaUsuarios')->with(['users' => $users]);
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        dd("create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre'              => 'required|max:250',
            'apellido'            => 'required|max:250',
            'correo'              => 'required|max:250|unique:users,email',
            'tipo'                => 'required',
            'contrasena'          => 'required|max:250|min:6',
            'confirmarContrasena' => 'required|max:250|min:6',
            ]);
        if($request->contrasena != $request->confirmarContrasena){
            flash('Las contraseñas no son iguales', 'danger');
        }

        $user            = new User();
        $user->name      = $request->nombre;
        $user->apellidos = $request->apellido;
        $user->email     = $request->correo;
        $user->user_type = $request->tipo;
        $user->password  = bcrypt($request->contrasena);
        
        if($request->activo != null){
            $user->activo = 1;
        }

        flash('Error al agregar el usuario', 'danger');
        \DB::transaction(function() use ($request, $user){
            if ($user->save()) {
                flash('Usuario agregado', 'success');
            }
            $userId = $user->id;
            if($request->areas != null)
            {
                foreach ($request->areas as $area) {
                    $relAreaUser       = new RelAreaUser();
                    $relAreaUser->user = $userId;
                    $relAreaUser->area = $area;
                    $relAreaUser->save();
                }
            }


        });
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre'   => 'required|max:250',
            'correo'   => 'required|max:250',
            'apellido' => 'required|max:250',
            'activo'   => 'required',
            ]);
        if(Auth::user()->id == $id && $request->activo == 0){
            flash('No puede desactivar su usuario.', 'danger');
            return back();
        }

        $user = User::find($id);
        if($user != null){
            $user->name      = $request->nombre;
            $user->apellidos = $request->apellido;
            $user->activo    = $request->activo;
            $user->email     = $request->correo;
            if($user->save()){
                flash('El usuario ha sido modificado.', 'success');
            } else{
                flash('Error al modificar el usuario.', 'danger');
            }
        } else {
            flash('El usuario no existe.', 'danger');
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        dd("destroy");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if(Auth::user()->id == $request->id){
            flash('No puede borrar su propio usuario', 'danger');
            return back();
        }

        $relAreaUser = RelAreaUser::where('user', '=', $request->id.'')->get();

        \DB::transaction(function() use($relAreaUser, $request)
        {
            flash('Error al eliminar el usuario', 'danger');

            foreach ($relAreaUser as $key) {
                RelAreaUser::destroy($key->id);
            }

            $query = User::destroy($request->id);
            if($query){
                flash('Usuario eliminado', 'success');
            }
        });
        return back();
    }
}
