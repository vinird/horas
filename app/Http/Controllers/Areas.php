<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

//Modelos
use App\Area;
use App\RelAreaUser;
use App\Actividad;

class Areas extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
        $this->middleware('userActivo');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.areas');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre'     => 'required|max:200',
            'color'    => 'required|max:10',
            ]);

        // Se crea un instancia del modelo
        $area = new Area();
        // Se asignan valores a la area
        $area->nombre  = $request->nombre;
        $area->color = $request->color;

        // Se guardan los datos de la area en la base de datos
        if(!$area->save())
        {
          flash('Error al guardar los datos.', 'danger'); // Si no se guarda despliega esta alerta
          return back();
        }
        else
        {
          flash('Área guardada exitosamente.', 'success');
          return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $area = Area::find($id);
        return view('admin.editarAreas')->with(['area' => $area]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
          'nombre'     => 'required|max:200',
          'color'    => 'required|max:10',
          ]);

      $area = Area::find($id);

      $area->nombre  = $request->nombre;
      $area->color = $request->color;

      if(!$area->save())
      {
        flash('Error al actualizar los datos.', 'danger'); // Si no se guarda despliega esta alerta
        return back();
      }
      else
      {
        flash('Área actualizada exitósamente.', 'success');
      }
      // Retorna
      return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $existe = Actividad::where('fkArea', '=', $id)->first();
        if(isset($existe))
        {
            flash('Esta área tiene actividades relacionadas, elimine primero las actividades para poder realizar la acción.', 'danger');
            return back();
        }


        $query = \DB::table('rel_area_users')->where('area', '=', $id)->get();
        if (count($query) > 0) {
            flash('Esta área tiene usuarios de actividades registrados, por lo tanto no se puede eliminar. Verifique los usuarios y elimine los que están asignados a esta área para poder realizar esta acción.', 'danger');
            return back();
        }

        if (Area::destroy($id)) {
            flash('Área eliminada exitósamente.', 'success');
        } else {
            flash('Error al eliminar los datos.', 'danger');
        }

        return back();
    }
}
