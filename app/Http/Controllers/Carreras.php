<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Carrera;

use App\Estudiante;

class Carreras extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
        $this->middleware('userActivo');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|max:100',
            'sigla'  => 'required|max:20',
            ]);

        $carrera         = new Carrera();
        $carrera->nombre = $request->nombre;
        $carrera->sigla  = $request->sigla;

        if ($carrera->save()) {
            flash('Carrera guardada exitosamente.', 'success'); // Si no se guarda despliega esta alerta
        } else {
            flash('Error al guardar los datos.', 'danger'); // Si no se guarda despliega esta alerta
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required|max:100',
            'sigla'  => 'required|max:20',
            ]);

        $carrera         = Carrera::find($id);
        $carrera->nombre = $request->nombre;
        $carrera->sigla  = $request->sigla;

        if ($carrera->save()) {
            flash('Carrera actualizada exitósamente.', 'success'); // Si no se guarda despliega esta alerta
        } else {
            flash('Error al modificar los datos.', 'danger'); // Si no se guarda despliega esta alerta
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $existe = Estudiante::where('fkCarrera', '=', $id)->first();
        if(isset($existe))
        {
            flash('Esta carrera tiene estudiantes relacionadas, elimine primero los estudiantes para poder realizar la acción.', 'danger');
            return back();
        }
        if(Carrera::destroy($id)){
            flash('Carrera eliminada exitosamente.', 'success'); // Si no se guarda despliega esta alerta
        } else {
            flash('Error al eliminar los datos.', 'danger'); // Si no se guarda despliega esta alerta
        }
        return back();
    }
}
