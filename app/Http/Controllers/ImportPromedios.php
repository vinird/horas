<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Maatwebsite\Excel\Facades\Excel;
use App\Estudiante;
use File;

class ImportPromedios extends Controller
{
	public function __construct()
	{
        $this->middleware('auth');
        $this->middleware('userActivo');
	}

	public function importPromedio(Request $request)
	{
		$file = $request->file('promedios');
		$formato = File::extension($file->getClientOriginalName());

		if ($formato == "xml" || $formato == "csv" || $formato == "xlsx") {

			Excel::load($file, function($reader) {

				$estudiantes = Estudiante::all();
				foreach ($reader->get() as $promedios) {
					foreach ($estudiantes as $estudiante) {
						if (strcasecmp($estudiante->carnet, $promedios->carnet) == 0 || strcasecmp($estudiante->carnet, $promedios->carné) == 0 || strcasecmp($estudiante->carnet, $promedios->carne) == 0) {
							if($promedios->promedio != null) {
								$estudiante->promedio = $promedios->promedio;
								$estudiante->save();
							} else {
								if ($promedios->promedios != null) {
									$estudiante->promedio = $promedios->promedios;
									$estudiante->save();
								}
							}
						}
					}
				}
			});
			flash('Promedios agregados exitósamente.', 'success');
			return back();
		} else {
			 flash('Formato del archivo incorrecto, debe usar .csv / .xlsx / .xml.', 'danger');
			return back();
		}
	}
}
