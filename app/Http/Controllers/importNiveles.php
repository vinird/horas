<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Maatwebsite\Excel\Facades\Excel;
use App\Estudiante;
use File;

class importNiveles extends Controller
{
	public function __construct()
	{
        $this->middleware('auth');
        $this->middleware('userActivo');
	}

  	public function importNiveles(Request $request)
	{
		$file = $request->file('niveles');
		$formato = File::extension($file->getClientOriginalName());

		if ($formato == "xml" || $formato == "csv" || $formato == "xlsx") {

			Excel::load($file, function($reader) {

				$estudiantes = Estudiante::all();
				foreach ($reader->get() as $niveles) {
					foreach ($estudiantes as $estudiante) {
						if (strcasecmp($estudiante->carnet, $niveles->carnet) == 0 || strcasecmp($estudiante->carnet, $niveles->carné) == 0 || strcasecmp($estudiante->carnet, $niveles->carne) == 0) {
							if($niveles->nivel != null) {
								$estudiante->nivel = $niveles->nivel;
								$estudiante->save();
							} else {
								if ($niveles->niveles != null) {
									$estudiante->nivel = $niveles->niveles;
									$estudiante->save();
								}
							}
						}
					}
				}
			});
			flash('Niveles agregados exitósamente.', 'success');
			return back();
		} else {
			 flash('Formato del archivo incorrecto, debe usar .csv / .xlsx / .xml.', 'danger');
			return back();
		}
	}
}
