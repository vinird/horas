<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

//Modelos
use App\Banco;

use App\Estudiante;

class Bancos extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
        $this->middleware('userActivo');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'nombre'     => 'required|max:100',
        ]);

        // Se crea un instancia del modelo
        $banco = new Banco();
        // Se asignan valor al nombre del banco
        $banco->nombre  = $request->nombre;

        // Se guardan el dato del banco en la base de datos
        if(!$banco->save())
        {
            flash('Error al guardar los datos.', 'danger'); // Si no se guarda despliega esta alerta
            return back();
        }
        else
        {
            flash('Banco guardado exitosamente.', 'success');
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    $this->validate($request, [
        'nombre'     => 'required|max:100'
    ]);

      $banco = Banco::find($id);

      $banco->nombre  = $request->nombre;

      if(!$banco->save())
      {
        flash('Error al actualizar los datos.', 'danger'); // Si no se guarda despliega esta alerta
        return back();
      }
      else
      {
        flash('Banco actualizado exitósamente.', 'success');
      }
      // Retorna
      return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      // Instancia a actividad para eliminarlo

      $existe = Estudiante::where('fkBanco', '=', $id)->first();

      if(isset($existe)){

        flash('El banco no se puede eliminar porque está asociado a un estudiante.' , 'danger');
      }
      else{
        if(Banco::destroy($id))
        {
          flash('Banco eliminado exitosamente.', 'success');
        }
        else
        {
          flash('Error al eliminar los datos.', 'danger'); // Si no se guarda despliega esta alerta
        }

      }
      return back();
    }
}
