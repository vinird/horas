<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use Hash;
use File;
use DB;

//Modelos
use App\Actividad;
use App\Estudiante;

class GestionBD extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
        $this->middleware('userActivo');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Limpia la base de datos.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function clean(Request $request)
    {
      $this->validate($request, [
          'confirmarContrasenaRespaldo' => 'required|max:250',
      ]);
      if(Hash::check($request->confirmarContrasenaRespaldo, Auth::user()->password)) {
        $dirFiles = storage_path().'/app/public';
        DB::table('rel_actividad_estudiantes')->delete();
        Actividad::getQuery()->delete();
        Estudiante::getQuery()->delete();
        File::cleanDirectory($dirFiles.'/cartas');
        File::cleanDirectory($dirFiles.'/cedulas');
        File::cleanDirectory($dirFiles.'/expedientesAcademicos');
        File::cleanDirectory($dirFiles.'/informesMatricula');
        flash('Mantenimiento del sistema realizado existosamente.', 'success');
      } else {
        flash('La contraseña introducida no es correcta.', 'danger');
      }
      return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
