<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

use App\Log;
use App\Area;
use App\User;
use App\Banco;
use App\Periodo;
use App\Carrera;
use App\Actividad;
use App\User_type;
use App\Estudiante;
use App\RelAreaUser;
use App\Designacion;
use App\RelActividadEstudiante;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('userActivo');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_type = auth()->user()->user_type;

        $areas                       = 0;
        $bancos                      = 0;
        $carreras                    = 0;
        $usuarios                    = 0;
        $user_types                  = 0;
        $actividades                 = 0;
        $estudiantes                 = 0;
        $cantidadlogs                = 0;
        $primerAcceso                = 0;
        $designaciones               = 0;
        $periodoSistema              = 0;
        $usuariosActivos             = 0;
        $cantidadUsuarios            = 0;
        $relacionActividadEstudiante = 0;
        $relAreaUser                 = [];
        $notificaciones              = 0;

        if($user_type == 4)
        {
            $areas         = Area::all();
            $designaciones = Designacion::all();
            $relAreaUser   = RelAreaUser::where('user', '=', auth()->user()->id)->get();

        } elseif ($user_type == 2 || $user_type == 3) {
            $carreras                    = Carrera::all();
            $bancos                      = Banco::all();
            $relacionActividadEstudiante = RelActividadEstudiante::all();

        } elseif ($user_type == 1) {
            $areas                       = Area::all();
            $bancos                      = Banco::all();
            $carreras                    = Carrera::all();
            $user_types                  = User_type::all();
            $relAreaUser                 = RelAreaUser::all();
            $designaciones               = Designacion::all();
            $relacionActividadEstudiante = RelActividadEstudiante::all();
            $usuarios                    = \DB::table('users')->select('id', 'name', 'apellidos', 'email', 'activo', 'user_type')->get();
            $notificaciones              = \App\Notificacion::all();
        }

        $cantidadlogs                = Log::count();
        $primerAcceso                = Log::find(1);
        $cantidadUsuarios            = User::count();
        $actividades                 = \DB::table('actividads')->orderBy('fkArea', 'asc')->get();
        $estudiantes                 = Estudiante::all();
        $periodoSistema              = Periodo::all()->first();
        $usuariosActivos             = User::where('activo', '=', 1)->get()->count();

        if (isset($primerAcceso)) {
            $primerAcceso = $primerAcceso->created_at;
        }

        return view('admin.main')->with([
            "areas"                       => $areas,
            "bancos"                      => $bancos,
            "carreras"                    => $carreras,
            'usuarios'                    => $usuarios,
            'user_types'                  => $user_types,
            'relAreaUser'                 => $relAreaUser,
            "actividades"                 => $actividades,
            "estudiantes"                 => $estudiantes,
            'primerAcceso'                => $primerAcceso,
            'cantidadlogs'                => $cantidadlogs,
            "designaciones"               => $designaciones,
            "periodoSistema"              => $periodoSistema,
            'usuariosActivos'             => $usuariosActivos,
            'cantidadUsuarios'            => $cantidadUsuarios,
            "relacionActividadEstudiante" => $relacionActividadEstudiante,
            "notificaciones"              => $notificaciones,
            ]);
    }

    public function contrasena(Request $request){
        $this->validate($request, [
            'antiguaContrasena'        => 'required',
            'nuevaContrasena'          => 'required|min:6',
            'confirmarNuevaContrasena' => 'required|min:6'
            ]);

        $user = User::find(\Auth::user()->id);

        $contrasenaCorrecta = Hash::check($request->antiguaContrasena, $user->password);

        if($contrasenaCorrecta == false){
            flash('Contraseña inválida.', 'danger');
            return back();
        }
        if($request->nuevaContrasena != $request->confirmarNuevaContrasena){
            flash('Las contraseñas no son iguales.', 'danger');
            return back();
        }
        if($request->antiguaContrasena == $request->nuevaContrasena){
            flash('La nueva contraseña debe ser diferente a la antigua.', 'danger');
            return back();
        }
        $user->password = bcrypt($request->nuevaContrasena);

        if($user->save()){
            flash('Contraseña modificada exitosamente.', 'success');
        } else {
            flash('Error al modificar los datos.', 'danger');
        }
        return back();
    }
}
