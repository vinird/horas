<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

//Modelos
use App\Actividad;
use App\Designacion;
use App\Area;
use DB;
use App\RelActividadEstudiante;

class Actividades extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('userActivo');
    }

    private function orderRef()
    {
      \DB::transaction(function(){
        $actividades = Actividad::orderBy('fkArea', 'asc')->get();
        $index = 1;
        foreach ($actividades as $actividad) {
          $actividad->ref = $index;
          $actividad->save();
          $index++;
        }
      });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {
        //
        $nuevaRef = DB::table('actividads')->max('id')+1;
        $designaciones = Designacion::all();
        $areas = Area::all();
        return view('admin.actividades')->with(['nuevaRef' => $nuevaRef, 'designaciones' => $designaciones, 'areas' => $areas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // Validaciones de los campos del formulario
      $this->validate($request, [
          'designacion' => 'required',
          'titulo'    => 'required|max:100',
          'area' => 'required',
          'horas'     => 'required|digits_between:1,2',
          'descripcion'   => 'required',
          'requisitos'       => 'required'
          ]);

      // Se crea un instancia del modelo
      $actividad = new Actividad();
      // Se asignan valores a la actividad
      $actividad->titulo        = $request->titulo;
      $actividad->horas         = $request->horas;
      $actividad->fkDesignacion = $request->designacion;
      $actividad->fkArea        = $request->area;
      $actividad->descripcion   = $request->descripcion;
      $actividad->requisitos    = $request->requisitos;
      $actividad->created_by   = auth()->user()->id;

      // Se guardan los datos de la actividad en la base de datos
      if(!$actividad->save())
      {
        flash('Error al guardar los datos.', 'danger'); // Si no se guarda despliega esta alerta
        return back();
      }
      else
      {
        flash('Actividad guardada exitosamente.', 'success');
      }

      $this->orderRef();
      // Retorna
      return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $designaciones = Designacion::all();
        $areas = Area::all();
        $actividad = Actividad::find($id);
        return view('admin.editarActividades')->with(['designaciones' => $designaciones, 'areas' => $areas, 'actividad' => $actividad]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'titulo'      => 'required|max:100',
            'horas'       => 'required|digits_between:1,2',
            'area'        => 'required',
            'designacion' => 'required',
            'descrip'     => 'required',
            'req'         => 'required'
          ]
        );


          $actividad=Actividad::find($id);

          if (auth()->user()->user_type == 4) {
            if ($actividad->created_by != auth()->user()->id) {
              flash('Usted no tiene permisos para modificar esta actividad.', 'danger');
              return back();
            }
          }

          $actividad->titulo        = $request->titulo;
          $actividad->horas         = $request->horas;
          $actividad->fkDesignacion = $request->designacion;
          $actividad->fkArea        = $request->area;
          $actividad->descripcion   = $request->descrip;
          $actividad->requisitos    = $request->req;

          if(!$actividad->save())
          {
            flash('Error al actualizar los datos.', 'danger'); // Si no se guarda despliega esta alerta
            return back();
          }
          else
          {
            flash('Actividad actualizada exitosamente.', 'success');
          }

          $this->orderRef();
          // Retorna
          return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      // Instancia a actividad para eliminarlo
      $actividad = Actividad::where('id', '=', $id)->firstOrFail();
      DB::table('rel_actividad_estudiantes')->where('fkActividad', '=', $id)->delete();
      if($actividad != null){
        if(!$actividad->destroy($id))
        {
          flash('Error al eliminar los datos.', 'danger'); // Si no se guarda despliega esta alerta
        }
        else
        {
          flash('Actividad eliminada exitosamente.', 'success');
        }

      }
      $this->orderRef();
      return back();
    }


    public function setEstudianteElegido(Request $request)
    {
      $relacion = RelActividadEstudiante::where('fkEstudiante', '=', $request->fkEstudiante)->where('fkActividad', '=', $request->fkActividad)->first();
      $relacion->elegido = 1;
      if ($relacion->save()) {
          flash('El estudiante fue elegido.', 'success');
      } else {
        flash('Error al modificar el estado del estudiante.', 'danger');
      }
      return back();
    }


    public function eliminarEstudianteElegido(Request $request)
    {
      $relacion = RelActividadEstudiante::where('fkEstudiante', '=', $request->fkEstudiante)->where('fkActividad', '=', $request->fkActividad)->first();
      $relacion->elegido = null;
      if ($relacion->save()) {
          flash('El estudiante ya no está eligido en esta actividad.', 'success');
      } else {
        flash('Error al modificar el estado del estudiante.', 'danger');
      }
      return back();
    }

}
