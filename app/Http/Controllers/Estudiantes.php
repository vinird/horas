<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

// Modelos
use App\Designacion;
use App\Actividad;
use App\Banco;
use App\Carrera;
use App\Estudiante;
use App\RelActividadEstudiante;
use App\MailSender;
use App\Notificacion;

// Namespace para el almacenamiento
use Storage;

// Namespace para consultas a la base de datos
use DB;

class Estudiantes extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function formulario()
    {
      $bancos =        Banco::all();
      $designaciones = Designacion::all();
      $actividades =   Actividad::all();
      $carreras =      Carrera::all();
        // Se retorna la vista formulario, con actividades y bancos
      return view('estudiante.formulario')->with(['bancos' => $bancos, 'actividades' => $actividades, 'carreras' => $carreras, 'designaciones' => $designaciones]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDocumentos($carpetaDocumentos, $carpetaFechas, $documentos)
    {
      return response()->file(storage_path().'/app/public/'.$carpetaDocumentos.'/'.$carpetaFechas.'/'.$documentos);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCarnet($carnet)
    {
        // Obtiene el id del estudiante actual
      $estudiante = Estudiante::where('carnet', '=', $carnet)->first();
        // Verifica que la consulta obtuvo resultados
      if(count($estudiante) > 0) {
            // Compara el resultado con el parametro ignorando mayúsculas o minúsculas
        if(strcasecmp($estudiante->carnet, $carnet) == 0){
                return 1; // Retorna 1 si el carnet ya existe
              }
            }
        return null; // Retorna null si no encontró un carnet similar
      }

     /** Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
     */
     public function newEstudiante(Request $request)
     {
      try {
        // Validaciones de los campos del formulario
        $this->validate($request, [
          'nombre'    => 'required|max:100',
          'apellidos' => 'required|max:100',
          'correo'    => 'required|max:200|email',
          'cedula'    => 'required|max:20',
            'carnet'    => 'required|max:10|unique:estudiantes,carnet', // Se debe testear
            'carrera'   => 'required',
            'cuentaBancaria'    => 'required|max:100',
            'docCarta'              => 'required|mimes:doc,docx,pdf,odt',
            'docCedula'             => 'required|mimes:doc,docx,pdf,odt',
            'docExpedAcademico'     => 'required|mimes:doc,docx,pdf,odt',
            'docInformeMatricula'   => 'required|mimes:doc,docx,pdf,odt',
            'banco'         => 'required',
            'actividades'   => 'required',
            'terminos_y_condiciones'    => 'required',
            ]);        

        // Crea los archivos
        $carta      = $request->file('docCarta');
        $docCedula  = $request->file('docCedula');
        $expediente = $request->file('docExpedAcademico');
        $informe    = $request->file('docInformeMatricula');

        // Ruta de archivos, se agrega el carnet al nombre del archivo. Se crea una carpeta con la fecha actual
        $carta_ruta      = '/'.date('Y').'_'.date('m').'_'.date('d').'/'.$request->carnet.'_'.$carta->getClientOriginalName();
        $docCedula_ruta  = '/'.date('Y').'_'.date('m').'_'.date('d').'/'.$request->carnet.'_'.$docCedula->getClientOriginalName();
        $expediente_ruta = '/'.date('Y').'_'.date('m').'_'.date('d').'/'.$request->carnet.'_'.$expediente->getClientOriginalName();
        $informe_ruta    = '/'.date('Y').'_'.date('m').'_'.date('d').'/'.$request->carnet.'_'.$expediente->getClientOriginalName();

        // Se guardan los archivos
        Storage::disk('cartas')->put($carta_ruta, file_get_contents($carta->getRealPath() ) );
        Storage::disk('cedulas')->put($docCedula_ruta, file_get_contents($docCedula->getRealPath() ) );
        Storage::disk('expedientesAcademicos')->put($expediente_ruta, file_get_contents($expediente->getRealPath() ) );
        Storage::disk('informesMatricula')->put($informe_ruta, file_get_contents($informe->getRealPath() ) );

        // Se crea un instancia del modelo
        $estudiante = new Estudiante();
        // Se asignan valores al estudiante
        $estudiante->apellidos      = $request->apellidos;
        $estudiante->nombre         = $request->nombre;
        $estudiante->correo         = trim($request->correo);
        $estudiante->cedula         = trim($request->cedula);
        $estudiante->carnet         = trim($request->carnet);
        $estudiante->cuentaBancaria = trim($request->cuentaBancaria);
        $estudiante->URLDocumentosPersonales    = $carta_ruta; // Ruta del documento
        $estudiante->URLExpedienteAcademico     = $expediente_ruta; // Ruta del documento
        $estudiante->URLInformeMatricula        = $informe_ruta; // Ruta del documento
        $estudiante->URLDocumentoCedula         = $docCedula_ruta; // Ruta del documento
        $estudiante->fkBanco    = $request->banco; // id de banco
        $estudiante->fkCarrera  = $request->carrera; // id de carrera

        // Se guardan los datos del estudiante en la base de datos
        if(!$estudiante->save())
        {
                flash('Error al guardar los datos.', 'danger'); // Si no se guarda despliega esta alerta
                return back();
              }

            // Guarda notificacion
              $notificacion = new Notificacion();

              $notificacion->nombre    = $request->nombre;
              $notificacion->descripcion = "El estudiante con el carné " + trim($request->carnet) + " está concursando.";

        // Busca el estudiante actual
              $actual_estudiante = Estudiante::where('carnet', '=', trim($request->carnet))->firstOrFail();

        // Obtiene el id del estudiante actual
              $id_estudiante = $actual_estudiante->id;

        // Recorre las actividades y las guarda en la tabla RelActividadEstudiante
        // junto con el id del estudiante actual
              foreach ($request->actividades as $actividad)
              {
                $actividades_estudiante = new RelActividadEstudiante();
            $actividades_estudiante->fkActividad  = intval($actividad); // Convierte el valor a int
            $actividades_estudiante->fkEstudiante = $id_estudiante;
            $actividades_estudiante->save();
          }


          $mailActividades = [];
          foreach ($request->actividades as $actividad) {
            $act = Actividad::find(intval($actividad));
            array_push($mailActividades, $act);
          }

          $conn = @fsockopen("www.google.com", 80);
          if($conn)
          {
           MailSender::enviarCorreo("estudiante.mail.template", $request, $mailActividades, "Universidad@ac.cr", trim($request->correo), $request->nombre, "Concurso de horas estudiante, asistente y asistente de posgrado");
         } else {
          flash('<em>'.$request->nombre.' ' .$request->apellidos.' <em/> los datos se enviaron exitosamente. <br/> Pero se perdió la conexión a internet, por lo tanto no recibirá el mensaje de confirmación en su correo. ', 'success');
          return redirect('/');
        }
        // Envía un correro al estudiante con la confirmación del formulario

        // Retorna
        flash('<em>'.$request->nombre.' ' .$request->apellidos.' <em/> los datos se enviaron exitosamente. <br/>Usted recibirá la confirmación al correo '.$request->correo, 'success');
        return redirect('/');
      } catch (Exception $e) {
        flash('Sucedió un error al enviar la información por favor vuelva a enviar el formulario', 'success');
        return redirect('/');
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
      dd('create');
    }

    // *
    //  * Store a newly created resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response

    // public function store(Request $request)
    // {
    //     //
    //     dd($request->actividades);
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
      dd('show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->middleware('userActivo');
      // Validaciones de los campos del formulario
      if(auth()->check() && auth()->user()->user_type != 4  )
      {
        $this->validate($request, [
          'nombre'    => 'required|max:100',
          'apellidos' => 'required|max:100',
          'carnet'    => 'required|max:10',
          'cedula'    => 'required|numeric',
          'promedio'  => 'numeric',
          'nivel'     => 'max:50',
          ]);

        // Instancia a estudiante para modificarlo
        $estudiante = Estudiante::where('id', '=', $id)->firstOrFail();

        if(auth()->user()->user_type == 1)
        {
          // Se asignan valores al estudiante
          $estudiante->nombre         = $request->nombre;
          $estudiante->apellidos      = $request->apellidos;
          $estudiante->carnet         = $request->carnet;
          $estudiante->cedula         = $request->cedula;
          if ($request->promedio == "") {
            $estudiante->promedio       = null; 
          }else{
            $estudiante->promedio       = $request->promedio; 
          }
          if ($request->nivel == "") {
            $estudiante->nivel          = null; 
          }else{
            $estudiante->nivel          = $request->nivel; 
          }
        } elseif (auth()->user()->user_type == 2) 
        {
          if ($request->nivel == "") {
            $estudiante->nivel          = null; 
          }else{
            $estudiante->nivel          = $request->nivel; 
          }
        } elseif (auth()->user()->user_type == 3)
        {
          if ($request->promedio == "") {
            $estudiante->promedio       = null; 
          }else{
            $estudiante->promedio       = $request->promedio; 
          }
        }

        if(!$estudiante->save())
        {
        flash('Error al modificar los datos.', 'danger'); // Si no se guarda despliega esta alerta
      }
      else
      {
        flash('Estudiante modificado exitosamente.', 'success');
      }
    }
      // Retorna
    return back();
  }

  /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function destroy($id)
  {
    $this->middleware('userActivo');
    if(auth()->check() && auth()->user()->user_type == 1)
    {
        // Instancia a estudiante para eliminarlo
      $estudiante = Estudiante::where('id', '=', $id)->first();
        // Se eliminan las actividades relacionadas al estudiante
      DB::table('rel_actividad_estudiantes')->where('fkEstudiante', '=', $id)->delete();
      if($estudiante != null){
        if(!$estudiante->destroy($id))
        {
          flash('Error al eliminar los datos.', 'danger'); // Si no se guarda despliega esta alerta
        }
        else
        {
          flash('Estudiante eliminado exitosamente.', 'success');
        }
      }
    }
    return back();
  }
}
