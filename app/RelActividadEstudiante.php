<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelActividadEstudiante extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fkActividad', 'fkEstudiante',
    ];


	// Relacion de muchos a muchos con estudiante
    public function estudiante()
    {
    	return $this->belongsToMany('App\Estudiante');
    }

    // Relacion de muchos a muchos con actividad
    public function actividad()
    {
    	return $this->belongsToMany('App\Actividad');
    }
}
