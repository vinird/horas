<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelAreaUser extends Model
{
    protected $fillable = [
        'user', 'area',
    ];

    public function users()
    {
    	return $this->belongsToManny('App\User');
    }

    public function areas()
    {
    	return $this->belongsToManny('App\Area');
    }
}
