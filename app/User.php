<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name', 'apellidos', 'email', 'password', 'tipo',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // Relación con user_type
    public function user_types()
    {
        return $this->belongsTo('App\User_type');
    }

    public function rel_area_users()
    {
        return $this->belongsToManny('App\RelAreaUser');
    }

    public function actividads()
    {
        return $this->hasMany('App\Actividad');
    }
}
