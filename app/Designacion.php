<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designacion extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'nombre',
  ];

  // Relacion inversa con actividad
  public function actividad()
  {
    $this->hasMany('App\Actividad');
  }
}
