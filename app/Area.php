<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'color',
    ];

	// Relacion con actividad
    public function actividad()
    {
    	return $this->hasMany('App\Atividad');
    }

    // Relacion con User
    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function rel_area_users()
    {
        return $this->belongsToManny('App\RelAreaUser');
    }
}
