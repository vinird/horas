<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrera extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'sigla',
    ];

    // Relacion con estudiante
    public function estudiante()
    {
    	return $this->hasMany('App\Estudiante');
    }
}
