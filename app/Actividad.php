<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actividad extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'designacion', 'sigla', 'horas', 'descripcion', 'requisitos', 'fkArea',
    ];

		// Relacion inversa con area
    public function area()
    {
    	return $this->belongsTo('App\Area');
    }

	// Relacion inversa con designacion
	public function designacion()
	{
		$this->belongsTo('App\Designacion');
	}

    public function users()
    {
        return $this->belongsTo('App\User');
    }
}
