<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'apellidos', 'correo', 'cedula', 'carnet', 'URLDocumentosPersonales', 'URLInformeMatricula', 'URLExpedienteAcademico', 'nivel', 'promedio', 'fkBanco', 'fkCarrera',
    ];


    // Relacion inversa con banco
    public function banco()
    {
    	return $this->belongsTo('App\Banco');
    }

    // Relacion inversa con carrera
    public function carrera()
    {
    	return $this->belongsTo('App\Carrera');
    }
}
